# Imports
from radia_mm import  QuadEMYokeProfileParam, MeshExtTriParam, QuadEMYokeParam
from radia_mm import CoilParam, QuadEM, QuadEMParam

# --- Parameters
# Profile
# # profile_p = QuadEMYokeProfileParam(pole_type='hyperbola')
# profile_p = QuadEMYokeProfileParam(pole_type='qf1')
# # Mesh
# mesh_p = MeshExtTriParam()
# # Yoke
# yoke_p = QuadEMYokeParam(chamfer=[10, 10], profile_params=profile_p, mesh_params=mesh_p, axis='y')
# # Coil
# coil_p = CoilParam()
# quad_p = QuadEMParam(yoke_p, coil_p)
# Quad
quad_p = QuadEMParam(quad_type='qf8')

quad = QuadEM(quad_p, solve_switch=False)
quad.plot_geo()
input()




