# Dipole-quad module example
# Gael Le Bec, ESRF, 2020

from radia_mm import DipoleQuadEMYokeProfileParam, DipoleQuadEMYokeProfile, DipoleQuadEMYoke, DipoleQuadEMYokeParam, \
    DipoleQuadEMParam, DipoleQuadEM, MeshExtTriParam, PrecParam
import radia as rad
from matplotlib.pyplot import plot, show
import numpy as np

# Parameters
p_prec = PrecParam(tolerance=1e-4, max_iter=10000) # Precision
p_dq = DipoleQuadEMParam(dipole_quad_type='dq1', prec_params=p_prec) # DQ parameters
p_dq.refine_mesh(long_sub_n=40) # Mesh

# Build and solve
dq = DipoleQuadEM(p_dq, solve_switch=True)
dq.plot_geo() # Plot

# Multipoles
r_ref = 7 # Reference radius
print('Compute multipoles...')
# Integral along a straight line
# multi, ib, ib_r, xz = dq.multipoles(r0 = r_ref) 
# Integral along a line-arc trajectory
multi, ib, ib_r, xz, xy_ref = dq.multipoles_line_arc(r0=r_ref, xz0=[0,0], theta_bend=0.0292, n_points=100, n_multi=16)
print(multi)
