# Cross-talk between a quadrupole and a sextupole
# Gael Le Bec, ESRF, 2019-2020

# --- Import quadrupoles and dipole objects
from radia_mm import QuadEM, QuadEMParam, SextuEMParam, SextuEM, MM
 
# --- Build a quadrupole
quad_params = QuadEMParam(quad_type='qd3') # Quad parameters
quad = QuadEM(quad_params, current=90) # Build and solve the quad
 
# --- Build a sextupole
sextu_params = SextuEMParam(sextu_type='sd1') # Sextupole parameters
sextu = SextuEM(sextu_params, current=65) # Build and solve the sextupole
 
# --- Assembly
distance = 75 # Yoke to yoke distance (mm)
quad.translate(quad.length_yoke() / 2) # Move the quad
quad.top()  # Delete the bottom of the quad
sextu.translate(- sextu.length_yoke() / 2 - distance) # Move the sextupole
sextu.top()  # Delete the bottom of the magnet
quad_sextu = quad + sextu # Create a quad + sextupole assembly
quad_sextu.sym_vert() # Force the vertical symmetry
# quad_sextu.plot_geo() # Plot the assembly
 
# --- Solve
quad_sextu.solve()
 
# --- Analysis
# Quad + sextu assembly
strengths_qs = quad_sextu.strengths()
# Individual magnets
quad, sextu = quad_sextu.split() # Split the assembly
quad.sym_vert(), sextu.sym_vert() # restaure the up / down symmetry
strengths_q = quad.strengths() # Quad only
strengths_s = sextu.strengths() # Sextu only
# Results
print('Integrated gradient              (assembly)  : ', strengths_qs[1], ' T')
print('Integrated gradient              (quadrupole): ', strengths_q[1], ' T')
print('Integrated gradient              (sextupole) : ', strengths_s[1], ' T')
print('Integrated sextupole strength    (assembly)  : ', strengths_qs[2], ' T/mm')
print('Integrated sextupole strength    (quadrupole): ', strengths_q[2], ' T/mm')
print('Integrated sextupole strength    (sextupole) : ', strengths_s[2], ' T/mm')
# quad.plot_gradient([0, 300, 0], [0, -300, 0], x_axis='y')
# sextu.plot_gradient([0, 300, 0], [0, -300, 0], x_axis='y')