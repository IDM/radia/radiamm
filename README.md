# Radia Multipole Magnets (RadiaMM)
## Introduction
The Radia Multipole Magnets (radia_mm) module is dedicated to the simulations of accelerator electromagnet quadrupoles. radia_mm is based on the RADIA magnetostatic code. It inclules functions for building multipoles such as bending magnets, quadrupole lenses and higher order multipoles. 

Radia Multipole Magnets provide an upper layer to RADIA. It allows to build fully parameterized accelerator magnets with a few command lines. A radia_mm object contains parameters (yoke parameters, coil parameters, mesh, etc.) and RADIAobjects (e.g. the main RADIAobject named self.obj). Additional methods are provided for visualisation, field analysis and aims to simplify the use of RADIA.

## Dependencies
RadiaMM was tested with Python 36 and should be compatible with later verions.

This Python module needs the following libraries:

- RADIA and associated packages
  - RADIA for Python: https://github.com/ochubar/radia
  - RADIA utilities for Python: https://gitlab.esrf.fr/IDM/Radia/RadiaUtils
- Standard Python libraries
  - matplotlib
  - numpy and scipy
  - datetime
  - pickle
  - intertools
  
## Installation

No setup file is provided from now, but the installation is rather simple:

- Clone the repository where you want
- Navigate to your python *Lib/site-packages* folder
- Create a *radia_mm.pth* file containing the path to the repository (note that the path to the *radia_utils* can be included in the same file)

 ## 3D objects vizualization available with Windows only

The present version of Radia do not allow to display 3D objects with Linux and MacOSX. It will be integrated in the *.plot_geo()* method when available. A VTK visualization tool was developed by RadiaSoft: https://github.com/radiasoft/Radia-Examples. 

 ## Examples
 Basic examples are provided with this library:
 - PM dipole module: dl_module_test.py
 - Combined dipole-quadrupole: dq_test.py
 - Quadrupole: quad_test.py

 More elaborated examples are also available:
 - Quadrupole to sextupole cross-talk: quad_sext_crosstalk.py
 - A Jupyter notebook for quadrupoles: quadrupoles.ipynb

