# -----------------------------------------
# Multipole magnet Radia models library
#
# G Le Bec, ESRF, 2019
# ------------------------------------------

import radia as rad
import radia_util as rad_uti
import radia_mat_util as rad_mat
from matplotlib.pyplot import plot, show, xlabel, ylabel, figure, title, imshow, xticks, yticks, colorbar
import numpy as np
from math import pi, cos, sin, atan2, asin, atan, tan
from cmath import exp, polar
import pickle
from datetime import datetime
from itertools import accumulate
from copy import deepcopy
from random import uniform
from scipy.optimize import minimize


# -------------------------------------------
# Parameters for multipole electromagnet 
# -------------------------------------------
class Param:
    """Base class for multipole electromagnet parameters"""


# -------------------------------------------
# Parameters for quadrupole electromagnet yoke 
# -------------------------------------------
class QuadEMYokeParam(Param):
    """
    Radia Parameters for quadrupole electromagnets
    """
    def __init__(self, length=500, chamfer=None, profile_params=None, mesh_params=None, mat='xc6',
                 color=[1,0,0], axis='y'):
        """
        Main parameters for quadrupole magnet yokes
        :param length=500: yoke length (mm)
        :param chamfer=None: chamfer sizes [pole tip chamfer, pole_side chamfer]. This parameter is also used to define
                    the pole and yoke extremities with different mesh.
        :param profile_params=None:  profile parameters (see QuadEMYokeProfileParam)
        :param mesh_params=None: mesh parameters (see MeshExtTriParam)
        :param mat='xc6': material: 'xc6' (XC 6 / AISI 1006), 'feco' (FeCo), 'fecov' (FeCoV)
        :param color=[1,0,0]: yoke color
        :param axis='y': longitudinal axis
        """
        # --- Magnet type
        self.mag_type = '4-poles'
        # --- Yoke parameters
        self.length = length
        self.chamfer = chamfer
        # --- Profile parameters
        if profile_params is None:
            self.profile_params = QuadEMYokeProfileParam()
        else:
            self.profile_params = profile_params
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = mat
        # --- Quadrupole axis
        self.axis = axis
        # --- Color 
        self.color = color
        
    def build_qf1_parameters(self, mesh_params=None):
        """
        Set the parameters for an ESRF EBS QF1 quad yoke
        """
        # --- Magnet type
        self.mag_type = '4-poles'
        # --- Yoke parameters
        self.length = 295
        self.chamfer = [2.5, 18]
        # --- Profile parameters
        self.profile_params = QuadEMYokeProfileParam(pole_type='qf1')
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = '1300-100a'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [1, 0, 0]

    def build_qd2_qf4_qd5_parameters(self, mesh_params=None):
        """
        Set the parameters for an ESRF EBS QD2 ... QD5 quad yoke
        """
        # --- Magnet type
        self.mag_type = '4-poles'
        # --- Yoke parameters
        self.length = 212
        self.chamfer = [2.9, 18]
        # --- Profile parameters
        self.profile_params = QuadEMYokeProfileParam(pole_type='qf1_qd2_qd3_qf4_qd5')
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = '1300-100a'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [1, 0, 0]

    def build_qd3_parameters(self, mesh_params=None):
        """
        Set the parameters for an ESRF EBS QD3 quad yoke
        """
        # --- Magnet type
        self.mag_type = '4-poles'
        # --- Yoke parameters
        self.length = 162
        self.chamfer = [2.6, 18]
        # --- Profile parameters
        self.profile_params = QuadEMYokeProfileParam(pole_type='qd3')
        # --- Yoke thickness
        self.profile_params.w = 70
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = '1300-100a'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [1, 0, 0]

    def build_qf6_parameters(self, mesh_params=None):
        """
        Set the parameters for an ESRF EBS QF6 quad yoke
        """
        # --- Magnet type
        self.mag_type = '4-poles'
        # --- Yoke parameters
        self.length = 388
        self.chamfer = [0, 18]
        # --- Profile parameters
        self.profile_params = QuadEMYokeProfileParam(pole_type='qf6')
        # --- Yoke thickness
        self.profile_params.w = 75
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = 'xc6'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [1, 0, 0]

    def build_qf8_parameters(self, mesh_params=None):
        """
        Set the parameters for an ESRF EBS QF8 quad yoke
        """
        # --- Magnet type
        self.mag_type = '4-poles'
        # --- Yoke parameters
        self.length = 484
        self.chamfer = [0, 18]
        # --- Profile parameters
        self.profile_params = QuadEMYokeProfileParam(pole_type='qf8')
        # --- Yoke thickness
        self.profile_params.w = 75
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = 'xc6'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [1, 0, 0]


# -------------------------------------------
# Parameters for quadrupole electromagnet yoke profile
# -------------------------------------------
class QuadEMYokeProfileParam(Param):
    """
    Radia Parameters for quadrupole profiles
    """
    def __init__(self, r0=12.5, n_points=5, pole_shape=None, x0=12.5, r1=7, r2=80, r3=150, r4=40, w=70, theta_2=20,
                 theta_3=45, pole_type='hyperbola', backleg_type='45deg'):
        """
        Yoke profile parameters
        :param r0=12.5: bore radius [mm]
        :param n_points=5: number of points for the hyperbola if pole_shape is None
        :param pole_shape=None: displacements between the pole shape and the hyperbola defined as
                [dx_0 + 1j * dz_0, dx_1 + 1j * dz_1, ...]. Defines the number of points if not None
        :param x0=12.5: transverse position of the hyperbolic cut [mm]
        :param r1=7: length of the horizontal segment parallel to the symmetry plane [mm]
        :param r2=80: length of the segment between the pole tip and the coil [mm]
        :param r3=150: length of the segment close to the coil [mm]
        :param r4=40: length of the segment parallel to the coil with
        :param w=50: width of the yoke
        :param theta_2=20: angle between the segment of length r2 and the horizontal axis [deg]
        :param theta_3=45: angle between the segment of length r3 and the horizontal axis [deg]
        :param pole_type='hyperbola': Switch the type of pole profile:
                    -- 'hyperbola': hyperbolic profile (+shape) using the parameters above
                    -- 'qf1', 'qd2', ..., 'qf8': ESRF EBS quadrupole profiles
                    (to be completed with other pole profiles)
        :param backleg_type='45deg': Switch the backleg yoke profile
                    -- '45deg': Backleg yoke profile with 45 deg angles
                    (other backleg types to be implemented, e.g. circular)
        Please see Fig. 3 in the reference [PRAB 19, 052401 (2016)] for more details on the geometric parameters.
        The default parameters generate a high gradient quadrupole profile without pole shaping
        """
        # --- Hold the parameters
        self.r0 = r0
        self.pole_shape = pole_shape
        if pole_shape is None:
            self.n_points = n_points
        else:
            self.n_points = len(pole_shape)
        self.x0 = x0
        self.r1 = r1
        self.r2 = r2
        self.r3 = r3
        self.r4 = r4
        self.w = w
        self.theta_2 = theta_2
        self.theta_3 = theta_3
        self.pole_type = pole_type
        self.backleg_type = backleg_type


# -------------------------------------------
# Parameters for sextupole electromagnet yoke profile
# -------------------------------------------
class SextuEMYokeParam(Param):
    """
    Radia Parameters for sextupole electromagnets
    """
    def __init__(self, length=166, chamfer=None, profile_params=None, mesh_params=None, mat='xc6',
                 color=[0,0.5,0], axis='y'):
        """
        Main parameters for sextupole magnet yokes
        :param length=500: yoke length (mm)
        :param chamfer=None: chamfer sizes [pole tip chamfer, pole_side chamfer]. This parameter is also used to define
                    the pole and yoke extremities with different mesh.
        :param profile_params=None:  profile parameters (see SextuEMYokeProfileParam)
        :param mesh_params=None: mesh parameters (see MeshExtTriParam)
        :param mat='xc6': material: 'xc6' (XC 6 / AISI 1006), 'feco' (FeCo), 'fecov' (FeCoV)
        :param color=[1,0,0]: yoke color
        :param axis='y': longitudinal axis
        """
        # --- Magnet type
        self.mag_type = '6-poles'
        # --- Yoke parameters
        self.length = length
        self.chamfer = chamfer
        # --- Profile parameters
        if profile_params is None:
            self.profile_params = SextuEMYokeProfileParam()
        else:
            self.profile_params = profile_params
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = mat
        # --- Quadrupole axis
        self.axis = axis
        # --- Color
        self.color = color

    def build_sd1_parameters(self, mesh_params=None):
        """
        Set the parameters for an ESRF EBS SD1 sextu yoke
        """
        # --- Magnet type
        self.mag_type = '6-poles'
        # --- Yoke parameters
        self.length = 166
        self.chamfer = [0, 15]
        # --- Profile parameters
        self.profile_params = SextuEMYokeProfileParam(pole_type='sd1')
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = 'xc6'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [0, 0.5, 0]

    def build_sf2_parameters(self, mesh_params=None):
        """
        Set the parameters for an ESRF EBS SF2 sextu yoke
        """
        # --- Magnet type
        self.mag_type = '6-poles'
        # --- Yoke parameters
        self.length = 200
        self.chamfer = [0, 15]
        # --- Profile parameters
        self.profile_params = SextuEMYokeProfileParam(pole_type='sd1')
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = 'xc6'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [0, 0.5, 0]


# -------------------------------------------
# Parameters for quadrupole electromagnet yoke profile
# -------------------------------------------
class SextuEMYokeProfileParam(Param):
    """
    Radia Parameters for quadrupole profiles
    """
    def __init__(self, r0=19.2, n_points=5, pole_shape=None, x0=18.93, r1=6.3, r2=112, r3=161, r4=66, w=80,
                 theta_2=8.943, theta_3=30, pole_type='flat', backleg_type='30deg'):
        """
        Yoke profile parameters
        :param r0=19.2: bore radius [mm]
        :param n_points=5: number of points for the hyperbola if pole_shape is None
        :param pole_shape=None: displacements between the pole shape and the hyperbola defined as
                [dx_0 + 1j * dz_0, dx_1 + 1j * dz_1, ...]. Defines the number of points if not None
        :param x0=18.93: transverse position of the hyperbolic cut [mm]
        :param r1=6.3: length of the horizontal segment parallel to the symmetry plane [mm]
        :param r2=112: length of the segment between the pole tip and the coil [mm]
        :param r3=161: length of the segment close to the coil [mm]
        :param r4=66: length of the segment parallel to the coil with
        :param w=80: width of the yoke
        :param theta_2=8.943: angle between the segment of length r2 and the horizontal axis [deg]
        :param theta_3=30: angle between the segment of length r3 and the horizontal axis [deg]
        :param pole_type='flat': Switch the type of pole profile:
                    -- 'flat': flat pole tip
                    -- 'hyperbola': hyperbolic profile (+shape) using the parameters above
                    (to be completed with other pole profiles)
        :param backleg_type='30deg': Switch the backleg yoke profile
                    -- '30deg': Backleg yoke profile with 30 deg angles
                    (other backleg types to be implemented, e.g. circular)
        The default parameters generate an EBS sextupole profile with a flat pole tip
        """
        # --- Hold the parameters
        self.r0 = r0
        self.pole_shape = pole_shape
        if pole_shape is None:
            self.n_points = n_points
        else:
            self.n_points = len(pole_shape)
        self.x0 = x0
        self.r1 = r1
        self.r2 = r2
        self.r3 = r3
        self.r4 = r4
        self.w = w
        self.theta_2 = theta_2
        self.theta_3 = theta_3
        self.pole_type = pole_type
        self.backleg_type = backleg_type


# -------------------------------------------
# Parameters for octupole electromagnet yoke profile
# -------------------------------------------
class OctuEMYokeParam(Param):
    """
    Radia Parameters for sextupole electromagnets
    """
    def __init__(self, pole_length=90, yoke_length=34.6, chamfer=[0, 10], profile_params=None, mesh_params=None,
                 mat='xc6', color=[0,0,0.5], axis='y'):
        """
        Main parameters for octupole magnet yokes
        :param pole_length=90: yoke length (mm)
        :param yoke_length=34.6: yoke length (mm)
        :param chamfer=[0,10]: chamfer sizes [pole tip chamfer, yoke chamfer]. This parameter is also used to define
                    the pole and yoke extremities with different mesh. Ignored for EBS octupoles
        :param profile_params=None:  profile parameters (see OctuEMYokeProfileParam)
        :param mesh_params=None: mesh parameters (see MeshExtTriParam)
        :param mat='xc6': material: 'xc6' (XC 6 / AISI 1006), 'feco' (FeCo), 'fecov' (FeCoV)
        :param color=[0,0,0.5]: yoke color
        :param axis='y': longitudinal axis
        """
        # --- Magnet type
        self.mag_type = '8-poles'
        # --- Yoke parameters
        self.yoke_length = yoke_length
        self.pole_length = pole_length
        self.chamfer = chamfer
        # --- Profile parameters
        if profile_params is None:
            self.profile_params = OctuEMYokeProfileParam()
        else:
            self.profile_params = profile_params
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = mat
        # --- Quadrupole axis
        self.axis = axis
        # --- Color
        self.color = color

    def build_od1_parameters(self, mesh_params=None):
        """
        Set the parameters for an ESRF EBS octupole yoke
        """
        # --- Magnet type
        self.mag_type = '8-poles'
        # --- Yoke parameters
        self.length = 90
        # --- Profile parameters
        self.profile_params = OctuEMYokeProfileParam(pole_type='od1')
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = 'xc6'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [0, 0, 0.5]


# -------------------------------------------
# Parameters for octupole electromagnet yoke profile
# -------------------------------------------
class OctuEMYokeProfileParam(Param):
    """
    Radia Parameters for quadrupole profiles
    """
    def __init__(self, pole_type='od1'):
        """
        Yoke profile parameters -- To be completed for parametric description of the profile, not yet implemented
        :param pole_type='ebs': pole profile type
        """
        # --- Hold the parameters
        self.pole_type = pole_type


# -------------------------------------------
# Parameters for staggeed quadrupole yoke
# -------------------------------------------
class QuadStaggeredYokeParam(Param):

    def __init__(self, length=100, period=200, n_periods=2, profile_params=None, mesh_params=None,
                             mat='xc6', color=[1,0,0], axis='y'):
        """
        Initialize parameters for staggered quadrupole yoke
        :param length=100: pole length (mm)
        :param period=200: pole period (mm)
        :param n_period=2: number of periods
        :param profile_params=None: pole profile parameters
        :param mesh_params=None: mesh parameters
        :param mat='xc6': material
        :param color=[1,0,0]: pole color
        :param axis='y': yoke axis
        """
        self.length = length
        self.period = period
        self.n_periods = n_periods
        self.mesh_params = mesh_params
        self.mat = mat
        self.color = color
        self.axis = axis
        if profile_params is None:
            self.profile_params = QuadStaggeredYokeProfileParam()
        else:
            self.profile_params = profile_params
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params


# -------------------------------------------
# Parameters for staggered quadrupole yoke profile
# -------------------------------------------
class QuadStaggeredYokeProfileParam(Param):
    """
    Radia Parameters for quadrupole profiles
    """
    def __init__(self, r0=5, n_points=5, pole_shape=None, x0=5, r1=20, r2=20,r3=20, theta=pi/4, pole_type='hyperbola'):
        """
        Yoke profile parameters
        :param r0=12.5: bore radius [mm]
        :param n_points=5: number of points for the hyperbola if pole_shape is None
        :param pole_shape=None: displacements between the pole shape and the hyperbola defined as
                [dx_0 + 1j * dz_0, dx_1 + 1j * dz_1, ...]. Defines the number of points if not None
        :param x0=5: transverse position of the hyperbolic cut [mm]
        :param r1=20: length of segment 1 (horizontal) [mm]
        :param r2=20: length of segment 2 (angle theta)[mm]
        :param r3=20: length of segment 3 (vertical )[mm]
        :param theta=pi/4: angle of segment 2 (rad)
        :param pole_type='hyperbola': Switch the type of pole profile:
                    -- 'hyperbola': hyperbolic profile (+shape) using the parameters above
                    (to be completed with other pole profiles)
        """
        # --- Hold the parameters
        self.r0 = r0
        self.pole_shape = pole_shape
        if pole_shape is None:
            self.n_points = n_points
        else:
            self.n_points = len(pole_shape)
        self.x0 = x0
        self.r1 = r1
        self.r2 = r2
        self.r3 = r3
        self.theta = theta
        self.pole_type = pole_type


# -------------------------------------------
# Parameters for staggered quadrupole PM blocks
# -------------------------------------------
class QuadStaggeredPMParam(Param):
    def __init__(self, r0=20, width=10, height=20, chamfer=10, br=1.3, angle=pi/6, mat='ndfeb', mesh_params=None, color=[0,0,1]):
        """Parameters for staggered quadrupole PM blocks
        :param r0=20: Distance from beam axis to PM (mm)
        :param width=10: PM block width (mm)
        :param height=20: PM block height (mm)
        :param chamfer=10: PM block chamfer (mm)
        :param br=1.6: remanent field (T)
        :param angle=pi/6: direction of the magnetization, vertical if 0 (rad)
        :param mat='ndfeb': material
        :param mesh_params=None: mesh parameters (see MeshExtTriParam)
        :param color=[1,0,1]: PM block color
        """
        self.r0 = r0
        self.width = width
        self.height = height
        self.chamfer = chamfer
        self.br = br
        self.angle = angle
        self.mat = mat
        self.color = color
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params


# -------------------------------------------
# Parameters for dipole-quadrupole electromagnet yoke
# -------------------------------------------
class DipoleQuadEMYokeParam(Param):
    def __init__(self, length=1000, r_bend=None, profile_params=None, mesh_params=None, mat='xc6', end='rect',
                 color=[0,0,1], axis='y'):
        """
        Main parameters for dipole-quadrupole magnet yokes
        :param length=1000: yoke length (mm)
        :param r_bend=None: radius of curvature of the magnet (set to None for straight magnets)
        :param profile_params=None:  profile parameters (see DipoleQuadYokeProfileParam)
        :param mesh_params=None: mesh parameters (see MeshExtTriParam)
        :param mat='xc6': material: 'xc6' (XC 6 / AISI 1006), 'feco' (FeCo), 'fecov' (FeCoV)
        :param end='rect': magnet extremities
                            -- 'rect': rectangular
                            -- 'sector': sector magnet
        :param color=[0,0,1]: yoke color
        :param axis='y': longitudinal axis
        """
        # --- Magnet type
        self.mag_type = '2-4-poles'
        # --- Yoke parameters
        self.length = length
        self.chamfer = None
        self.r_bend = r_bend
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
        else:
            self.mesh_params = mesh_params
        # --- Profile parameters
        if profile_params is None:
            self.build_dq1_parameters()
        else:
            self.profile_params = profile_params
        # --- Quadrupole axis
        self.axis = axis
        # -- Extremities
        self.end = end
        # --- Color
        self.color = color
        # --- Material
        self.mat = mat

    def build_dq1_parameters(self, mesh_params=None):
        """
        Initialize yoke parameters for EBS DQ1 dipole-quadrupole
        :param mesh_params=None: mesh parameters
        """
        # --- Magnet type
        self.mag_type = '2-4-poles'
        # --- Yoke parameters
        self.length = 1028
        self.chamfer = None
        self.r_bend = 35206
        self.end = 'rect'
        # --- Profile parameters
        self.profile_params = DipoleQuadEMYokeProfileParam(pole_type='dq1')
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
            self.mesh_params.long_sub = [10, 1]
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = 'xc6'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [0, 0, 1]

    def build_dq2_parameters(self, mesh_params=None):
        """
        Initialize yoke parameters for EBS DQ2 dipole-quadrupole
        :param mesh_params=None: mesh parameters
        """
        # --- Magnet type
        self.mag_type = '2-4-poles'
        # --- Yoke parameters
        self.length = 800
        self.chamfer = None
        self.r_bend = 50955
        self.end = 'rect'
        # --- Profile parameters
        self.profile_params = DipoleQuadEMYokeProfileParam(pole_type='dq2')
        # --- Subdivision and mesh parameters
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam()
            self.mesh_params.long_sub = [10, 1]
        else:
            self.mesh_params = mesh_params
        # --- Material
        self.mat = 'xc6'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [0, 0, 1]


# -------------------------------------------
# Parameters for PM dipole module yoke and pole
# -------------------------------------------
class DipoleModulePMYokeParam(Param):
    def __init__(self, yoke_length=337, pole_length=353.6, profile_params=None, yoke_mesh_params=None,
                 yoke_mat='xc6', pole_mat='armco', pole_mesh_params=None, color=[0,0.5,1], axis='y'):
        """
        Main parameters for dipole-quadrupole magnet yokes
        :param length=1000: yoke length (mm)
        :param r_bend=None: radius of curvature of the magnet (set to None for straight magnets)
        :param profile_params=None:  profile parameters (see DipoleQuadYokeProfileParam)
        :param mesh_params=None: mesh parameters (see MeshExtTriParam)
        :param yoke_mat='xc6': backleg yoke material material
        :param pole_mat='armco': pole material
        :param end='rect': magnet extremities
                            -- 'rect': rectangular
                            -- 'sector': sector magnet
        :param color=[0,0,1]: yoke color
        :param axis='y': longitudinal axis
        """
        # --- Magnet type
        self.mag_type = '2-poles'
        # --- Yoke parameters
        self.yoke_length = yoke_length
        self.pole_length = pole_length
        # --- Subdivision and mesh parameters
        if pole_mesh_params is None:
            self.pole_mesh_params = MeshExtTriParam()
        else:
            self.pole_mesh_params = pole_mesh_params
        if yoke_mesh_params is None:
            self.yoke_mesh_params = MeshExtTriParam()
        else:
            self.yoke_mesh_params = yoke_mesh_params
        # --- Profile parameters
        if profile_params is None:
            self.build_dl_high_field()
        else:
            self.profile_params = profile_params
        # --- Magnet axis
        self.axis = axis
        # --- Color
        self.color = color
        # --- Material
        self.yoke_mat = yoke_mat
        self.pole_mat = pole_mat

    def build_dl_high_field(self):
        # --- Magnet type
        self.mag_type = '2-poles'
        # --- Yoke parameters
        self.yoke_length = 337
        self.pole_length = 353.6
        # --- Profile parameters
        self.profile_params = DipoleModulePMYokeProfileParam(pole_type='dl-high-field')
        # --- Subdivision and mesh parameters
        self.yoke_mesh_params = MeshExtTriParam()
        self.yoke_mesh_params.long_sub = [10, 1]
        self.pole_mesh_params = MeshExtTriParam(area_max=100)
        self.pole_mesh_params.long_sub = [10, 1]
        # --- Material
        self.yoke_mat = 'xc6'
        self.pole_mat = 'armco'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [0, 0.5, 1]

    def build_dl_low_field(self):
        # --- Magnet type
        self.mag_type = '2-poles'
        # --- Yoke parameters
        self.yoke_length = 337
        self.pole_length = 353.6
        # --- Profile parameters
        self.profile_params = DipoleModulePMYokeProfileParam(pole_type='dl-low-field')
        # --- Subdivision and mesh parameters
        self.yoke_mesh_params = MeshExtTriParam()
        self.yoke_mesh_params.long_sub = [10, 1]
        self.pole_mesh_params = MeshExtTriParam(area_max=100)
        self.pole_mesh_params.long_sub = [10, 1]
        # --- Material
        self.yoke_mat = 'xc6'
        self.pole_mat = 'armco'
        # --- Quadrupole axis
        self.axis = 'y'
        # --- Color
        self.color = [0, 0.5, 1]


# -------------------------------------------
# Parameters for PM dipole module PM blocks
# -------------------------------------------
class DipoleModulePMBlocksParam(Param):
    def __init__(self, top_width=70, top_height=22,top_full_length=308, top_n_blocks=4, side_width=22,
                 side_height=35, side_full_length=337, side_n_blocks=1, yoke_points=[-3,-8], mat='sm2co17', br=1.1,
                 temp=20, temp_coef=-0.00033, sub=None, color = [1,0.5,0]):
        """
        Initialize parameters for Dipole PM blocks
        :param top_width=70: width of the top blocks (mm)
        :param top_height=22: height of the top blocks (mm)
        :param top_full_length=308: total length of the top magnets (mm)
        :param top_n_blocks=1: number of upper blocks
        :param side_width=22: width of the side blocks (mm)
        :param side_height=35: height of the side magnets
        :param side_full_length=322: total length of the side magnets (mm)
        :param side_n_blocks=1: number of side blocks
        :param yoke_points=[-3,-8]: points on the pole profile defining the PM position [inner, outer]
        :param mat='sm2co17': PM material
        :param br=1.1: remanent field (T)
        :param sub=None: subdivision parameters (see MeshSubParam)
        :param color=[0,1,0]: block color
        """
        # Geometric parameters
        self.top_width = top_width
        self.top_height = top_height
        self.top_full_length = top_full_length
        self.top_n_blocks = top_n_blocks
        self.side_width = side_width
        self.side_height = side_height
        self.side_full_length = side_full_length
        self.side_n_blocks = side_n_blocks
        self.yoke_points = yoke_points
        # Material
        self.mat = mat
        self.br = br
        self.temp = temp
        self.temp_coef = temp_coef
        # Other
        if sub is not None:
            self.sub = sub
        else:
            self.sub = MeshSubParam(3,3,3)
        self.color = color

    def build_dl_5(self):
        """
        Set parameters for DL1_1 PM blocks
        """
        self.top_width = 70
        self.top_height = 22
        self.top_full_length = 308
        self.top_n_blocks = 14
        self.side_width = 22
        self.side_height = 35
        self.side_full_length = 337
        self.side_n_blocks = 14
        self.yoke_points = [-3,-8]
        # Material
        self.mat = 'sm2co17'
        self.br = 1.1
        self.sub = MeshSubParam(3, 3, 3)
        self.color = [1,0.5,0]

    def build_dl_4(self):
        """
        Set parameters for DL1_2 PM blocks
        """
        self.top_width = 70
        self.top_height = 22
        self.top_full_length = 173.25
        self.top_n_blocks = 14
        self.side_width = 22
        self.side_height = 35
        self.side_full_length = 202.2
        self.side_n_blocks = 14
        self.yoke_points = [-3,-8]
        # Material
        self.mat = 'sm2co17'
        self.br = 1.1
        self.sub = MeshSubParam(3, 3, 3)
        self.color = [1,0.5,0]

    def build_dl_3(self):
        """
        Set parameters for DL1_3 PM blocks
        """
        self.top_width = 70
        self.top_height = 22
        self.top_full_length = 154
        self.top_n_blocks = 14
        self.side_width = 22
        self.side_height = 35
        self.side_full_length = 134.8
        self.side_n_blocks = 14
        self.yoke_points = [-3, -8]
        # Material
        self.mat = 'sm2co17'
        self.br = 1.1
        self.sub = MeshSubParam(3, 3, 3)
        self.color = [1,0.5,0]

    def build_dl_2(self):
        """
        Set parameters for DL1_4 PM blocks
        """
        self.top_width = 70
        self.top_height = 22
        self.top_full_length = 154
        self.top_n_blocks = 14
        self.side_width = 22
        self.side_height = 35
        self.side_full_length = 67.4
        self.side_n_blocks = 14
        self.yoke_points = [-3, -8]
        # Material
        self.mat = 'sm2co17'
        self.br = 1.1
        self.sub = MeshSubParam(3, 3, 3)
        self.color = [1,0.5,0]

    def build_dl_1(self):
        """
        Set parameters for DL1_5 PM blocks
        """
        self.top_width = 70
        self.top_height = 22
        self.top_full_length = 175
        self.top_n_blocks = 14
        self.side_height = None
        self.side_full_length = None
        self.side_n_blocks = None
        self.yoke_points = [-1, -6] # Needed to attach the shims
        # Material
        self.mat = 'sm2co17'
        self.br = 1.1
        self.sub = MeshSubParam(3, 3, 3)
        self.color = [1,0.5,0]


# -------------------------------------------
# Parameters for PM dipole module shims
# -------------------------------------------
class DipoleModulePMShimsParams(Param):
    def __init__(self, thickness=4.7, mat=[[0.1362,0.02605,0.04917],[2118,63.06,17.138]], temp=20, temp_coef=-0.026,
                 sub=None, color=[1,0,0.5]):
        """
        Initialise thermal shims parameters
        :param thickness=2: shim thichness (mm)
        :param mat: [[br_0...br_2], [ksi_0...ksi2]] parameters for B(H) curve (default: Thermoflux)
        :param temp=20: temperature (deg C)
        :param temp_coef=-0.026: temperature coefficient
        :param sub=None: subdivision parameters (see MeshSubParam)
        :param color=[1,0,0.5]:
        """
        self.thickness = thickness
        self.mat = mat
        self.temp = temp
        self.temp_coef = temp_coef
        self.color = color
        if sub is None:
            self.sub = MeshSubParam()
        else:
            self.sub = sub

    def build_dl_5(self):
        """
        Set the shim parameters for DL type 5 modules
        """
        self.thickness = 4.7

    def build_dl_4(self):
        """
        Set the shim parameters for DL type 4 modules
        """
        self.thickness = 4.7

    def build_dl_3(self):
        """
        Set the shim parameters for DL type 3 modules
        """
        self.thickness = 4.7

    def build_dl_2(self):
        """
        Set the shim parameters for DL type 2 modules
        """
        self.thickness = 4.7

    def build_dl_1(self):
        """
        Set the shim parameters for DL type 1 modules
        """
        self.thickness = 0.8


# -------------------------------------------
# Parameters for dipole-quadrupole yoke profile
# -------------------------------------------
class DipoleQuadEMYokeProfileParam(Param):
    def __init__(self, r0=19, pole_type='dq1'):
        """
        Parameters for dipole-quadrupole yoke profile
        :param r0=19: bore radius (mm)
        :param pole_type='dq1': type of the pole
        """
        # --- Hold the parameter
        self.r0 = r0
        self.pole_type = pole_type


# -------------------------------------------
# Parameters for dipole module yoke profile
# -------------------------------------------
class DipoleModulePMYokeProfileParam(Param):
    def __init__(self, r0=12.25, pole_type='dl-high-field'):
        """
        Parameters for dipole-quadrupole yoke profile
        :param r0=19: bore radius (mm)
        :param pole_type='dq1': type of the pole
        """
        # --- Hold the parameter
        self.r0 = r0
        self.pole_type = pole_type


# -------------------------------------------
# Parameters for an electromagnet coil
# -------------------------------------------
class CoilParam(Param):
    def __init__(self, current=100, height=130, width=25, dr=5, pole_width=None, pole_length=None, coil_segments=4,
                 axis='y', coil_type='race_track', color=[1,0.5,0], turns=80, cond=None, coil_layers=None):
        """
        Initialize parameters for a electromagnet coil
        :param current=100: current (A)
        :param height=130: coil height (mm)
        :param width=25: coil width (mm)
        :param dr=5: coil to yoke distance (mm)
        :param pole_width=None: pole width to use if yoke parameters are not provided
        :param pole_length=None: pole length to use if yoke parameters are not provided
        :param pole_width=100: width of the coil
        :param coil_segments=4: number of segments per arc
        :param axis='y': longitudinal axis
        :param coil_type='race_track': coil type (multi layer coils to be implemented)
        :param color=[1,0.5,0]: coil color
        :param turns=80: number of turns (for racetrack coils)
        :param cond=None: conductor parameters [size_h, size_v, hole, corner radius, insulator thickness]
        :param coil_layers=None: definition of the coil layers
                [number of layers,
                [number of conductors layer 0, offset layer 0],
                [number of conductors layer 1, offset layer 1],
                ...]
        """

        # --- Generic parameters
        self.coil_type = coil_type
        self.current = current
        self.coil_segments = coil_segments
        self.pole_width = pole_width
        self.pole_length = pole_length
        self.axis = axis
        self.color = color
        self.current_sign = 1
        self.yoke_points = None

        # --- Racetrack coils
        self.height = height
        self.width = width
        self.dr = dr
        self.turns = turns

        # --- Multi layer coils
        self.cond = cond
        self.coil_layers = coil_layers

    # Moderate gradient quads
    def build_qf1_parameters(self):
        """
        Coil parameters for ESRF EBS QF1 magnets
        """
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 88.43
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]

        # --- Racetrack coils
        self.height = 119
        self.width = 28
        self.dr = 3
        self.turns = 68

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None

    def build_qd2_qf4_qd5_parameters(self):
        """
        Coil parameters for ESRF EBS QD2 ... QD5 magnets
        """
        # same as QF1
        self.build_qf1_parameters()
        self.current = 90 # QD2 current

    def build_qd3_parameters(self):
        """
        Coil parameters for ESRF EBS QD3 magnets
        """
        # same as QF1
        self.build_qf1_parameters()
        self.current = 80.7

    # High gradient quads
    def build_qf6_parameters(self):
        """
        Coil parameters for ESRF EBS QF1 magnets
        """
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 92
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]

        # --- Racetrack coils
        self.height = 140
        self.width = 28
        self.dr = 3
        self.turns = 77

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None

    def build_qf8_parameters(self):
        """
        Coil parameters for ESRF EBS QF1 magnets
        """
        self.build_qf6_parameters()

    # Dipole-quads
    def build_dq1_main_parameters(self):
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 85.5
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]
        self.yoke_points = [0, 19]

        # --- Racetrack coils
        self.height = 127.5
        self.width = 30
        self.dr = 3
        self.turns = 65

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None

    def build_dq1_aux_parameters(self):
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 85.5
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]
        self.yoke_points = [5, 17]
        self.current_sign = 1

        # --- Racetrack coils
        self.height = 90
        self.width = 7.5
        self.dr = 3
        self.turns = 12

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None

    def build_dq1_corr_parameters(self):
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 0
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]
        self.current_sign = -1
        self.yoke_points = [5, 17]

        # --- Racetrack coils
        self.height = 45
        self.width = 12
        self.dr = 3
        self.turns = 120

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None

    def build_dq2_main_parameters(self):
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 90
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]
        self.yoke_points = [-1, 19]

        # --- Racetrack coils
        self.height = 105
        self.width = 30
        self.dr = 3
        self.turns = 65

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None

    def build_dq2_aux_parameters(self):
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 90
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]
        self.yoke_points = [5, 17]
        self.current_sign = 1

        # --- Racetrack coils
        self.height = 52.5
        self.width = 15
        self.dr = 3
        self.turns = 13

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None

    def build_dq2_corr_parameters(self):
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 0
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]
        self.current_sign = -1
        self.yoke_points = [5, 17]

        # --- Racetrack coils
        self.height = 33
        self.width = 15
        self.dr = 3
        self.turns = 88

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None

    # Sextupoles
    def build_sd1_parameters(self):
        """
            Coil parameters for ESRF EBS SD1 magnets
        """
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 60
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]

        # --- Racetrack coils
        self.height = 91
        self.width = 28
        self.dr = 3
        self.turns = 51

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None

    def build_sf2_parameters(self):
        """
            Coil parameters for ESRF EBS SF2 magnets
        """
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 60
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]

        # --- Racetrack coils
        self.height = 91
        self.width = 28
        self.dr = 3
        self.turns = 51

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None

    # Octupoles
    def build_od1_parameters(self):
        """
        Coil parameters for ESRF EBS OD1 octupole magnets
        """
        # --- Generic parameters
        self.coil_type = 'race_track'
        self.current = 50
        self.coil_segments = 4
        self.pole_width = None
        self.pole_length = None
        self.axis = None
        self.color = [1, 0.5, 0]

        # --- Racetrack coils
        self.height = 45
        self.width = 28
        self.dr = 3
        self.turns = 36

        # --- Multi layer coils
        self.cond = None
        self.coil_layers = None


# -------------------------------------------
# Parameters for generalized Halbach magnets
# -------------------------------------------
class GeneralizedHalbachPMParams(Param):
    def __init__(self, 
                 order=2, 
                 r0=10, 
                 r1=50, 
                 v_aperture=None,
                 n_blocks=16, 
                 length=100, 
                 dr=None, 
                 angles=None,
                 mat='sm2co17', 
                 br=1.1,
                 color=[0,0.5,1], 
                 axis='y', 
                 mesh_params=None,
                 ) -> None:
        """
        Parameters for a generalized Halbach multipole
        :param order: order of the multipole (1: dipole, 2: quadrupole, 3: sextupole, etc.)
        :param r0: inner radius
        :param r1: outer radius
        :param v_aperture: vertical aperture if needed (or 0 or None if no aperture)
        :param n_blocks: number of PM blocks
        :param length: magnet length
        :param dr: inner radius offset for all blocks (the length of dr should be equal to n_blocks / 2 due to the up/down symmetry)
        :param angles: magnetization angles (the length of angles should be equal to n_blovks / 2)
        :param mat: permanent magnet material
        :param br: remanent field
        :param color: magnet color
        :param axis: longitudinal axis
        :param mesh_params: mesh parameters (see MeshExtTriParam)
        """
        
        self.order = order
        self.r0 = r0
        self.r1 = r1
        self.v_aperture = v_aperture
        self.n_blocks = n_blocks
        self.length = length
        self.dr = dr
        self.mat = mat
        self.br = br
        self.angles = angles
        self.color = color
        self.axis = axis
        if mesh_params is not None:
            self.mesh_params = mesh_params
        else:
            self.mesh_params = MeshExtTriParam()
        

# -------------------------------------------
# Parameters for extruded triangle mesh
# -------------------------------------------
class MeshExtTriParam(Param):
    def __init__(self, area_max=500, tri_ang_min=20, long_sub=[3, 1], long_sub_ext=[2,1], pole_tip_sub=1,
                 pole_yoke_sub=5):
        """
        Initialize mesh parameters for extruded triangle mesh
        :param area_max=500: max area for triangles (mm^2)
        :param tri_ang_min=20: minimum angle (deg)
        :param long_sub=[3,1]: longitudinal segmentation [number of elems, length of last elem / length of first elem]
        :param long_sub_ext=[2,1]: longitudinal segmentation of the extremity [number, last/first]
        :param pole_tip_sub=1: number of elements in pole tip segments
        :param pole_yoke_sub=5: number of elements at the interface between the pole and the backleg yoke
        """
        # --- Hold the parameters
        self.area_max = area_max
        self.tri_ang_min = tri_ang_min
        self.long_sub = long_sub
        self.long_sub_ext = long_sub_ext
        self.pole_tip_sub = pole_tip_sub
        self.pole_yoke_sub = pole_yoke_sub


# -------------------------------------------
# Parameters for subdivisions
# -------------------------------------------
class MeshSubParam(Param):
    def __init__(self, kx=1, ky=1, kz=1):
        """
        Initialize mesh parameters for extruded triangle mesh
        :param kx=1: Subdivision number in the x direction
        :param ky=1: Subdivision number in the x direction
        :param kz=1: Subdivision number in the x direction
        """
        # --- Hold the parameters
        self.sub = [kx, ky, kz]


# -------------------------------------------
# Precision parameters for Radia solver
# -------------------------------------------
class PrecParam(Param):
    def __init__(self, tolerance=1e-5, max_iter=10000):
        """
        Initialize precision parameters for the Radia solver
        :param tolerance: tolerance (default = 1e-5)
        :param max_iter:  maximum number of iterations (default=10000)
        """
        self.tolerance = tolerance
        self.max_iter = max_iter


# -------------------------------------------
# Parameters for Electromagnet quads
# -------------------------------------------
class QuadEMParam(Param):
    def __init__(self, yoke_params=None, coil_params=None, quad_type='qf8', prec_params=None, axis=None):
        """
        Initialize a quadrupole parameter object
        :param yoke_parameters=None: yoke parameters, see QuadEMYokeParam
        :param coil_params=None: coil parameters, see CoilParam
        :param quad_type='qf8': type of quadrupole to build
        :param prec_param=None: precision parameters, see PrecParam
        :param axis=none: magnet axis (set to 'y' i None)
        """
        self.yoke_params = yoke_params
        self.coil_params = coil_params
        if prec_params is not None:
            self.prec_params = prec_params
        else:
            self.prec_params = PrecParam()
        self.quad_type = quad_type
        if yoke_params is None or coil_params is None:
            self.build()
        if axis is not None:
            self.yoke_params.axis = axis

    def build(self):
        """
        Build yoke parameters according to the specs
        """
        try:
            if self.quad_type == 'qf1':
                self.build_qf1_parameters()
            elif self.quad_type == 'qd2_qf4_qd5' or self.quad_type == 'qd2' or self.quad_type == 'qf4' \
                    or self.quad_type == 'qd5':
                self.build_qd2_qd3_qf4_qd5_parameters()
            elif self.quad_type == 'qd3':
                self.build_qd3_parameters()
            elif self.quad_type == 'qf6':
                self.build_qf6_parameters()
            elif self.quad_type == 'qf8':
                self.build_qf8_parameters()
        except:
            raise Exception('QuadEMParam.build() failed')

    def build_qf1_parameters(self):
        """
        Build parameters for an ESRF EBS QF1
        """
        self.quad_type = 'qf1'
        # Yoke
        self.yoke_params =  QuadEMYokeParam()
        self.yoke_params.build_qf1_parameters()
        # Coil
        self.coil_params = CoilParam()
        self.coil_params.build_qf1_parameters()

    def build_qd2_qd3_qf4_qd5_parameters(self):
        """
        Build parameters for an ESRF EBS QD2 ... QD5
        """
        self.quad_type = 'qf1_qd2_qd3_qf4_qd5'
        # Yoke
        self.yoke_params =  QuadEMYokeParam()
        self.yoke_params.build_qd2_qf4_qd5_parameters()
        # Coil
        self.coil_params = CoilParam()
        self.coil_params.build_qd2_qf4_qd5_parameters()

    def build_qd3_parameters(self):
        """
        Build parameters for an ESRF EBS QD3
        """
        self.quad_type = 'qd3'
        # Yoke
        self.yoke_params =  QuadEMYokeParam()
        self.yoke_params.build_qd3_parameters()
        # Coil
        self.coil_params = CoilParam()
        self.coil_params.build_qd3_parameters()

    def build_qf6_parameters(self):
        """
        Build parameters for an ESRF EBS QF6
        """
        self.quad_type = 'qf6'
        # Yoke
        self.yoke_params =  QuadEMYokeParam()
        self.yoke_params.build_qf6_parameters()
        # Coil
        self.coil_params = CoilParam()
        self.coil_params.build_qf6_parameters()

    def build_qf8_parameters(self):
        """
        Build parameters for an ESRF EBS QF8
        """
        self.quad_type = 'qf8'
        # Yoke
        self.yoke_params =  QuadEMYokeParam()
        self.yoke_params.build_qf8_parameters()
        # Coil
        self.coil_params = CoilParam()
        self.coil_params.build_qf8_parameters()

    def set_mesh(self, mesh_params):
        """
        change the mesh parameters
        :param mesh_params:
        """
        try:
            self.yoke_params.mesh_params = mesh_params
        except AttributeError:
            raise Exception('Attribute error in set_mesh()')

    def set_material(self, mat_name):
        """
        Change the material of the yoke
        :param mat_name: name of the magnetic material
        """
        try:
            self.yoke_params.mat = mat_name

        except AttributeError:
            raise Exception('Attribute error in set_material()')

    def refine_mesh(self, long_sub_n=3, long_sub_n_ext=2, area_max=500):
        """
        Refine the mesh according to the specifications
        :param long_sub_n=3: number of longitudinal segments on 1/2 quad, w/o extremities
        :param long_sub_n_ext=2: number of longitudinal segments in extremities
        :param area_max=250: maximum triangle area
        """
        try:
            self.yoke_params.mesh_params.long_sub = [long_sub_n, 1]
            self.yoke_params.mesh_params.long_sub_ext = [long_sub_n_ext, 1]
            self.yoke_params.mesh_params.area_max = area_max
        except AttributeError:
            raise Exception('Attribute error in refine_mesh()')

# -------------------------------------------
# Parameters for Dipole-quads
# -------------------------------------------
class DipoleQuadEMParam(Param):
    def __init__(self, i_main=None, i_corr=None,yoke_params=None, main_coil_params=None, aux_coil_params=None,
                 dipole_quad_type=None, prec_params=None, mesh_params=None, axis=None):
        """
        Initialize dipole-quadrupole parameters
        :param i_main=None: main current (A) (Set to the default current if None.)
        :param i_corr=None: corrector current (A) (Set to zero if None)
        :param yoke_params=None: yoke parameters (see DipoleQuadEMYokeParam)
        :param main_coil_params=None: main coil parameters (see CoilParam)
        :param aux_coil_params=None: auxiliary coil parameters (see CoilParam)
        :param dipole_quad_type=None: dipole-quad type (e.g. 'dq1' for EBS DQ1 magnet)
        :param prec_params=None: precision parametres (see PrecParam)
        :param mesh_params=None: mesh parameters (see MeshParam)
        :param axis=None: magnet axis (set to 'y' if None)
        """
        self.yoke_params = yoke_params
        self.main_coil_params = main_coil_params
        self.aux_coil_params = aux_coil_params
        self.dipole_quad_type = dipole_quad_type
        if prec_params is not None:
            self.prec_params = prec_params
        else:
            self.prec_params = PrecParam()
        if dipole_quad_type == 'dq1':
            self.build_dq1_parameters(mesh_params=mesh_params)
        elif dipole_quad_type == 'dq2':
            self.build_dq2_parameters(mesh_params=mesh_params)
        elif yoke_params is None or main_coil_params is None or aux_coil_params is None:
            self.build_dq1_parameters(mesh_params=mesh_params)
        if axis is not None:
            self.yoke_params.axis = axis
        if i_main is not None:
            self.main_coil_params.current = i_main
            self.aux_coil_params.current = i_main
        if i_corr is not None:
            self.corr_coil_params.current = i_corr

    def build_dq1_parameters(self, mesh_params=None):
        """
        Initialize parameters for EBS DQ1 magnets
        """
        self.dipole_quad_type = 'dq1'
        # Yoke
        self.yoke_params = DipoleQuadEMYokeParam()
        self.yoke_params.build_dq1_parameters(mesh_params=mesh_params)
        # Main coil
        self.main_coil_params = CoilParam()
        self.main_coil_params.build_dq1_main_parameters()
        # Auxiliary coil
        self.aux_coil_params = CoilParam()
        self.aux_coil_params.build_dq1_aux_parameters()
        # Correction coil
        self.corr_coil_params = CoilParam()
        self.corr_coil_params.build_dq1_corr_parameters()

    def build_dq2_parameters(self, mesh_params=None):
        """
        Initialize parameters for EBS DQ2 magnets
        """
        self.dipole_quad_type = 'dq2'
        # Yoke
        self.yoke_params = DipoleQuadEMYokeParam()
        self.yoke_params.build_dq2_parameters(mesh_params=mesh_params)
        # Main coil
        self.main_coil_params = CoilParam()
        self.main_coil_params.build_dq2_main_parameters()
        # Auxiliary coil
        self.aux_coil_params = CoilParam()
        self.aux_coil_params.build_dq2_aux_parameters()
        # Correction coil
        self.corr_coil_params = CoilParam()
        self.corr_coil_params.build_dq2_corr_parameters()

    def set_current(self, current):
        """
        Change the current parameters in the main and auxiliary coils
        :param current: current (A)
        """
        self.main_coil_params.current = current
        self.aux_coil_params.current = current

    def refine_mesh(self, long_sub_n=10, area_max=500):
        """
        Refine the mesh of the yoke
        :param long_sub_n=10: Number of segments in the longitudinal direction
        :param area_max=500: Maximum area of triangles
        """
        self.yoke_params.mesh_params.area_max = area_max
        self.yoke_params.mesh_params.long_sub = [long_sub_n, 1]


# -------------------------------------------
# Parameters for Electromagnet sextupoles
# -------------------------------------------
class SextuEMParam(Param):
    def __init__(self, yoke_params=None, coil_params=None, sextu_type='sd1', prec_params=None, axis=None):
        """
        Initialize a quadrupole parameter object
        :param yoke_parameters=None: yoke parameters, see SextuEMYokeParam
        :param coil_params=None: coil parameters, see CoilParam
        :param sextu_type='sd1': type of quadrupole to build
        :param prec_param=None: precision parameters, see PrecParam
        :param axis=none: magnet axis (set to 'y' i None)
        """
        self.yoke_params = yoke_params
        self.coil_params = coil_params
        if prec_params is not None:
            self.prec_params = prec_params
        else:
            self.prec_params = PrecParam()
        self.sextu_type = sextu_type
        if yoke_params is None or coil_params is None:
            self.build()
        if axis is not None:
            self.yoke_params.axis = axis

    def build(self):
        """
        Build yoke parameters according to the specs
        """
        try:
            if self.sextu_type == 'sd1':
                self.build_sd1_parameters()
            elif self.sextu_type == 'sf2':
                self.build_sf2_parameters()
            else:
                print('Warning: Unknown sextupole type, SD1 magnet built')
                self.build_sd1_parameters()
        except AttributeError:
            raise Exception('SextuEMParam.build() failed')

    def build_sd1_parameters(self):
        """
        Build parameters for an ESRF EBS SD1 sextupole
        """
        self.sextu_type = 'sd1'
        # Yoke
        self.yoke_params = SextuEMYokeParam()
        self.yoke_params.build_sd1_parameters()
        # Coil
        self.coil_params = CoilParam()
        self.coil_params.build_sd1_parameters()

    def build_sf2_parameters(self):
        """
        Build parameters for an ESRF EBS SF2 sextupole
        """
        self.sextu_type = 'sf2'
        # Yoke
        self.yoke_params = SextuEMYokeParam()
        self.yoke_params.build_sf2_parameters()
        # Coil
        self.coil_params = CoilParam()
        self.coil_params.build_sf2_parameters()

    def refine_mesh(self, long_sub_n=3, long_sub_n_ext=2, area_max=500):
        """
        Refine the mesh according to the specifications
        :param long_sub_n=3: number of longitudinal segments on 1/2 quad, w/o extremities
        :param long_sub_n_ext=2: number of longitudinal segments in extremities
        :param area_max=250: maximum triangle area
        """
        try:
            self.yoke_params.mesh_params.long_sub = [long_sub_n, 1]
            self.yoke_params.mesh_params.long_sub_ext = [long_sub_n_ext, 1]
            self.yoke_params.mesh_params.area_max = area_max
        except AttributeError:
            raise Exception('Attribute error in refine_mesh()')


# -------------------------------------------
# Parameters for Electromagnet octupoles
# -------------------------------------------
class OctuEMParam(Param):
    def __init__(self, yoke_params=None, coil_params=None, octu_type='od1', prec_params=None, axis=None):
        """
        Initialize a quadrupole parameter object
        :param yoke_parameters=None: yoke parameters, see SextuEMYokeParam
        :param coil_params=None: coil parameters, see CoilParam
        :param sextu_type='sd1': type of quadrupole to build
        :param prec_param=None: precision parameters, see PrecParam
        :param axis=none: magnet axis (set to 'y' i None)
        """
        self.yoke_params = yoke_params
        self.coil_params = coil_params
        if prec_params is not None:
            self.prec_params = prec_params
        else:
            self.prec_params = PrecParam()
        self.octu_type = octu_type
        if yoke_params is None or coil_params is None:
            self.build()
        if axis is not None:
            self.yoke_params.axis = axis

    def build(self):
        """
        Build yoke parameters according to the specs
        """
        try:
            if self.octu_type == 'od1':
                self.build_od1_parameters()
            # Other descriptions to be added...
            else:
                print('Warning: Unknown octupole type, OD1 built')
                self.build_od1_parameters()
        except:
            raise Exception('OctuEMParam.build() failed')

    def build_od1_parameters(self):
        """
        Build parameters for an ESRF EBS SD1 sextupole
        """
        self.octu_type = 'od1'
        # Yoke
        self.yoke_params = OctuEMYokeParam()
        self.yoke_params.build_od1_parameters()
        # Coil
        self.coil_params = CoilParam()
        self.coil_params.build_od1_parameters()

    def refine_mesh(self, long_sub_n=3, long_sub_n_ext=2, area_max=500):
        """
        Refine the mesh according to the specifications
        :param long_sub_n=3: number of longitudinal segments on 1/2 quad, w/o extremities
        :param long_sub_n_ext=2: number of longitudinal segments in extremities
        :param area_max=250: maximum triangle area
        """
        try:
            self.yoke_params.mesh_params.long_sub = [long_sub_n, 1]
            self.yoke_params.mesh_params.long_sub_ext = [long_sub_n_ext, 1]
            self.yoke_params.mesh_params.area_max = area_max
        except AttributeError:
            raise Exception('Attribute error in refine_mesh()')


# -------------------------------------------
# Parameters for PM Dipole Module
# -------------------------------------------
class DipoleModulePMParam(Param):
    def __init__(self, yoke_params=None, pm_params=None, shim_params=None, prec_params=None, yoke_mesh_params=None,
                 pole_mesh_params=None, pm_mesh_params=None, dipole_module_type=None, axis=None):
        """
        Initialize dipole module parameters
        :param yoke_params=None: yoke parameters (see DipoleModulePMParam)
        :param pm_params=None: permanent magnet parameters
        :param shim_params=None: shim parameters
        :param prec_params=None: precision parametres (see PrecParam)
        :param yoke_mesh_params=None: mesh parameters (see MeshParam)
        :param pole_mesh_params=None: mesh parameters (see MeshParam)
        :param pm_mesh_params=None: mesh parameters (see MeshParam)
        :param dipole_module_type=None: dipole-quad type (e.g. 'dl1-1' for EBS DL1-1 magnet module)
        :param axis=None: magnet axis (set to 'y' if None)
        """
        if yoke_params is not None:
            self.yoke_params = yoke_params
        else:
            self.yoke_params = DipoleModulePMYokeParam()
        if pm_params is not None:
            self.pm_params = pm_params
        else:
            self.pm_params = DipoleModulePMBlocksParam()
        self.dipole_module_type = dipole_module_type
        if prec_params is not None:
            self.prec_params = prec_params
        else:
            self.prec_params = PrecParam()
        if axis is not None:
            self.yoke_params.axis = axis
        # Mesh
        if yoke_mesh_params is None:
            self.yoke_mesh_params = MeshExtTriParam()
        else:
            self.yoke_mesh_params = yoke_mesh_params
        if pole_mesh_params is None:
            self.pole_mesh_params = MeshExtTriParam()
        else:
            self.pole_mesh_params = pole_mesh_params
        if pm_mesh_params is None:
            self.pm_mesh_params = MeshSubParam()
        else:
            self.pm_mesh_params = pm_mesh_params
        # Shims
        if shim_params is None:
            self.shim_params = DipoleModulePMShimsParams()
        else:
            self.shim_params = shim_params
        if dipole_module_type is None:
            dipole_module_type = 'dl-high-field'
        if dipole_module_type == 'dl-high-field':
            self.yoke_params.build_dl_high_field()
            self.pm_params.build_dl_5()
            self.shim_params.build_dl_5()
        elif dipole_module_type == 'dl-low-field':
            self.yoke_params.build_dl_low_field()
            self.pm_params.build_dl_1()
            self.shim_params.build_dl_1()
        elif dipole_module_type == 'dl_1':
            self.yoke_params.build_dl_low_field()
            self.pm_params.build_dl_1()
            self.shim_params.build_dl_1()
        elif dipole_module_type == 'dl_2':
            self.yoke_params.build_dl_high_field()
            self.pm_params.build_dl_2()
            self.shim_params.build_dl_2()
        elif dipole_module_type == 'dl_3':
            self.yoke_params.build_dl_high_field()
            self.pm_params.build_dl_3()
            self.shim_params.build_dl_3()
        elif dipole_module_type == 'dl_4':
            self.yoke_params.build_dl_high_field()
            self.pm_params.build_dl_4()
            self.shim_params.build_dl_1()
        elif dipole_module_type == 'dl_5':
            self.yoke_params.build_dl_high_field()
            self.pm_params.build_dl_5()
            self.shim_params.build_dl_5()
        if yoke_mesh_params is not None:
            self.yoke_params.yoke_mesh_params = yoke_mesh_params
        if pole_mesh_params is not None:
            self.pole_params.pole_mesh_params = pole_mesh_params
        if pm_mesh_params is not None:
            self.pm_params.sub = pm_mesh_params

    def refine_mesh_yoke(self, long_sub_n=10, area_max=500):
        """
        Refine the mesh of the yoke
        :param long_sub_n: longitudinal subdivision number
        :param area_max: Maximum area of triangles
        """
        try:
            self.yoke_mesh_params.long_sub = [long_sub_n, 1]
            self.yoke_mesh_params.area_max = area_max
        except AttributeError:
            raise Exception('Attribute error in refine_mesh_yoke()')

    def refine_mesh_pole(self, long_sub_n=10, area_max=100):
        """
        Refine the mesh of the poles
        :param long_sub_n: longitudinal subdivision number
        :param area_max: Maximum area of triangles
        """
        try:
            self.pole_mesh_params.long_sub = [long_sub_n, 1]
            self.pole_mesh_params.area_max = area_max
        except AttributeError:
             raise Exception('Attribute error in refine_mesh_pole()')

    def refine_mesh_pm(self, kx=3, ky=3, kz=3):
        """
        Refine the mesh of the PM blocks
        :param kx=3: Subdivision numbers
        :param ky=3: same
        :param kz=3: same
        """
        try:
            self.pm_mesh_params.sub = [kx, ky, kz]
        except AttributeError:
             raise Exception('Attribute error in refine_mesh_pm()')


# -------------------------------------------
# Parameters for background field
# -------------------------------------------
class BackgroundParam(Param):
    def __init__(self, b0=None):
        """Initialize background field parameters
        :param b0=None: background field components [bx, by, bz] (T)
        """
        if b0 is None:
            self.back_field_params = [0, 1, 0]
        else:
            self.back_field_params = b0
        self.type = 'background'


# -------------------------------------------
# Parameters for Solenoid
# -------------------------------------------
class SolenoidParam(Param):
    def __init__(self, length=2000, r_in=100, r_out=120, j=150, k=1, n_seg=5, axis='y'):
        """
        Initialize parameters for a basic solenoid
        :param length=2000: length (mm)
        :param r_in=100: inner radius (mm)
        :param r_out=120: outer radius (mm)
        :param j=150: current density (A/mm^2)
        :param k=1: filling factor
        :param n_seg=20: number of segments
        :param axis='y': solenoid axis
        """
        self.length = length
        self.r_in = r_in
        self.r_out = r_out
        self.j = j
        self.k = k
        self.n_seg = n_seg
        self.axis = axis
        self.type = 'solenoid'


# -------------------------------------------
# Parameters for surrounding field
# -------------------------------------------
class SurroundParam(Param):
    def __init__(self, param=None):
        if param is None:
            self.surround_field_params = BackgroundParam()
        else:
            self.surround_field_params = param
        self.type = self.surround_field_params.type


# -------------------------------------------
# Parameters for shield
# -------------------------------------------
class ShieldParam(Param):
    def __init__(self, r_out=150, r_in=None, length=200, thickness=3, shield_type='can', mat='xc6', axis=None,
                 color=[0.3,0.3,0.3], sym=False, mesh_params=None):
        """
        Initialize parameters for iron shields
        :param r_out=150: Outer radius (mm)
        :param r_in=None: Inner radius (mm), close can if None
        :param length=200: Length (mm)
        :param thickness=3: thickness (mm)
        :param shield_type='can': type of the shield
        :param mat='xc6': material
        :param axis=None: symmetry axis ('y' if None)
        :param color=[0.3,0.3,0.3]: shim color
        :param sym=False: apply symmetries if True
        """
        self.length = length
        self.shield_type = shield_type
        self.mat = mat
        self.profile_params = ShieldProfileParam(r_out, r_in, thickness, n_seg=6)
        if axis is None:
            self.axis = 'y'
        else:
            self.axis = axis
        self.color = color
        self.sym = sym
        if mesh_params is None:
            self.mesh_params = MeshExtTriParam(area_max=100, long_sub=[5,1])
        else:
            self.mesh_params = mesh_params


# -------------------------------------------
# Parameters for shield profiles
# -------------------------------------------
class ShieldProfileParam(Param):
    def __init__(self, r_out=150, r_in=None, thickness=3, n_seg=6, theta=pi/2):
        """
        Parameters for shield params
        :param r_out=150: outer radius (mm)
        :param r_in=None: inner radius (mm), no aperture if None
        :param thickness=3: thickness (mm)
        :param n_seg=6: number of segments
        :param theta=pi/2: builld the profile from angle = 0 ... theta
        """
        self.r_out = r_out
        self.r_in = r_in
        self.thickness = thickness
        self.n_seg = n_seg


# -------------------------------------------
# Parameters for Electromagnet quads
# -------------------------------------------
class QuadStaggeredParam(Param):
    def __init__(self, yoke_params=None, surround_field_params=None, pm_params=None, prec_params=None):
        """
        Initialize a staggered quadrupole parameter object
        :param yoke_parameters=None: yoke parameters, see QuadEMYokeParam
        :param surround_field_params=None: surrounding field parameters, see SurroundParam
        :param pm_params=None: PM blocks' parametres, see QuadStaggeredPMParam (PM blocks not built if None)
        :param prec_param=None: precision parameters, see PrecParam
        """
        self.yoke_params = yoke_params
        self.surround_field_params = surround_field_params
        self.prec_params = prec_params
        self.pm_params = pm_params
        if yoke_params is None:
            self.yoke_params = QuadStaggeredYokeParam()
        if surround_field_params is None:
            self.surround_field_params = SurroundParam()
        if prec_params is None:
            self.prec_params = PrecParam()


# -------------------------------------------
# Base class for electromagnet yoke profile
# -------------------------------------------
class YokeProfile:
    # -------------------------------------------
    def plot(self):
        """
        Plot the profile if it exists
        :param pole_only=False: ignore the backleg profile if True
        """
        try:
            pole_profile = self.pole_profile
        except AttributeError:
            raise Exception('Attribute error in rYokeProfile.plot()')
        try:
            backleg_profile = self.backleg_profile
        except AttributeError:
            backleg_profile = None
        if pole_profile is not None and backleg_profile is not None:
            # --- Extract the profiles
            # Pole
            np_pole_profile = np.transpose(np.array(self.pole_profile))
            x_pole = np_pole_profile[0]
            y_pole = np_pole_profile[1]
            # Backleg
            np_back_profile = np.transpose(np.array(self.backleg_profile))
            x_back = np_back_profile[0]
            y_back = np_back_profile[1]
            # --- Repeat the first point to close the contour
            x_pole = np.concatenate((x_pole, x_pole[:1]))
            y_pole = np.concatenate((y_pole, y_pole[:1]))
            x_back = np.concatenate((x_back, x_back[:1]))
            y_back = np.concatenate((y_back, y_back[:1]))
            # --- Plot
            plot(x_pole, y_pole, x_back, y_back)
            show()
        elif pole_profile is not None and backleg_profile is None:
            if pole_profile is not None:
                # --- Extract the profiles
                # Pole
                np_pole_profile = np.transpose(np.array(self.pole_profile))
                x_pole = np_pole_profile[0]
                y_pole = np_pole_profile[1]
                # --- Repeat the first point to close the contour
                x_pole = np.concatenate((x_pole, x_pole[:1]))
                y_pole = np.concatenate((y_pole, y_pole[:1]))
                # --- Plot
                plot(x_pole, y_pole)
                show()
        else:
            raise Exception('Undefined pole or backleg profile in YokeProfile.plot()')

    # -------------------------------------------
    def print(self):
        """
        Print the profile if it exists
        :param pole_only=False: ignore the backleg profile if True
        """
        try:
            pole_profile = self.pole_profile
            pole_sub = self.pole_sub
        except AttributeError:
            raise Exception('Attribute error in YokeProfile.print()')
        try:
            backleg_profile = self.backleg_profile
            backleg_sub = self.backleg_sub
        except AttributeError:
            backleg_profile = None
            backleg_sub = None
        if pole_profile is not None and pole_sub is not None:
            print('--- Pole profile ---')
            for n in range(len(pole_profile)):
                print(f'Point number: {n:{2}}, '
                      f'Position: [{float(pole_profile[n][0]):{10}.{4}},{float(pole_profile[n][1]):{10}.{4}}], '
                      f'Subdivision: [{int(pole_sub[n][0]):{3}},{float(pole_sub[n][1]):{10}.{3}}] ')
        if backleg_profile is not None and backleg_sub is not None:
            print('--- Backleg yoke profile ---')
            for n in range(len(backleg_profile)):
                print(f'Point number: {n:{2}}, '
                      f'Position: [{float(backleg_profile[n][0]):{10}.{4}},{float(backleg_profile[n][1]):{10}.{4}}], '
                      f'Subdivision: [{int(backleg_sub[n][0]):{3}},{float(backleg_sub[n][1]):{10}.{3}}] ')

    # -------------------------------------------
    def change_profile_to_y_axis(self):
        """
        Change the yoke profile for extrusion along y axis
        :param pole_only=False: ignore backleg profile if True
        """

        # --- Get the profiles
        try:
            pole_profile = self.pole_profile
        except AttributeError:
            raise Exception('Attribute error in YokeProfile.change_profile_to_y_axis()')
        try:
            backleg_profile = self.backleg_profile
        except AttributeError:
            backleg_profile = None

        if pole_profile is None:
            raise Exception('Undefined profile in YokeProfile.change_profile_to_y_axis()')

        # --- Change the profile : exchange x and z
        for n in range(len(pole_profile)):
            x, z = pole_profile[n]
            pole_profile[n] = [z, x]

        if backleg_profile is not None:
            for n in range(len(backleg_profile)):
                x, z = backleg_profile[n]
                backleg_profile[n] = [z, x]

    # -------------------------------------------
    def change_profile_to_axis(self, axis='x'):
        """
        Change the profile according to the specified axis
        :param axis='x': longitudinal axis
        """

        if axis == 'y':
            self.change_profile_to_y_axis()


# -------------------------------------------
# Class for quadrupole profiles
# -------------------------------------------
class QuadEMYokeProfile(YokeProfile):
    def __init__(self, profile_params=None, mesh_params=None):
        """
        Initialize a quadrupole yoke profile
        :param profile_params=None: profile parameters (see QuadEMYokeProfileParam)
        :param mesh_params=None: mesh parameters (see MeshExtTriParam)
        """
        # --- Hold the parameters
        if profile_params is not None:
            self.pole_profile = None
            self.pole_sub = None
            self.backleg_profile = None
            self.backleg_sub = None
            # --- Build the profile
            pole_type = profile_params.pole_type
            back_type = profile_params.backleg_type
            if pole_type == 'shape':
                self.build_hyperbola_shape(profile_params, mesh_params)
                if back_type == '45deg':
                    self.build_yoke_backleg_45deg(profile_params)
                # Other backleg yoke types to implement
                else:
                    self.build_yoke_backleg_45deg(profile_params)
            elif pole_type == 'qf1' or pole_type == 'qd2' or pole_type == 'qd3' or pole_type == 'qf4' \
                    or pole_type == 'qd5' or pole_type == 'qf1_qd2_qd3_qf4_qd5':
                self.build_ebs_qf1_qd2_qd3_qf4_qd5()
            elif pole_type == 'qf6':
                self.build_ebs_q6()
            elif pole_type == 'qf8':
                self.build_ebs_qf8()
            else:
                self.build_hyperbola(profile_params, mesh_params)
                if back_type == '45deg':
                    self.build_yoke_backleg_45deg(profile_params)
                # Other backleg yoke types to implement
                else:
                    self.build_yoke_backleg_45deg(profile_params)
        else:
            raise Exception('Missing profile parameters in QuadEMYokeProfile()')

    # -------------------------------------------
    def build_pole_hyperbola_shape(self, profile_params, mesh_params):
        """
        Build a pole profile for a quadrupole magnet.
        :param profile_params: pole profile parameters (see QuadEMYokeProfileParam)
        :param mesh_params: mesh parameters (see MeshExtTriParam)
        """

        # --- Unpack the parameters
        if profile_params is not None:
            r0 = profile_params.r0
            x1 = profile_params.x0
            y1 = r0 ** 2 / (2 * x1)
            r1 = profile_params.r1
            r2 = profile_params.r2
            r3 = profile_params.r3
            theta_2 = profile_params.theta_2
            theta_3 = profile_params.theta_3
        else:
            raise Exception('Wrong pole parameters in build_pole_hyperbola_shape()')
        if mesh_params is not None:
            pole_yoke_sub = mesh_params.pole_yoke_sub
            pole_tip_sub = mesh_params.pole_tip_sub
        else:
            pole_yoke_sub = 5
            pole_tip_sub = 1
        if profile_params.pole_shape is not None:
            pole_shape = np.array(profile_params.pole_shape, dtype='complex64')
            n_points = pole_shape.size
        else:
            n_points = profile_params.n_points
            pole_shape = np.zeros(n_points, dtype='complex64')
        # --- Define the contour points in the complex plane
        u = np.zeros(n_points + 4, dtype='complex64')
        s =  np.ones(n_points + 4, dtype='complex64') # same for the subdivisions
        # Hyperbola
        alpha_0 = pi / 4
        alpha_1 = atan2(y1, x1)
        d_alpha = (alpha_1 - alpha_0) / (n_points - 1)
        for n in range(n_points):
            alpha_n = alpha_0 + n * d_alpha
            u[n] = (exp(1j * alpha_n)) * r0 / sin(2 * alpha_n) ** 0.5
            # Shape the pole according to pole_shape
            if n == 0:
                u[n] = u[n] + pole_shape[0].real * (1 + 1j) / 2 ** 0.5  # Displacement parallel to symmetry axis
            else:
                u[n] = u[n] + pole_shape[n]
            # Subdivision
            s[n] = pole_tip_sub
        # Pole
        u[n_points] = u[n_points - 1] + r1
        u[n_points + 1] = u[n_points] + r2 * exp(1j * theta_2 * pi / 180)
        u[n_points + 2] = u[n_points + 1] + r3 * exp(1j * theta_3 * pi / 180)
        u[n_points + 3] = u[n_points + 2] + (u[n_points + 2].real - u[n_points + 2].imag) * (-1 + 1j) / 2
        # --- Compute the profile
        np_profile = np.transpose(np.array([u.real, u.imag]))
        self.pole_profile = np_profile.tolist()
        np_sub = np.transpose(np.array([s.real, s.imag]))
        self.pole_sub = np_sub.tolist()
        self.pole_sub[-2] = [pole_yoke_sub, 1]

    # -------------------------------------------
    def build_yoke_backleg_45deg(self, profile_params):
        """
        Build a backleg profile for a quadrupole magnet.
        :param profile_params: profile parameters (see QuadEMYokeProfileParam)
        """
        # --- Get the pole profile and yoke parameters
        try:
            p_profile = self.pole_profile
            p_sub = self.pole_sub
            r4 = profile_params.r4
            w = profile_params.w
        except AttributeError:
            raise Exception('Attribute error in build_yoke_backleg_45deg()')
        if self.pole_profile is None:
            raise Exception('Undefined pole profile in build_yoke_backleg_45deg()')
        if self.pole_sub is None:
            raise Exception('Undefined pole subdivisions in build_yoke_backleg_45deg()')

        # --- Define the contour points in the complex plane
        # Pole points
        u = np.zeros(7, dtype='complex64')
        u[0] = p_profile[-1][0] + 1j * p_profile[-1][1]
        u[1] = p_profile[-2][0] + 1j * p_profile[-2][1]
        # Backleg yoke
        u[2] = u[1] + r4 * exp(- 1j * pi / 4)
        u[3] = u[2].real
        u[4] = u[3] + w
        u[5] = u[2] + w * (1 + 1j * (2 ** 0.5 - 1))
        u[6] = u[5] + (u[5].real - u[5].imag) * (-1 + 1j) / 2
        # --- Compute the profile
        np_profile = np.transpose(np.array([u.real, u.imag]))
        self.backleg_profile = np_profile.tolist()
        np_sub = np_profile * 0 + 1
        self.backleg_sub = np_sub.tolist()
        self.backleg_sub[0] = p_sub[-2]

    # -------------------------------------------
    def build_hyperbola_shape(self, profile_params, mesh_params):
        """
        Build the pole and backleg yoke profile, with used defined pole shape
        :param profile_params: profile parameters (see (see QuadEMYokeProfileParam))
        :param mesh_params: mesh parameters (see MeshExtTriParam)
        """

        # --- Build the pole profile
        err = self.build_pole_hyperbola_shape(profile_params, mesh_params)
        if err != 0:
            return
        # --- Build the backleg yoke profile
        err = self.build_yoke_backleg_45deg(profile_params)
        if err != 0:
            return

    # -------------------------------------------
    def build_hyperbola(self, profile_params, mesh_params):
        """
        Build a quadrupole profile with a hyperbolic pole shape
        :param profile_params: profile parameters (see (see QuadEMYokeProfileParam))
        :param mesh_params: mesh parameters (see MeshExtTriParam)
        """

        # --- Set the pole shape to None
        profile_params.pole_shape = None

        # --- Build the pole profile
        err = self.build_pole_hyperbola_shape(profile_params, mesh_params)
        if err != 0:
            return
        # --- Build the backleg yoke profile
        err = self.build_yoke_backleg_45deg(profile_params)
        if err != 0:
            return

    # -------------------------------------------
    def build_ebs_qf1_qd2_qd3_qf4_qd5(self):
        """
        Defines the profile of a QF1, QD2, QD3, QF4, QD5 quadrupole from the EBS
        """
        # --- Pole
        self.pole_profile = [[11.626, 11.626], [13.315, 10.122], [15.2451, 8.788], [18.236, 7.479], [22.845, 5.581],
                        [27.845, 5.581], [121.815, 39.783], [215.860, 133.828], [174.844, 174.844]]
        self.pole_sub = [[3, 1], [3, 1], [3, 1], [3, 1], [3, 1], [1, 1], [1, 1], [5, 1], [20, 0.05]]
        # --- Backleg
        self.backleg_profile = [[174.844, 174.844], [215.860, 133.828], [234, 115.688], [234., 0.],
                             [304., 0.], [304., 144.683], [224.341, 224.341]]
        self.backleg_sub = [[5, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]

    # -------------------------------------------
    def build_ebs_q6(self):
        """
        Defines the profile of a QF6 quadrupole from the EBS
        """
        # --- Pole
        self.pole_profile = [[8.960, 8.960], [9.980, 8.418], [11.109, 7.639], [12.376, 6.772], [13.640, 5.563],
                        [20.640, 5.563], [95.815, 32.925], [199.760, 136.870], [168.315, 168.315]]
        self.pole_sub = [[3, 1], [3, 1], [3, 1], [3, 1], [3, 1], [1, 1], [1, 1], [5, 1], [20, 0.05]]

        # --- Backleg
        self.backleg_profile = [ [168.315, 168.315], [199.760, 136.870], [229., 107.63], [229., 0.],
                        [304., 0.], [304., 138.696], [221.348, 221.348]]
        self.backleg_sub = [[5, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]

    # -------------------------------------------
    def build_ebs_qf8(self):
        """
        Defines the profile of a QF8 quadrupole from the EBS
        """

        # --- Pole
        self.pole_profile =  [[8.910, 8.910], [9.939, 8.610], [11.081, 7.872], [12.272, 6.756], [13.781, 5.611],
                        [20.781, 5.611], [95.957, 32.972], [199.902, 136.917], [168.409, 168.409]]
        self.pole_sub = [[3, 1], [3, 1], [3, 1], [3, 1], [3, 1], [1, 1], [1, 1], [5, 1], [20, 0.05]]

        # --- Backleg
        self.backleg_profile = [[168.409, 168.409], [199.902, 136.917], [229., 107.819], [229., 0.],
                        [304., 0.], [304., 138.885], [221.443, 221.443]]
        self.backleg_sub = [[5, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]


# -------------------------------------------
# Class for sextupole profiles
# -------------------------------------------
class SextuEMYokeProfile(YokeProfile):
    def __init__(self, profile_params=None, mesh_params=None):
        """
        Initialize a sextupole yoke profile
        :param profile_params=None: profile parameters (see SextuEMYokeProfileParam)
        :param mesh_params=None: mesh parameters (see MeshExtTriParam)
        """
        # --- Hold the parameters
        if profile_params is not None:
            self.pole_profile = None
            self.pole_sub = None
            self.backleg_profile = None
            self.backleg_sub = None
            # --- Build the profile
            pole_type = profile_params.pole_type
            if pole_type == 'shape':
                self.build_flat_shape(profile_params, mesh_params)
            # Hyperbolic profiles to be implemented and added here...
            else:
                self.build_flat(profile_params, mesh_params)
        else:
            raise Exception('Missing profile parameters in SextuEMYokeProfile()')

    def build_pole_flat_shape(self, profile_params, mesh_params, shape_switch=True):
        """
        Build a pole profile for a sextupole magnet.
        :param profile_params: pole profile parameters (see SextuEMYokeProfileParam)
        :param mesh_params: mesh parameters (see MeshExtTriParam)
        :param shape_switch=True: use profile_params.pole_shape to shape the if True
        """

        # --- Unpack the parameters
        if profile_params is not None:
            r0 = profile_params.r0
            x0 = r0 * cos(pi / 6)
            x1 = profile_params.x0
            r1 = profile_params.r1
            r2 = profile_params.r2
            r3 = profile_params.r3
            theta_2 = profile_params.theta_2
            theta_3 = profile_params.theta_3
        else:
            raise Exception('Wrong pole parameters in build_pole_flat_shape()')
        if mesh_params is not None:
            pole_yoke_sub = mesh_params.pole_yoke_sub
            pole_tip_sub = mesh_params.pole_tip_sub
        else:
            pole_yoke_sub = 5
            pole_tip_sub = 1
        if profile_params.pole_shape is not None:
            pole_shape = np.array(profile_params.pole_shape, dtype='complex64')
            n_points = pole_shape.size
        else:
            n_points = profile_params.n_points
            pole_shape = np.zeros(n_points, dtype='complex64')
        # --- Define the contour points in the complex plane
        u = np.zeros(n_points + 4, dtype='complex64')
        s =  np.ones(n_points + 4, dtype='complex64') # same for the subdivisions
        # flat profile
        dx = (x1 - x0) / (n_points - 1)
        for n in range(n_points):
            u[n] = r0 * exp(1j * pi / 6) + 2 * n * dx * exp(- 1j * pi / 3)
            # Shape the pole according to pole_shape
            if shape_switch:
                if n == 0:
                    u[n] = u[n] + pole_shape[0].real * exp(1j * pi / 6)  # Displacement parallel to symmetry axis
                else:
                    u[n] = u[n] + pole_shape[n]
            # Subdivision
            s[n] = pole_tip_sub
        # Pole
        u[n_points] = u[n_points - 1] + r1
        u[n_points + 1] = u[n_points] + r2 * exp(1j * theta_2 * pi / 180)
        u[n_points + 2] = u[n_points + 1] + r3 * exp(1j * theta_3 * pi / 180)
        l = (u[n_points + 2].real - 3 ** 0.5 * u[n_points + 2].imag) / 2
        u[n_points + 3] = u[n_points + 2] + l * exp(1j * 2 * pi / 3)
        # --- Compute the profile
        np_profile = np.transpose(np.array([u.real, u.imag]))
        self.pole_profile = np_profile.tolist()
        np_sub = np.transpose(np.array([s.real, s.imag]))
        self.pole_sub = np_sub.tolist()
        self.pole_sub[-2] = [pole_yoke_sub, 1]

    def build_yoke_backleg_30deg(self, profile_params):
        """
        Build a backleg profile for a sextupole magnet.
        :param profile_params: profile parameters (see SextuEMYokeProfileParam)
        """
        # --- Get the pole profile and yoke parameters
        try:
            p_profile = self.pole_profile
            p_sub = self.pole_sub
            r4 = profile_params.r4
            w = profile_params.w
        except AttributeError:
            raise Exception('Attribute error in build_yoke_backleg_30deg()')
        if self.pole_profile is None:
            raise Exception('Undefined pole profile in build_yoke_backleg_30deg()')
        if self.pole_sub is None:
            raise Exception('Undefined pole subdivisions in build_yoke_backleg_30deg()')

        # --- Define the contour points in the complex plane
        # Pole points
        u = np.zeros(7, dtype='complex64')
        u[0] = p_profile[-1][0] + 1j * p_profile[-1][1]
        u[1] = p_profile[-2][0] + 1j * p_profile[-2][1]
        # Backleg yoke
        u[2] = u[1] + r4 * exp(- 1j * pi / 3)
        u[3] = u[2].real
        u[4] = u[3] + w
        u[6] = u[0] + w * exp(1j * pi/6)
        u[5] = u[6] + 2 * (u[4].real - u[6].real) * exp(- 1j * pi/3)
        # --- Compute the profile
        np_profile = np.transpose(np.array([u.real, u.imag]))
        self.backleg_profile = np_profile.tolist()
        np_sub = np_profile * 0 + 1
        self.backleg_sub = np_sub.tolist()
        self.backleg_sub[0] = p_sub[-2]

    def build_flat_shape(self, profile_params, mesh_params):
        """
        Build the pole and backleg yoke profile, with used defined pole shape
        :param profile_params: profile parameters (see (see SextuEMYokeProfileParam))
        :param mesh_params: mesh parameters (see MeshExtTriParam)
        """
        # --- Build the pole profile
        self.build_pole_flat_shape(profile_params, mesh_params)
        # --- Build the backleg yoke profile
        self.build_yoke_backleg_30deg(profile_params)

    def build_flat(self, profile_params, mesh_params):
        """
        Build the pole and backleg yoke profile, with used defined pole shape
        :param profile_params: profile parameters (see SextuEMYokeProfileParam)
        :param mesh_params: mesh parameters (see MeshExtTriParam)
        """
        # --- Build the pole profile
        self.build_pole_flat_shape(profile_params, mesh_params, shape_switch=False)
        # --- Build the backleg yoke profile
        self.build_yoke_backleg_30deg(profile_params)

    def build_ebs_sd1(self):
        """
        Defines the profile of a SD1 sextupole from the EBS
        """

        self.pole_profile = [[16.627, 9.600], [17.203, 8.603], [17.778, 7.606], [18.354, 6.609], [18.930, 5.612],
                             [25.229, 5.612], [135.868, 23.022], [275.298, 103.522], [251.30, 145.088]]

        self.pole_sub = [[1.0, 0.0], [1.0, 0.0], [1.0, 0.0], [1.0, 0.0], [1.0, 0.0], [1.0, 0.0], [1.0, 0.0], [5, 1],
                         [1.0, 0.0]]

        # --- Backleg
        self.backleg_profile = [[251.300, 145.088], [275.298, 103.522], [308.298, 46.365], [308.298, 0.0],
                                [378.15, 0.0], [378.15, 85.0], [320.582, 185.088]]

        self.backleg_sub = [[5, 1], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0]]

    def build_ebs_sf2(self):
        """
        Defines the profile of a SF2 sextupole from the EBS
        """

        self.pole_profile = [[16.627, 9.600], [17.203, 8.603], [17.778, 7.606], [18.354, 6.609], [18.930, 5.612],
                             [25.229, 5.612], [135.868, 23.022], [275.298, 103.522], [251.30, 145.088]]

        self.pole_sub = [[1.0, 0.0], [1.0, 0.0], [1.0, 0.0], [1.0, 0.0], [1.0, 0.0], [1.0, 0.0], [1.0, 0.0], [5, 1],
                         [1.0, 0.0]]

        # --- Backleg
        self.backleg_profile = [[251.300, 145.088], [275.298, 103.522], [308.298, 46.365], [308.298, 0.0],
                                [378.15, 0.0], [378.15, 85.0], [320.582, 185.088]]

        self.backleg_sub = [[5, 1], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0], [1.0, 1.0]]


# -------------------------------------------
# Class for octupole profiles
# -------------------------------------------
class OctuEMYokeProfile(YokeProfile):
    def __init__(self, profile_params=None, mesh_params=None):
        """
        Initialize an octupole yoke profile
        :param profile_params=None: profile parameters (see OctuEMYokeProfileParam)
        :param mesh_params=None: mesh parameters (see MeshExtTriParam)
        """
        # --- Hold the parameters
        if profile_params is not None:
            self.pole_profile = None
            self.pole_sub = None
            self.backleg_profile = None
            self.backleg_sub = None
            # --- Build the profile
            pole_type = profile_params.pole_type
            if pole_type == 'ebs':
                self.build_ebs(mesh_params)
            # Other octupole shapes to be added here...
            else:
                self.build_ebs()
        else:
            raise Exception('Missing profile parameters in OctuEMYokeProfile()')

    def build_ebs(self, mesh_params=None):
        """
        Build a pole profile for a EBS octupole magnet.
        """
        if mesh_params is not None:
            pole_yoke_sub = mesh_params.pole_yoke_sub
            pole_tip_sub = mesh_params.pole_tip_sub
        else:
            pole_yoke_sub = 5
            pole_tip_sub = 1


        self.pole_profile = [[5.54, 18.08], [6.31, 17.58], [7.12, 17.18], [7.96, 16.89], [8.87, 16.70],
                             [12.68, 20.52], [73.21, 123.28], [148.92, 198.98], [127.70, 220.19],
                             [99.42, 220.19], [41.41,  162.19], [5.54, 23.48]]

        self.pole_sub = [[pole_tip_sub, 1], [pole_tip_sub, 1], [pole_tip_sub, 1], [pole_tip_sub, 1], [pole_tip_sub, 1],
                         [pole_tip_sub, 1], [pole_yoke_sub, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]

        self.backleg_profile = [[85.28, 135.34], [110.31, 110.31], [173.95, 173.95], [148.92, 198.98]]

        self.backleg_sub = [[1, 1], [1, 1], [1, 1], [pole_yoke_sub, 1]]


# -------------------------------------------
# Class for staggered quadrupole profiles
# -------------------------------------------
class QuadStaggeredYokeProfile(YokeProfile):

    def __init__(self, profile_params=None, mesh_params=None):
        """
        Initialize a staggered quadrupole yoke profile
        :param profile_params=None: profile parameters (see QuadEMYokeProfileParam)
        :param mesh_params=None: mesh parameters (see MeshExtTriParam)
        """
        # --- Hold the parameters
        if profile_params is not None:
            self.pole_profile = None
            self.pole_sub = None
            self.backleg_profile = None
            self.backleg_sub = None
            # --- Build the profile
            # pole_type = profile_params.pole_type
            self.build_pole_hyperbola_shape(profile_params, mesh_params)
        else:
            raise Exception('Missing profile parameters in QuadStaggeredYokeProfile()')

    # -------------------------------------------
    def build_pole_hyperbola_shape(self, profile_params, mesh_params):
        """
        Build a pole profile for a quadrupole magnet.
        :param profile_params: pole profile parameters (see QuadEMYokeProfileParam)
        :param mesh_params: mesh parameters (see MeshExtTriParam)
        """

        # --- Unpack the parameters
        if profile_params is not None:
            r0 = profile_params.r0
            x1 = profile_params.x0
            y1 = r0 ** 2 / (2 * x1)
            r1 = profile_params.r1
            r2 = profile_params.r2
            r3 = profile_params.r3
            theta = profile_params.theta
        else:
            raise Exception('Wrong pole parameters in build_pole_hyperbola_shape()')
        if mesh_params is not None:
            pole_yoke_sub = mesh_params.pole_yoke_sub
            pole_tip_sub = mesh_params.pole_tip_sub
        else:
            pole_yoke_sub = 5
            pole_tip_sub = 1
        if profile_params.pole_shape is not None:
            pole_shape = np.array(profile_params.pole_shape, dtype='complex64')
            n_points = pole_shape.size
        else:
            n_points = profile_params.n_points
            pole_shape = np.zeros(n_points, dtype='complex64')
        # --- Define the contour points in the complex plane
        u = np.zeros(n_points + 4, dtype='complex64')
        s =  np.ones(n_points + 4, dtype='complex64') # same for the subdivisions
        # Hyperbola
        alpha_0 = pi / 4
        alpha_1 = atan2(y1, x1)
        d_alpha = (alpha_1 - alpha_0) / (n_points - 1)
        for n in range(n_points):
            alpha_n = alpha_0 + n * d_alpha
            u[n] = (exp(1j * alpha_n)) * r0 / sin(2 * alpha_n) ** 0.5
            # Shape the pole according to pole_shape
            if n == 0:
                u[n] = u[n] + pole_shape[0].real * (1 + 1j) / 2 ** 0.5  # Displacement parallel to symmetry axis
            else:
                u[n] = u[n] + pole_shape[n]
            # Subdivision
            s[n] = pole_tip_sub
        # Pole
        u[n_points] = u[n_points - 1] + r1
        u[n_points + 1] = u[n_points] + r2 * exp(1j * theta)
        u[n_points + 2] = u[n_points + 1] + r3 * 1j
        u[n_points + 3] =  (u[n_points + 2].real + u[n_points + 2].imag) / 2 * (1 + 1j)
        # --- Compute the profile
        np_profile = np.transpose(np.array([u.real, u.imag]))
        self.pole_profile = np_profile.tolist()
        np_sub = np.transpose(np.array([s.real, s.imag]))
        self.pole_sub = np_sub.tolist()
        self.pole_sub[-2] = [pole_yoke_sub, 1]


# -------------------------------------------
# Class for dipole-quadrupole profiles
# -------------------------------------------
class DipoleQuadEMYokeProfile(YokeProfile):
    def __init__(self, profile_params=None, mesh_params=None):
        # --- Hold the parameters
        if profile_params is not None:
            self.pole_profile = None
            self.pole_sub = None
            # --- Build the profile
            pole_type = profile_params.pole_type
            if pole_type == 'dq1':
                self.build_ebs_dq1()
            elif pole_type == 'dq2':
                self.build_ebs_dq2()
        else:
            raise Exception('Missing profile parameters in DipoleQuadEMYokeProfile()')

    # -------------------------------------------
    def build_dipole_quadrupole_shape(self):
        # Profile from parameters to be implemented
        # Build an EBS DQ1 magnet for the moment... to be implemented
        self.build_ebs_dq1()

    # -------------------------------------------
    def build_ebs_dq1(self):
        """
        Defines the profile of a DQ1 dipole-quadrupole from the EBS
        """
        self.pole_profile =[[166.195, 126.445], [194.479, 98.161], [194.479,0], [244.479, 0], [244.479, 246.78],
                            [-78.751, 246.78], [-78.751, 54.276], [-78.751, 41.696], [-22.176, 9.032],
                            [-19.176, 9.032], [-18.106, 10.244], [-17.246, 10.752], [-16.608, 11.200],[-16.181, 11.599],
                            [-16.251, 12.304], [-16.251, 15.304], [-38.751, 54.275], [-38.75, 206.78], [85.859, 206.78],
                            [114.143, 178.496], [16.832, 81.181], [-8.525,26.803], [-8.525, 23.803], [-7.416, 19.767],
                            [-5.542, 17.277], [-3.484, 15.440], [-1.573, 13.578], [0.289, 11.705], [2.726, 9.984],
                            [6.565, 8.341], [13.711, 5.982], [16.711, 5.982], [71.089, 31.339]]
        self.pole_sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                         [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [3, 1], [3, 1], [3, 1],
                         [3, 1], [3, 1], [3, 1], [3, 1], [3, 1], [3, 1], [3, 1], [3, 1], [3, 1]]

    # -------------------------------------------
    def build_ebs_dq2(self):
        """
        Defines the profile of an EBS DQ2 dipole-quadrupole
        """
        self.pole_profile = [[181.069, 82.943], [181.069, 0], [231.069, 0], [231.069, 228.118], [-77.144, 228.118],
                             [-77.144, 53.858], [-77.144, 40.531], [-21.506, 8.408], [-18.506 ,8.408],
                             [-17.579, 9.160], [-16.765, 9.659], [-16.056, 10.165], [-15.437, 10.684],
                             [-14.900, 11.223], [-14.644, 11.887], [-14.644, 14.887], [-37.144, 53.858],
                             [-37.1442, 188.118], [75.894, 188.118], [104.178, 159.833], [21.933, 77.585],
                             [-3.424, 23.207], [-3.424, 20.207], [-1.761, 18.175], [-0.373,	15.781], [1.181, 13.987],
                             [2.982, 12.521], [4.971, 11.136], [7.329, 9.688], [10.569, 8.193], [16.211, 6.674],
                             [19.211, 6.674], [73.589, 32.031], [152.785, 111.227]]

        self.pole_sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                         [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                         [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]


# -------------------------------------------
# Class for dipole module profiles
# -------------------------------------------
class DipoleModulePMYokeProfile(YokeProfile):
    def __init__(self, profile_params=None):
        # --- Hold the parameters
        if profile_params is not None:
            self.pole_profile = None
            self.pole_sub = None
            # --- Build the profile
            pole_type = profile_params.pole_type
            if pole_type == 'dl-high-field':
                self.build_ebs_dl_high_field()
            if pole_type == 'dl-low-field':
                self.build_ebs_dl_low_field()
        else:
            raise Exception('Missing profile parameters in DipoleModulePMYokeProfile()')

    # -------------------------------------------
    def build_ebs_dl_high_field(self):
        """
        Defines the profile of a high field DL module from the EBS
        """
        # Pole
        self.pole_profile = [[-30.0, 12.53], [-28.0, 12.53], [-26.0, 12.53], [-24.0, 12.5], [-22.0, 12.5],
                             [-20.0, 12.5], [-18.0, 12.53], [-16.0, 12.60], [-14.0, 12.74], [-12.0, 12.82],
                             [-10.0, 12.82], [-8.0, 12.75], [-6.0, 12.68], [-4.0, 12.70], [-2.0, 12.75], [0.0, 12.75],
                             [2.0, 12.75], [4.0, 12.70], [6.0, 12.68], [8.0, 12.75], [10.0, 12.82], [12.0, 12.82],
                             [14.0, 12.74], [16.0, 12.60], [18.0, 12.53], [20.0, 12.5], [22.0, 12.5], [24.0, 12.5],
                             [26.0, 12.53], [28.0, 12.53], [30.0, 12.53], [30.0, 22.75], [32.5, 22.75], [32.5, 62.05],
                             [28.5, 62.05], [28.5, 68.05], [-28.5, 68.05], [-28.5, 62.05], [-32.5, 62.05],
                             [-32.5, 22.75], [-30.0, 22.75]]
        self.pole_sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                         [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                         [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                         [1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]
        # Backleg yoke
        self.backleg_profile =[[-63.70, 0], [-63.70, 13.75], [-54.70, 22.75], [-54.70, 82.25], [-46.70, 90.25],
                               [54.70, 90.24], [54.70, 23.25], [59.70, 23.25], [74.70, 38.25], [74.70, 118.25],
                               [67.70, 125.25], [-101.70, 125.25], [-108.70, 118.25], [-108.70, 0]]
        self.backleg_sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                            [1, 1], [1, 1], [1, 1]]

    # -------------------------------------------
    def build_ebs_dl_low_field(self):
        """
        Defines the profile of a high field DL module from the EBS
        """
        # Pole
        self.pole_profile = [[-32.5, 15.02], [-29.25, 15.0], [-26.0, 15.0], [-22.75, 15.0], [-19.5, 15.0],
                             [-16.25, 15.12], [-13.0, 15.26], [-9.75, 15.52], [-6.5, 15.36], [-3.25, 15.25],
                             [0.0, 15.25], [3.25, 15.25], [6.5, 15.36], [9.75, 15.52], [13.0, 15.26], [16.25, 15.12],
                             [19.5, 15.0], [22.75, 15.0], [26.0, 15.0], [29.25, 15.0], [32.5, 15.0], [32.5, 62.05],
                             [28.5, 62.05], [28.5, 68.05], [-28.5, 68.05], [-28.5, 62.05], [-32.5, 62.05]]
        self.pole_sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1],  [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                         [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                         [1, 1], [1, 1], [1, 1], [1, 1]]
        # Backleg yoke
        self.backleg_profile =[[-63.70, 0], [-63.70, 13.75], [-54.70, 22.75], [-54.70, 82.25], [-46.70, 90.25],
                               [54.70, 90.24], [54.70, 23.25], [59.70, 23.25], [74.70, 38.25], [74.70, 118.25],
                               [67.70, 125.25], [-101.70, 125.25], [-108.70, 118.25], [-108.70, 0]]
        self.backleg_sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1],
                            [1, 1], [1, 1], [1, 1]]


# -------------------------------------------
# Class for shield profiles
# -------------------------------------------
class ShieldProfile(YokeProfile):
    def __init__(self, profile_params=None):
        """
        Initialize profile parameters for shields
        :param profile_params:
        """
        self.pole_profile = None
        self.sub = None
        if profile_params is None:
            self.profile_params = ShieldProfileParam()
        else:
            self.profile_params = profile_params

    def build_full_flange(self):
        """
        Build a 1/4 disk profile for the flange
        """
        try:
            n_seg = self.profile_params.n_seg
            r_out = self.profile_params.r_out
            th = self.profile_params.thickness
        except AttributeError:
            raise Exception('Missing parameters in ShieldProfile.build_full_flange()')
        self.yoke_profile = [[(r_out - th) * cos(k * pi / (2 * (n_seg - 1))),
                              (r_out - th) * sin(k * pi / (2 * (n_seg - 1)))] for k in range(n_seg)]
        self.yoke_profile.append([0, 0])
        self.sub = [[1, 1] for k in range(len(self.yoke_profile))]

    def build_hollow_flange(self):
        """
        Build 1/4 of the profile of a shield flange with an aperture
        """
        try:
            n_seg = self.profile_params.n_seg
            r_out = self.profile_params.r_out
            th = self.profile_params.thickness
            r_in = self.profile_params.r_in
            if r_in is None:
                raise Exception('Missing parameters in ShieldProfile.build_hollow_flange()')
        except:
            raise Exception(' Attribute error in ShieldProfile.build_hollow_flange()')

        xz = [[cos(k * pi / (2 * (n_seg - 1))), sin(k * pi / (2 * (n_seg - 1)))] for k in range(n_seg)]
        xz_in = r_in * np.array(xz)
        xz_out = (r_out - th) * np.array(xz)
        xz_in_out = np.concatenate((xz_in, np.flip(xz_out, 0)))
        self.yoke_profile = xz_in_out.tolist()
        self.sub = [[1, 1] for k in range(len(self.sub))]

    def build_outer_cylinder(self):
        """
        Build the profile of the outer cylinder of a shield
        """
        try:
            n_seg = self.profile_params.n_seg
            r_out = self.profile_params.r_out
            th = self.profile_params.thickness
        except AttributeError:
            raise Exception('Attribute error in ShieldProfile.build_outer_cylinder()')

        xz = [[cos(k * pi / (2 * (n_seg - 1))), sin(k * pi / (2 * (n_seg - 1)))] for k in range(n_seg)]
        xz_in = (r_out - th) * np.array(xz)
        xz_out = r_out * np.array(xz)
        xz_in_out = np.concatenate((xz_in, np.flip(xz_out, 0)))
        self.yoke_profile = xz_in_out.tolist()
        self.sub = [[1, 1] for k in range(len(self.sub))]


# -------------------------------------------
# Base class for Multipole magnet
# -------------------------------------------
class MM:
    def __init__(self, obj=None, filename=None, path=None, from_rad=False):
        """
        Build an empty MM object and initialize its main Radia object if specified
        :param obj=None: Radia object to be used for initialization, if not None
        :param filename=None: load filename.rad if filename is not None
        :param path=None: path to filename
        :param from_rad=False: use .mmo and .mmp files (i.e. Radia object + parameters) if False, else use .rad files
        """
        self.coil = None
        self.yoke = None
        self.result = None
        self.time_end = None
        self.time_start = None
        self.params = Param()
        self.params.prec_params = PrecParam()
        # Use obj if specified
        if obj is not None:
            self.obj = obj
        # Load a file if specified
        elif filename is not None:
            # Define the path if None
            if path is None:
                path = ''
            if from_rad == False:
                self.load(filename=filename, path=path)
            else:
                self.obj = rad_uti.load(filename, path=path)
        else:
            self.obj = rad.ObjCnt([])

    # -------------------------------------------
    def __add__(self, other):
        """
        Overloaded + operator for MM objects
        The result contains deep copies of the initial objects
        """
        res = MM()
        # --- duplicate objects and break the symmetries
        # --- Add the Radia objects in a single container
        rad.ObjAddToCnt(res.obj, [self.obj, other.obj])

        return res

    # -------------------------------------------
    def build(self):
        pass

    # -------------------------------------------
    def delete(self, delete_params=True):

        self.result = None
        self.time_end = None
        self.time_start = None
        if delete_params:
            self.params = None
        try:
            rad.UtiDel(self.obj)
            self.obj = None
        except AttributeError:
            pass

    # -------------------------------------------
    def rebuild(self, solve_switch=True, print_switch=True):
        """
        Delete the Radia objects and build new ones
        :return:
        """
        self.delete(False)
        self.build()
        if solve_switch:
            self.solve(print_switch)

    # -------------------------------------------
    def duplicate(self):
        """
        Duplicate the MM object
        :return:
        """
        new = MM()
        dict_keys = self.__dict__.keys()
        for k in dict_keys:
            setattr(new, k, deepcopy(getattr(self, k)))
            # Duplicate the Radia objects
            if hasattr(getattr(self, k), 'obj'):
                setattr(new, k + '.obj', rad.ObjDpl(getattr(getattr(self, k), 'obj')))
        return new

    # -------------------------------------------
    def split(self):
        """
        Build MM objects with the top layer of self.obj
        :return: a list of MM objects
        """

        obj_list = rad.ObjCntStuf(self.obj)
        n_obj = len(obj_list)
        mm_list = []
        for n in range(n_obj):
            mm_list.append(MM(obj=obj_list[n]))
        return mm_list

    # -------------------------------------------
    def plot_geo(self):
        """
        Plot the object
        """
        try:
            rad.ObjDrwOpenGL(self.obj)
        except AttributeError:
            raise Exception('Undefined obj in MM.plot_geo()')

    # -------------------------------------------
    # Plot the yoke profile
    def plot_profile(self):
        """
        Plot the yoke profile
        :return: None
        """

        try:
            self.yoke.plot_profile()
        except AttributeError:
            raise Exception('Attribute error in MM.plot_profile')

    # -------------------------------------------
    # Print the yoke profile
    def print_profile(self):
        """
        Print the yoke profile
        :return: None
        """

        try:
            self.yoke.print_profile()
        except AttributeError:
            raise Exception('Attribute error in MM.print_profile')

    # -------------------------------------------
    def solve(self, print_switch=True):
        """
        Radia solver
        """
        # --- Get tolerance parameters
        try:
            tol = self.params.prec_params.tolerance
            max_iter = self.params.prec_params.max_iter
        except AttributeError:
            raise Exception('Undefined tolerance parameters in MM.solve()')

        try:
            obj = self.obj
        except AttributeError:
            raise Exception('Undefined obj in MM.solve()')
        # --- Solve
        self.time_start = datetime.today()
        if print_switch:
            t = self.time_start
            print(t.day,'/', t.month,'/',t.year,' at ', t.hour,':',t.minute,':',t.second,
                  ' Start to solve the magnetization problem...')
        self.result = rad.Solve(self.obj, tol, max_iter)
        self.time_end = datetime.today()
        if print_switch:
            t = self.time_end
            print(t.day, '/', t.month, '/', t.year, ' at ', t.hour, ':', t.minute, ':', t.second,
                   ' Magnetization problem solved.')

    # -------------------------------------------
    def set_prec_params(self, prec_params):
        """
        Set the precision parameters
        :param prec_params: Precision parameters (see PrecParam)
        """
        self.params.prec_params = prec_params

    # -------------------------------------------
    def set_tolerance(self, tol):
        """
        Set the tolerance
        :param tol: tolerance
        """
        self.params.prec_params.tolerance = tol

    # -------------------------------------------
    def set_max_iter(self, max_iter):
        """
        Set the maximum number of iterations
        :param max_iter: maximum number of iterations
        """
        self.params.prec_params.max_iter = max_iter

    # -------------------------------------------
    def field(self, xyz_end, xyz_start=[0, 0, 0], n=100, b='bz'):
        """
        Compute the field along a straight line
        :param xyz_end: end point [x, y, z]
        :param xyz_start: starting point [x, y, z] (default = [0, 0, 0])
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :return: x, y, z, d, bz: positions, distance to initial point and field
        """
        # --- Sampling
        x0, y0, z0 = xyz_start
        x1, y1, z1 = xyz_end
        # Steps
        if n > 1:
            dx = (x1 - x0) / (n - 1)
            dy = (y1 - y0) / (n - 1)
            dz = (z1 - z0) / (n - 1)
        else:
            dx, dy, dz = 0, 0, 0
        # Positions
        x = [x0 + k * dx for k in range(n)]
        y = [y0 + k * dy for k in range(n)]
        z = [z0 + k * dz for k in range(n)]
        xyz = [[x[k], y[k], z[k]] for k in range(n)]
        # Distance to initial point
        d = [((x[k] - x[k - 1]) ** 2 + (y[k] - y[k - 1]) ** 2 + (z[k] - z[k - 1]) ** 2) ** 0.5 for k in range(1, n)]
        d = list(accumulate(d))
        d.insert(0, 0)

        # --- Field computation
        if d[-1] > 1e-6 and n > 1:
            bz = rad.Fld(self.obj, b, xyz)
        else:
            bz = [rad.Fld(self.obj, b, [x1, y1, z1])]
            x, y, z, d = [x1], [y1], [z1], [0.]

        # --- Reshape
        x, y, z, d, bz = np.array(x), np.array(y), np.array(z), np.array(d), np.array(bz)
        bz = bz.reshape(x.shape)

        # --- Return
        return x, y, z, d, bz

    # -------------------------------------------
    def plot_field(self, xyz_end, xyz_start=[0, 0, 0], n=100, b='bz', x_axis='d', plot_show=True, plot_title=''):
        """
        Compute and plot the field along a straight line
        :param xyz_end: end point [x, y, z]
        :param xyz_start: starting point [x, y, z] (default = [0, 0, 0])
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :param x_axis: defines the x axis of the plot, x_axis = 'x', 'y', 'z' or 'd' (default = 'd', i.e. distance)
        :param show: show the plot if True
        :param plot_title: plot title
        """

        # --- Compute the field
        x, y, z, d, bz = self.field(xyz_end=xyz_end, xyz_start=xyz_start, n=n, b=b)
        # --- Plot
        fig = figure()
        if x_axis == 'x':
            l = x
        elif x_axis == 'y':
            l = y
        elif x_axis == 'z':
            l = z
        else:
            l = d
        plot(l, bz)
        xlabel('Distance (mm)')
        ylabel('Field (T)')
        title(plot_title)
        if plot_show:
            show()

    # -------------------------------------------
    def gradient(self, xyz_end, xyz_start=[0, 0, 0], n=100, b='bz', gradient_axis=[1, 0, 0], dxyz=1):
        """
        Compute the transverse gradient along a straight line

        :param xyz_end: end point [x, y, z]
        :param xyz_start: starting point [x, y, z] (default = [0, 0, 0])
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :param gradient_axis=[1, 0, 0]: direction of the gradient
        :param dxyz=1: transverse step (mm)
        :return: x, y, z, d, g: positions, distance to initial point and gradient (T/m)
        """
        # --- Sampling
        x0, y0, z0 = xyz_start
        x1, y1, z1 = xyz_end
        # Steps
        dx = (x1 - x0) / (n - 1)
        dy = (y1 - y0) / (n - 1)
        dz = (z1 - z0) / (n - 1)
        # Positions
        x = [x0 + k * dx for k in range(n)]
        y = [y0 + k * dy for k in range(n)]
        z = [z0 + k * dz for k in range(n)]
        dx = dxyz * gradient_axis[0]
        dy = dxyz * gradient_axis[1]
        dz = dxyz * gradient_axis[2]
        dnorm = (dx ** 2 + dy ** 2 + dz ** 2) ** 0.5
        xyz_1 = [[x[k] + dx / 2, y[k] + dy / 2, z[k] + dz / 2] for k in range(n)]
        xyz_0 = [[x[k] - dx / 2, y[k] - dy / 2, z[k] - dz / 2] for k in range(n)]
        # Distance to initial point
        d = [((x[k] - x[k - 1]) ** 2 + (y[k] - y[k - 1]) ** 2 + (z[k] - z[k - 1]) ** 2) ** 0.5 for k in range(1, n)]
        d = list(accumulate(d))
        d.insert(0, 0)

        # --- Field computations
        b_1 = rad.Fld(self.obj, b, xyz_1)
        b_0 = rad.Fld(self.obj, b, xyz_0)
        g = [[1000 * (b_1[k] - b_0[k]) / dnorm] for k in range(n)]

        # --- Reshape
        x, y, z, d, g = np.array(x), np.array(y), np.array(z), np.array(d), np.array(g)
        g = g.reshape(x.shape)

        # --- Return
        return x, y, z, d, g

    # -------------------------------------------
    def plot_gradient(self, xyz_end, xyz_start=[0, 0, 0], n=100, b='bz', gradient_axis=[1, 0, 0], dxyz=1, x_axis='d',
                      plot_show=True, plot_title=''):
        """
        Compute and plot the gradient along a straight line
        :param xyz_end: end point [x, y, z]
        :param xyz_start: starting point [x, y, z] (default = [0, 0, 0])
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :param gradient_axis: direction of the gradient
        :param dxyz=1: step for gradient computation (mm)
        :param x_axis: defines the x axis of the plot, x_axis = 'x', 'y', 'z' or 'd' (default = 'd', i.e. distance)
        :param show: show the plot if True
        :param plot_title: plot title
        """

        # --- Compute the field
        x, y, z, d, g = self.gradient(xyz_end=xyz_end, xyz_start=xyz_start, n=n, b=b, gradient_axis=gradient_axis,
                                      dxyz=dxyz)
        # --- Plot
        fig = figure()
        if x_axis == 'x':
            l = x
        elif x_axis == 'y':
            l = y
        elif x_axis == 'z':
            l = z
        else:
            l = d
        plot(l, g)
        xlabel('Distance (mm)')
        ylabel('Field gradient (T/m)')
        title(plot_title)
        if plot_show:
            show()

    # -------------------------------------------
    def sextu_strength(self, xyz_end, xyz_start=[0, 0, 0], n=100, b='bz', gradient_axis=[1, 0, 0], dxyz=1):
        """
        Compute the sextupole strength along a straight line
        The sextupole strength is defined as 1/2 dB^2 / dx^2

        :param xyz_end: end point [x, y, z]
        :param xyz_start: starting point [x, y, z] (default = [0, 0, 0])
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :param gradient_axis=[1, 0, 0]: direction of the gradient
        :param dxyz=1: transverse step (mm)
        :return: x, y, z, d, s: positions, distance to initial point and gradient (T/m)
        """
        # --- Sampling
        x0, y0, z0 = xyz_start
        x1, y1, z1 = xyz_end
        # Steps
        dx = (x1 - x0) / (n - 1)
        dy = (y1 - y0) / (n - 1)
        dz = (z1 - z0) / (n - 1)
        # Positions
        x = [x0 + k * dx for k in range(n)]
        y = [y0 + k * dy for k in range(n)]
        z = [z0 + k * dz for k in range(n)]
        dx = dxyz * gradient_axis[0]
        dy = dxyz * gradient_axis[1]
        dz = dxyz * gradient_axis[2]
        dnorm = (dx ** 2 + dy ** 2 + dz ** 2) ** 0.5
        xyz_0 = [[x[k], y[k], z[k]] for k in range(n)]
        xyz_1 = [[x[k] + dx, y[k] + dy, z[k] + dz] for k in range(n)]
        xyz_2 = [[x[k] - dx, y[k] - dy, z[k] - dz] for k in range(n)]
        # Distance to initial point
        d = [((x[k] - x[k - 1]) ** 2 + (y[k] - y[k - 1]) ** 2 + (z[k] - z[k - 1]) ** 2) ** 0.5 for k in range(1, n)]
        d = list(accumulate(d))
        d.insert(0, 0)

        # --- Field computations
        b_0 = rad.Fld(self.obj, b, xyz_0)
        b_1 = rad.Fld(self.obj, b, xyz_1)
        b_2 = rad.Fld(self.obj, b, xyz_2)
        s = [[1/2 * 1e6 * (b_1[k] - b_0[k] + b_2[k]) / dnorm ** 2] for k in range(n)]

        # --- Reshape
        x, y, z, d, s = np.array(x), np.array(y), np.array(z), np.array(d), np.array(s)
        s = s.reshape(x.shape)

        # --- Return
        return x, y, z, d, s

        # -------------------------------------------

    def plot_sextu_strength(self, xyz_end, xyz_start=[0, 0, 0], n=100, b='bz', gradient_axis=[1, 0, 0], dxyz=1,
                            x_axis='d', plot_show=True, plot_title=''):
        """
        Compute and plot the sextupole strength along a straight line
        :param xyz_end: end point [x, y, z]
        :param xyz_start: starting point [x, y, z] (default = [0, 0, 0])
        :param n: number of points (default = 100)
        :param b: field component (default = 'bz')
        :param gradient_axis: direction of the gradient
        :param dxyz=1: step for gradient computation (mm)
        :param x_axis: defines the x axis of the plot, x_axis = 'x', 'y', 'z' or 'd' (default = 'd', i.e. distance)
        :param show: show the plot if True
        :param plot_title: plot title
        """

        # --- Compute the field
        x, y, z, d, s = self.sextu_strength(xyz_end=xyz_end, xyz_start=xyz_start, n=n, b=b, gradient_axis=gradient_axis,
                                      dxyz=dxyz)
        # --- Plot
        fig = figure()
        if x_axis == 'x':
            l = x
        elif x_axis == 'y':
            l = y
        elif x_axis == 'z':
            l = z
        else:
            l = d
        plot(l, s)
        xlabel('Distance (mm)')
        ylabel('Sextupole strength (T/m^2)')
        title(plot_title)
        if plot_show:
            show()

    # -------------------------------------------
    def plot2d_field(self, xyz_centre=[0,0,0], xyz_max=[100,100], xyz_min=[0,0], n_points=[10,10], b='bz',
                     norm_axis='y', n_ticks=5):
        """
        Compute the field on a 2D mesh and plot it
        :param xyz_centre=[0,0,0]: centre of the mesh (mm)
        :param xyz_max=[100,100]: max. values of first and second coordinates (mm)
        :param xyz_min=[0,0]: max. values of first and second coordinates (mm)
        :param n_points=[10,10]: number of points
        :param b='bz': component of the field
        :param norm_axis='y': normal axis
        :param n_ticks=5: number of ticks
        :return: b_xyz_2d, xyz: field values (T), positions (mm)
        """
        # --- 2D grid
        x0, y0, z0 = xyz_centre
        if norm_axis == 'x':
            y_max, z_max = xyz_max
            y_min, z_min = xyz_min
            ny, nz = n_points
            dy, dz = (y_max - y_min) / (ny - 1), (z_max - z_min) / (nz - 1)
            y = [y_min + k * dy for k in range(ny)]
            z = [z_min + k * dz for k in range(nz)]
            xyz = [[x0, y[k], z[l]] for k in range(ny) for l in range(nz)]
        elif norm_axis == 'y':
            x_max, z_max = xyz_max
            x_min, z_min = xyz_min
            nx, nz = n_points
            dx, dz = (x_max - x_min) / (nx - 1), (z_max - z_min) / (nz - 1)
            x = [x_min + k * dx for k in range(nx)]
            z = [z_min + k * dz for k in range(nz)]
            xyz = [[x[k], y0, z[l]] for k in range(nx) for l in range(nz)]
        elif norm_axis == 'z':
            x_max, y_max = xyz_max
            x_min, y_min = xyz_min
            nx, ny = n_points
            dx, dy = (x_max - x_min) / (nx - 1), (y_max - y_min) / (ny - 1)
            x = [x_min + k * dx for k in range(nx)]
            y = [y_min + k * dy for k in range(ny)]
            xyz = [[x[k], y[k], z0] for k in range(nx) for l in range(ny)]

        # --- Compute the field
        if b != 'bnorm':
            b_xyz = rad.Fld(self.obj, b, xyz)
        else:
            bx_xyz = rad.Fld(self.obj, 'bx', xyz)
            by_xyz = rad.Fld(self.obj, 'by', xyz)
            bz_xyz = rad.Fld(self.obj, 'bz', xyz)
            b_xyz = [(bx_xyz[k] ** 2 + by_xyz[k] ** 2 + bz_xyz[k] ** 2) ** 0.5 for k in range(len(bx_xyz))]

        # --- Reshape and plot
        b_xyz_2d = np.array(b_xyz)
        b_xyz_2d = b_xyz_2d.reshape((n_points[0], n_points[1]))
        figure()
        imshow(b_xyz_2d)
        colorbar()
        dx_ticks = int(n_points[0] / n_ticks)
        dy_ticks = int(n_points[1] / n_ticks)
        xpos =  range(0, n_points[0], dx_ticks)
        ypos = range(0, n_points[1], dy_ticks)
        if norm_axis == 'x':
            xlabels = np.arange(y_min, y_max, (y_max - y_min) / n_ticks)
            ylabels = np.arange(z_min, z_max, (z_max - z_min) / n_ticks)
        elif norm_axis == 'y':
            xlabels = np.arange(x_min, x_max, (x_max - x_min) / n_ticks)
            ylabels = np.arange(z_min, z_max, (z_max - z_min) / n_ticks)
        elif norm_axis == 'z':
            xlabels = np.arange(x_min, x_max, (x_max - x_min) / n_ticks)
            ylabels = np.arange(y_min, y_max, (y_max - y_min) / n_ticks)

        xticks(xpos, np.around(xlabels, 1))
        yticks(ypos, np.around(ylabels, 1))
        xlabel('Distance (mm)')
        ylabel('Distance (mm)')

        return b_xyz_2d, np.array(xyz)

    # -------------------------------------------
    def traj(self, e=6, init_cond=[0, 0, 0, 0], y_range=None, n_points=100):
        """
        Compute the trajectory of an electron in the magnet
        :param e: energy of the electron (GeV) (default: 6 GeV)
        :param init_cond: initial coordinates (mm) and angles (rad) [x0, dx/dy0, z0, dz/dy0] (default: [0, 0, 0, 0]).
                Several initial conditions can be passed as a list [init_cond_0, init_cond_1, etc.]
        :param y_range: initial and final value of the longitudinal coordinate (mm)
                                    (default: +/- 3 periods before the limits of the object)
        :param n_points: number of points (default: 100)
        :return: the trajectory: n_points lists of [[y0, x0, dxdy0, z0, dzdy0], [y1, x1, dxdy1, z1, dzdy1], etc.]
        """

        # --- Limits
        if y_range is None:
            lim = rad.ObjGeoLim(self.obj)[3] + 2 * self.length()
            y_range = [-lim, lim]

        # --- Are the initial conditions for one or more electrons?
        init_cond_list = [init_cond]
        if isinstance(init_cond[0], list):
            init_cond_list = init_cond

        trj_tr_list = []
        for init_cond_k in init_cond_list:
            # --- Compute the trajectory
            trj = rad.FldPtcTrj(self.obj, e, init_cond_k, y_range, n_points)
            # --- Transpose
            trj_np = np.array(trj)
            trj_tr = np.ndarray.tolist(trj_np.transpose())
            # --- Append
            trj_tr_list.append(trj_tr)

        # --- Return the trajectory
        return trj_tr_list

    # -------------------------------------------
    def plot_traj(self, e=6, init_cond=[0, 0, 0, 0], y_range=None, n_points=100, x_or_z='x',
                  plot_show=True, trj_list=None, plot_title=''):
        """
        Compute and plot the trajectory of a particle in the magnet
        :param e: energy of the electron (GeV) (default: 6 GeV)
        :param init_cond: initial coordinates (mm) and angles (rad) [x0, dx/dy0, z0, dz/dy0] (default: [0, 0, 0, 0]).
            Several initial conditions can be passed as a list [init_cond_0, init_cond_1, etc.]
        :param y_range: initial and final value of the longitudinal coordinate (mm)
                            (default: +/- 3 periods before the limits of the object)
        :param n_points: number of points (default: 100)
        :param x_or_z: select the trajectory component to plot
                        -- 'x': horizontal position (default)
                        -- 'z': vertical position
                        -- 'dxdy': horizontal angle
                        -- 'dzdy': vertical angle
        :param plot_show: show the plot if True
        :param trj_list: pre-computed list of trajectories to be plot (trajectories are not computed if not default=None)
        :param plot_title: title of the plot
        :return: a matplotlib figure
        """

        # --- Compute the trajectory
        if trj_list is None:
            trj_list = self.traj(e, init_cond, y_range, n_points)
        # --- Plot
        fig = figure()
        for trj_list_k in trj_list:
            y, x, dxdy, z, dzdy = trj_list_k
            if x_or_z == 'x':
                trj = x
            elif x_or_z == 'z':
                trj = z
            elif x_or_z == 'dxdy':
                trj = dxdy
            elif x_or_z == 'dzdy':
                trj = dzdy
            else:
                trj = y
            plot(y, trj)

        if x_or_z == 'x':
            ylab = 'Horiz. position (mm)'
        elif x_or_z == 'z':
            ylab = 'Vert. position (mm)'
        elif x_or_z == 'dxdy':
            ylab = 'Horiz. angle (rad)'
        elif x_or_z == 'dzdy':
            ylab = 'Vert. angle (rad)'
        else:
            ylab = 'Long. position (mm)'
        xlabel('Long. position (mm)')
        ylabel(ylab)
        title(plot_title)
        if plot_show:
            show()
        return fig

    # -------------------------------------------
    def fit_quad(self, end_effect=False, end_effect_sym=False, x_xp=[5, 0], x_xp_ref=None,
                 n_part=4, y_range=None, y0=0, grad=None, e=6, trj=None, n_points=100):
        """
        Fit a thick quadrupole model to tracked particle trajectories
        :param end_effect=False: Fit thin lenses at quad extremities if True
        :param end_effect_sym=False: Fit symmetric thin lenses at quad extremities if True
        :param x_xp=[5, 0]: initial conditions are [x, x', 0, 0] if focusing, else [0, 0, x, x'] (mm, rad)
        :param x_xp_ref=None: initial conditions of the reference trajectory [(mm), (rad)]
        :param n_part=4: number of particles to track
        :param y_range=None: particles tracked within [y_start, y_end] if specified (mm)
        :param y0=0: longitudinal position (mm)
        :param grad=None: pre-computed gradient vs. longitudinal position (computed if None)
        :param e=6: energy (GeV)
        :param trj=None: pre-computed trajectories (computed if None)
        :param trj_ref_calc=True: Compute the reference trajectory (x_xp=[0, 0]) if True
        :param n_points: number of steps for trajectory computations
        :return results, trj, grad:
                    results:
                     -- [gl, l_mag, g0, res] if end_effect=False
                     -- [gl, l_mag, g0, gl_in, gl_out, res] if end_effect=True
                        where   gl is the integrated field in (T),
                                l_mag is the magnetic length in (mm)
                                g0 is the central gradient in (T/m)
                                gl_in, gl_out are the strengths of input and output thin lenses
                                res is an OptimizeResult object (output of minimize())
                    trj: particle trajectories, see self.traj()
                    grad: gradient, see self.gradient()
        """

        # --- Limits
        lim = 2 * rad.ObjGeoLim(self.obj)[3]
        if y_range is None:
            y_range = [-lim, lim]

        # --- Compute the gradient
        if grad is None:
            x, y, z, d, g = self.gradient([0, lim, 0], xyz_start=[0, - lim, 0], dxyz=0.1)
        else:
            x, y, z, d, g = grad
        grad = x, y, z, d, g

        # --- Compute the magnetic length from the gradient
        # Maximum value of the gradient (T/m)
        g_min, g_max = np.abs(np.min(g)), np.abs(np.max(g))
        if g_min > g_max:
            g_max = g_min
        # Gradient integral (T)
        s = self.strengths(r0=x_xp[0])
        gl = s[1].real
        # Focusing or not?
        focusing = True
        if gl < 0:
            focusing = False
        gl = abs(gl)
        # Magnetic length (mm)
        l_mag = 1000 * gl / g_max

        # --- Compute the trajectories
        if trj is None:
            # Initial conditions
            if n_part == 1:
                if focusing:
                    init_cond = [[x_xp[0], x_xp[1], 0, 0]]
                else:
                    init_cond = [[0, 0, x_xp[0], x_xp[1]]]
            else:
                init_cond = [[] for k in range(n_part)]
                for k in range(n_part):
                    if focusing:
                        init_cond[k] = [uniform(-x_xp[0], x_xp[0]), uniform(-x_xp[1], x_xp[1]), 0, 0]
                    else:
                        init_cond[k] = [0, 0, uniform(-x_xp[0], x_xp[0]), uniform(-x_xp[1], x_xp[1])]
            # Trajectory
            trj = self.traj(e, init_cond, y_range, n_points=n_points)
        else:
            n_part = len(trj)

        # --- Reference trajectory
        if x_xp_ref is not None:
            if len(x_xp_ref) == 2:
                init_cond = [x_xp_ref[0], x_xp_ref[1], 0, 0]
                trj_ref = np.array(self.traj(e, init_cond, y_range, n_points=n_points))
                # --- Substract the reference
                for k, trj_k in enumerate(trj):
                    trj_k_np = np.array(trj_k)
                    for n in range(1, 5):
                        trj_k_np[n] -= trj_ref[0][n]
                    trj[k] = trj_k_np.tolist()

        def drift_quad_drift_trj(l, gl_in, gl_out, gl, y0, trj, gamma, focusing=True, sym=False):
            """
             Track the particles with a drift - quad - drift matrix
             :param l: quadrupole length (mm)
             :param gl_in: integrated gradient of input thin lens (T)
             :param gl_out: integrated gradient of output thin lens (T)
             :param gl: integrated gradient (T)
             :param y0: longitudinal position (mm)
             :param gamma: Lorentz factor
             :param focusing=True: assumes horizontal focusing if True, else vertical focusing
             :param sym=False: force input / output symmetry if True
             :return the trajectory
            """
            # --- Initialize
            trj_model = deepcopy(trj)
            y = trj[0][0]
            c, e, me = 299792458, 1.6021766e-19, 9.109e-31  # Constants
            # Convert lengths to m
            l_m = l / 1000
            g = gl / l_m  # T/m
            k = g / (me * c * gamma / e)  # Normalized gradient
            if gl_in is not None and gl_out is not None:
                kl_in = gl_in / (me * c * gamma / e)
                if sym:
                    kl_out = kl_in
                else:
                    kl_out = gl_out / (me * c * gamma / e)
            else:
                kl_in, kl_out = 0, 0

            y0_m, y_start_m = y0 / 1000, y[0] / 1000
            d_mat1 = np.array([[1, y0_m - y_start_m - l_m / 2], [0, 1]])
            q_mat = np.array([[cos(k ** 0.5 * l_m), 1 / k ** 0.5 * sin(k ** 0.5 * l_m)],
                              [- k ** 0.5 * sin(k ** 0.5 * l_m), cos(k ** 0.5 * l_m)]])  # Thick quad matrix
            q_mat_in = np.array([[1, 0], [- kl_in, 1]])
            q_mat_out = np.array([[1, 0], [- kl_out, 1]])
            # --- Tracking
            for i, y in enumerate(trj[0][0][1:]):
                y_m = y / 1000
                # Transfer matrix
                if y < y0 - l / 2:
                    mat = np.array([[1,  y_m - y_start_m],[0, 1]])
                elif y < y0 + l/2:
                    l_q_m = y_m - y0_m + l_m / 2
                    mat_i = np.array([[cos(k ** 0.5 * l_q_m), 1 / k ** 0.5 * sin(k ** 0.5 * l_q_m)],
                                      [- k ** 0.5 * sin(k ** 0.5 * l_q_m), cos(k ** 0.5 * l_q_m)]])
                    mat = mat_i @ q_mat_in @ d_mat1
                else:
                    mat_i = np.array([[1, y_m - y0_m - l_m /2],[0, 1]])
                    mat = mat_i @ q_mat_out @ q_mat @ q_mat_in @ d_mat1
                # New positions and angles
                for j in range(len(trj)):
                    if focusing:
                        x_xp_j = np.array([trj[j][1][0] / 1000, trj[j][2][0]])
                    else:
                        x_xp_j = np.array([trj[j][3][0] / 1000, trj[j][4][0]])
                    x_xp = mat @ x_xp_j
                    # Store the results
                    trj_model[j][0][i + 1] = y
                    if focusing:
                        trj_model[j][1][i + 1] = 1000 * x_xp[0]
                        trj_model[j][2][i + 1] = x_xp[1]
                        trj_model[j][3][i + 1] = 0
                        trj_model[j][4][i + 1] = 0
                    else:
                        trj_model[j][1][i + 1] = 0
                        trj_model[j][2][i + 1] = 0
                        trj_model[j][3][i + 1] = 1000 * x_xp[0]
                        trj_model[j][4][i + 1] = x_xp[1]
            # Return the trajectory
            return trj_model

        def err_quad_fit_trj(p, gl, y0, e, trj, focusing, sym=False):
            """
            Compute the fit error for the given parameters
            Use all points of the trajectory
            :param p: [length (mm), (gl_in (T), gl_out (T))] Parameter to be optimized
            :param gl: integrated gradient (T)
            :param l: quadrupole length (mm)
            :param g: gradient (T/m)
            :param y0: longitudinal position (mm)
            :param e: energy (GeV)
            :param trj: Trajectories
            :param focusing: True for focusing quads
            :param sym: force the input / output symmetry if Trus
            :return the sum of quadratic errors
            """
            # Parameters
            if not end_effect:
                l = p[0]
                gl_main = gl
                gl_in, gl_out = None, None
            else:
                if not sym:
                    l, gl_in, gl_out = p
                    gl_main = gl - gl_in - gl_out
                else:
                    l, gl_in = p
                    gl_main = gl - 2 * gl_in
                    gl_out = gl_in
            # Model
            trj_model = drift_quad_drift_trj(l, gl_in, gl_out, gl_main, y0, trj, 1957 * e, focusing=focusing, sym=sym)
            # Error
            err = 0
            for k in range(len(trj)):
                if focusing:
                    x_ref, xp_ref = np.array(trj[k][1]), np.array(trj[k][2])
                    x_mod, xp_mod = np.array(trj_model[k][1]), np.array(trj_model[k][2])
                else:
                    x_ref, xp_ref = np.array(trj[k][3]), np.array(trj[k][4])
                    x_mod, xp_mod = np.array(trj_model[k][3]), np.array(trj_model[k][4])
                err += np.linalg.norm(x_mod - x_ref) + np.linalg.norm(xp_mod[k] - xp_ref)
            return err

        if not end_effect:
            res = minimize(err_quad_fit_trj, [l_mag], args=(gl, y0, e, trj, focusing),
                           method='Nelder-Mead')
            l_mag_fit = res['x'][0]
            result = [gl, l_mag_fit, 1000 * gl / l_mag_fit, res]
        else:
            if not end_effect_sym:
                res = minimize(err_quad_fit_trj, [l_mag, 0, 0], args=(gl, y0, e, trj, focusing, False),
                               method='Nelder-Mead')
                l_mag_fit, gl_in, gl_out = res['x']
                result = [gl, l_mag_fit, 1000 * gl / l_mag_fit, gl_in, gl_out, res]
            else:
                res = minimize(err_quad_fit_trj, [l_mag, 0], args=(gl, y0, e, trj, focusing, True),
                               method='Nelder-Mead')
                l_mag_fit, gl_in = res['x']
                result = [gl, l_mag_fit, 1000 * gl / l_mag_fit, gl_in, gl_in, res]

        # --- Resuts
        return result, trj, grad

    # -------------------------------------------
    def save(self, filename, path=''):
        """
        Save the radia object to filename.rad and its parameters to filename.mmp.
        Use the load() method for loading the radia object.

        The file filename.rad can be opened with the Mathematica interface, using the RadUtiLoad[] function

        :param filename: file name without extension
        :param path='': absolute path if specified (default: relative path)
        """

        try:
            obj = self.obj
            params = self.params
        except AttributeError:
            raise Exception('Missing obj in MM.save()')

        # --- Save the magnet
        # Dump the undulator
        dmp = rad.UtiDmp(self.obj, 'bin')
        # Write to a file
        f = open(path + filename + '.rad', 'wb')
        f.write(dmp)
        f.close()

        # --- Save the parameters
        f = open(path + filename + '.mmp', 'wb')
        pickle.dump(self.params, f)
        f.close()

    # -------------------------------------------
    def export(self, filename, path=''):
        """
        Save the Radia model of the magnet to filename.rad
        :param filename: file name without extension
        :param path: absolute path if specified (default = '', i.e. relative path)
        """
        try:
            rad_uti.save(self.obj, filename, path=path)
        except AttributeError:
            raise Exception('Missing obj in MM.export()')

    # -------------------------------------------
    def load(self, filename, path=None):
        """
        Load the radia object from filename.rad and its parameters from filename.mmp.

        The file filename.rad can be also opened with the Mathematica interface, using the RadUtiLoad[] function

        :param filename: file name without extension
        :param path=None: absolute path if specified (default: relative path)
        """
        # --- Load the parameters
        if path is None:
            path = ''
        try:
            f = open(path + filename + '.mmp', 'rb')
            self.params = pickle.load(f)
            f.close()
        except FileNotFoundError:
            print('Warning: File.mmp not found in MM.load()')
            return

        # --- Load the magnet
        try:
            f = open(path + filename + '.rad', 'rb')
            obj_bin = f.read()
            f.close()
            self.obj = rad.UtiDmpPrs(obj_bin)
        except FileNotFoundError:
            raise Exception('File not found in MM.load()')

    # -------------------------------------------
    def rad_obj_to_bytes(self):
        """"
        Returns binaries from self.obj
        """

        return rad.UtiDmp(self.obj, 'bin')

    # -------------------------------------------
    def bytes_to_rad_obj(self, byte_obj):
        obj = rad.UtiDmpPrs(byte_obj)

    # -------------------------------------------
    def multipoles(self, r0=10, xz0=[0,0], n_multi=32):
        """

        Basic 2D circular multipole computations
        :param r0=10: reference radius (mm)
        :param xz0=[0,0]: Centre [x0, z0] (mm)
        :param n_multi=32: number of multipole coefficients
        :return multi, ib, ib_r, xz: complex numpy arrays: multipoles (Tmm), integrated fields ibz + 1j * ibx (Tmm),
                radial field (Tmm), positions (mm)
        """
        # Compute the integrated field
        ib = np.zeros(n_multi, dtype='complex64')
        xz = np.zeros(n_multi, dtype='complex64')
        alpha = np.zeros(n_multi)
        x0, z0 = xz0
        for n in range(n_multi):
            alpha[n] = n * 2 * pi / n_multi
            x, z = r0 * cos(alpha[n]), r0 * sin(alpha[n])
            ibx, ibz = rad.FldInt(self.obj, 'inf', 'ibxibz', [x + x0, -1000, z + z0], [x + x0, 1000, z + z0])
            ib[n] = ibz + 1j * ibx
            xz[n] = x + 1j * z
        # FFT
        ib_r = np.real(ib * np.exp(1j * alpha))  # Compute the radial field
        tf = np.fft.fft(ib_r) / n_multi
        # Rearrange the terms
        multi = 2 * tf[1:int(n_multi / 2)]  # Delete the first element corresponding to a '0-pole'
        # takes only the positive frequency terms and multiply by 2 to get the total amplitude
        # Return
        return multi, ib, ib_r, xz

    # -------------------------------------------
    def multipoles_line_arc(self, r0=10, xz0=[0,0], r_bend=None, theta_bend=None, y0=1000, n_multi=32, n_points=100):
        """
        Basix pseudo multipoles integrated on line arc trajectory
        :param r0=10: reference radius (mm)
        :param xz0=[0,0]: Centre [x0, z0] (mm)
        :param r_bend=None: bending angle (mm) (the the yoke bending angle if None)
        :param theta_bend=None: bending angle for the arc (rad) (use the yoke bending angle if None)
        :param y0=1000: final longitudinal position
        :param n_multi=32: number of multipole coefficients
        :param n_points=100: number of points in the longitudinal direction
        :return multi, ib, ib_r, xz, [x_ref, y_ref]: complex numpy arrays: multipoles (Tmm), integrated fields ibz + 1j * ibx (Tmm),
                radial field (Tmm), positions (mm), [x, y reference path (mm)]
        """
        # Get the parameters
        if r_bend is None:
            try:
                r_bend = self.yoke.yoke_params.r_bend
            except AttributeError:
                raise Exception('Attribute error in MM.multipoles_line_arc()')
        if theta_bend is None:
            try:
                l0 = self.yoke.yoke_params.length
            except AttributeError:
                raise Exception('Attribute error in MM.multipoles_line_arc()')
            theta_bend = 2 * asin(l0 / (2 * r_bend))
        # Define the reference line-arc trajectory
        dy = y0 / (n_points - 1)
        x = [0.] * n_points
        y = [dy * k for k in range (n_points)]
        theta = [0.] * n_points
        x0, z0 = xz0
        for k in range (n_points):
            theta[k] = atan(y[k]/ r_bend)
            if theta[k] <= theta_bend / 2:
                x[k] = x0 + r_bend * (1 - cos(theta[k]))
            else:
                x[k] = x[k - 1] + dy * tan(theta[k])
        # Compute the field integrals
        ib = np.zeros(n_multi, dtype='complex64')
        xz = np.zeros(n_multi, dtype='complex64')
        alpha = np.zeros(n_multi)
        for n in range(n_multi):
            alpha[n] = n * 2 * pi / n_multi
            xn, zn = r0 * cos(alpha[n]), r0 * sin(alpha[n])
            ibxyz = [rad.FldInt(self.obj, 'fin', 'ibxibyibz', [x[k] + xn, y[k], z0 + zn],
                                      [x[k + 1] + xn, y[k + 1], z0 + zn]) for k in range(n_points - 1)]
            ibx, ibz = 0, 0
            for k in range(n_points - 1):
                ibx += cos(theta[k]) * ibxyz[k][0] - sin(theta[k]) * ibxyz[k][1]
                ibz += ibxyz[k][2]
            ib[n] = 2 * (ibz + 1j * ibx)
            xz[n] = xn + 1j * zn
        # FFT
        ib_r = np.real(ib * np.exp(1j * alpha)) # Compute the radial field
        tf = np.fft.fft(ib_r) / n_multi
        # Rearrange the terms
        multi = 2 * tf[1:int(n_multi / 2)] # Delete the first element corresponding to a '0-pole'
                                            # takes only the positive frequency terms and multiply by 2 to get the total amplitude
        # Return
        return multi, ib, ib_r, xz, [x, y]

    # -------------------------------------------
    def strengths(self, r0=10, xz0=[0,0], n_multi=32, b_rho=1):
        """
        Basic 2D multipole strength computations
        param r0=10: reference radius (mm)
        :param xz0=[0,0]: Centre [x0, z0] (mm)
        :param n_multi=32: number of multipole strength coefficients
        :param b_rho=1: magnetic rigidity (Tmm)
        :return: strengths: complex numpy array containing the multipole strengths
        """
        # Compute the multipoles
        multi, ib, ib_r, xz = self.multipoles(r0, xz0, n_multi)
        # Normalize the strengths
        strengths = np.zeros(multi.size, dtype='complex64')
        for k, multi_k in enumerate(multi):
            strengths[k] = multi_k / r0 ** k / b_rho
        # Return
        return strengths

    # -------------------------------------------
    def sym_break(self):
        """
        Break the symmetries of the object
        """
        try:
            new = rad.ObjDpl(self.obj, 'FreeSym->True')
            # rad.UtiDel(self.obj)
            self.obj = new
        except AttributeError:
            raise Exception('Attribute error in MM.break_sym()')

    # -------------------------------------------
    def sym_n_fold(self, n, v_dir=[0,1,0]):
        """
        Create a n fold symmetry
        :param n: order of the symmetry (2: 2-fold, 4: 4-fold, etc)
        :param v_dir=[0,1,0]: longitudinal axis vector
        """
        try:
            rot = rad.TrfRot([0, 0, 0], v_dir, 2 * pi / n)
            inv = rad.TrfInv()
            trf = rad.TrfCmbL(rot, inv)
            rad.TrfMlt(self.obj, trf, n)
        except AttributeError:
            raise Exception('Attribute error in MM.sym_n_fold()')

    # -------------------------------------------
    def sym_2_fold(self, v_dir=[0,1,0]):
        """
        Create a 2 fold symetry
        :param v_dir=[0,1,0]: longitudinal axis vector
        """
        self.sym_n_fold(2, v_dir)

    # -------------------------------------------
    def sym_4_fold(self, v_dir=[0,1,0]):
        """
        Create a 4 fold symetry
        :param v_dir=[0,1,0]: longitudinal axis vector
        """
        self.sym_n_fold(4, v_dir)

    # -------------------------------------------
    def sym_6_fold(self, v_dir=[0,1,0]):
        """
        Create a 6 fold symetry
        :param v_dir=[0,1,0]: longitudinal axis vector
        """
        self.sym_n_fold(6, v_dir)

    # -------------------------------------------
    def sym_zer_para(self, plane, point=[0,0,0]):
        """
        Set the field component in the plane to zero
        :param plane: normal vector to the symmetry plane
        :param point=[0,0,0]: point defining the symmetry plane
        """
        try:
            rad.TrfZerPara(self.obj, point, plane)
        except AttributeError:
            raise Exception('Attribute error in MM.sym_zer_para()')

    # -------------------------------------------
    def sym_zer_perp(self, plane, point=[0, 0, 0]):
        """
        Set the field component perpendicular to the plane to zero
        :param plane: normal vector to the symmetry plane
        :param point=[0,0,0]: point defining the symmetry plane
        :return: 0 if no error, else 1
        """
        try:
            rad.TrfZerPerp(self.obj, point, plane)
        except AttributeError:
            raise Exception('Attribute error in MM.sym_zer_para()')

    # -------------------------------------------
    def sym_vert(self):
        """
        Vertical field in the symmetry plane
        Force the field to be parallel to the z azis in the horizontal plane [0, 0, 1] passing through [0, 0, 0]
        """
        self.sym_zer_para([0, 0, 1])

    # -------------------------------------------
    def sym_horiz(self):
        """
        Horizontal field in the symmetry plane
        Force the field to be parallel to the x axis in the vertical plane [1, 0, 0] passing through [0, 0, 0]
        """
        self.sym_zer_para([1, 0, 0])

    # -------------------------------------------
    def sym_long(self):
        """
        Longitudinal field in the symmetry plane
        Force the field to be parallel to the y axis in the vertical plane [0, 1, 0] passing through [0, 0, 0]
        """
        self.sym_zer_para([0, 1, 0])

    # -------------------------------------------
    def top(self, tolerance=1e-3):
        """
        Drops the bottom of the magnet
        WARNING: this function assumes that the top and bottom part of the magnet were build using symmetries.
        Consider using rad.ObjCutMag if this is not the case
        :param tolerance=1e-3: tolerance parameter
        """
        try:
            # --- Break the symmetries
            self.sym_break()
            # --- Separate the top and bottom of the magnet
            obj_list = rad.ObjCntStuf(self.obj)
            top = rad.ObjCnt([])
            for obj_tmp in obj_list:
                geo_lim = rad.ObjGeoLim(obj_tmp)
                z_min, z_max = geo_lim[4], geo_lim[5]
                if z_max > tolerance and z_min > - z_max * tolerance:
                    rad.ObjAddToCnt(top, [obj_tmp])
                else:
                    rad.UtiDel(obj_tmp)
            self.obj = top
        except AttributeError:
            raise Exception('attribute error in MM.top()')

    # -------------------------------------------
    def bottom(self, tolerance=1e-3):
        """
        Drops the top of the magnet
        WARNING: this function assumes that the top and bottom part of the magnet were build using symmetries.
        Consider using rad.ObjCutMag if this is not the case
        :param tolerance=1e-3: tolerance parameter
        """
        try:
            # --- Break the symmetries
            self.sym_break()
            # --- Separate the top and bottom of the magnet
            obj_list = rad.ObjCntStuf(self.obj)
            bottom = rad.ObjCnt([])
            for obj_tmp in obj_list:
                geo_lim = rad.ObjGeoLim(obj_tmp)
                z_min, z_max = geo_lim[4], geo_lim[5]
                if z_max < tolerance or z_min < - z_max * tolerance:
                    rad.ObjAddToCnt(bottom, [obj_tmp])
                else:
                    rad.UtiDel(obj_tmp)
            self.obj = bottom
        except AttributeError:
            raise Exception('attribute error in MM.bottom()')

    # -------------------------------------------
    def translate(self, dist, axis='y'):
        """
        Translate the object in the specified direction
        :param dist: distance of the translation (mm)
        :param axis='y': direction of the translation ('x', '-x', 'y', '-y', 'z', '-z')
        """
        try:
            if axis.lower() == 'x':
                v = [dist, 0, 0]
            elif axis.lower() == '-x':
                v = [-dist, 0, 0]
            elif axis.lower() == 'y':
                v = [0, dist, 0]
            elif axis.lower() == '-y':
                v = [0, -dist, 0]
            elif axis.lower() == 'z':
                v = [0, 0, dist]
            elif axis.lower() == '-z':
                v = [0, 0, - dist]
            else:
                v = [0, dist, 0]
            tr = rad.TrfTrsl(v)
            rad.TrfOrnt(self.obj, tr)
        except AttributeError:
            raise Exception('attribute error in MM.translate()')

    # -------------------------------------------
    def length(self, axis='y'):
        """
        Length of the object in the direction specified by axis
        :param axis='y': direction in which the length is measured
        :return: the length of the object, or 0
        """
        try:
            geo_lim = rad.ObjGeoLim(self.obj)
            if axis.lower() == 'x':
                len = geo_lim[1] - geo_lim[0]
            elif axis.lower() == 'y':
                len = geo_lim[3] - geo_lim[2]
            elif axis.lower() == 'z':
                len = geo_lim[5] - geo_lim[4]
            return len
        except AttributeError:
            raise Exception('obj not defined in MM.length()')

    # -------------------------------------------
    def length_yoke(self, axis='y'):
        """
        Length of the object's yoke in the direction specified by axis
        :param axis='y': direction in which the length is measured
        :return: the length of the object, or 0
        """
        try:
            geo_lim = rad.ObjGeoLim(self.yoke.obj)
            if axis.lower() == 'x':
                len = geo_lim[1] - geo_lim[0]
            elif axis.lower() == 'y':
                len = geo_lim[3] - geo_lim[2]
            elif axis.lower() == 'z':
                len = geo_lim[5] - geo_lim[4]
            return len
        except AttributeError:
            raise Exception('obj not defined in MM.length()')

    # -------------------------------------------
    def length_coil(self, axis='y'):
        """
        Length of the object's coil in the direction specified by axis
        :param axis='y': direction in which the length is measured
        :return: the length of the object, or 0
        """
        try:
            geo_lim = rad.ObjGeoLim(self.coil.obj)
            if axis.lower() == 'x':
                len = geo_lim[1] - geo_lim[0]
            elif axis.lower() == 'y':
                len = geo_lim[3] - geo_lim[2]
            elif axis.lower() == 'z':
                len = geo_lim[5] - geo_lim[4]
            return len
        except AttributeError:
            raise Exception('obj not defined in MM.length()')

    # -------------------------------------------
    def current(self):
        """
        Returns the current of the magnet, if defined
        :return: the current (A)
        """
        try:
            return self.params.coil_params.current
        except AttributeError:
            try:
                return self.params.main_coil_params.current
            except AttributeError:
                return 0


# -------------------------------------------
# Base class for electromagnet yoke
# -------------------------------------------
class Yoke(MM):
    """Base class for multipole electromagnet yoke"""
    # -------------------------------------------
    def print_profile(self):
        """
        Print the yoke profile
        """
        try:
            self.yoke_profile.print()
        except AttributeError:
            raise Exception('Attribute error in Yoke.print_profile()')

    # -------------------------------------------
    def plot_profile(self):
        """
        Plot the yoke profile
        (Note that x and z may permuted with respect to the 3D object if the long. axis is 'y')
        """
        try:
            self.yoke_profile.plot()
        except AttributeError:
            raise Exception('Attribute error in Yoke.plot_profile()')


# -------------------------------------------
# Class for electromagnet quad yoke
# -------------------------------------------
class QuadEMYoke(Yoke):

    def __init__(self, yoke_params):
        """
        Initialize an electromagnet quadrupole yoke
        :param yoke_params: yoke parameters (see QuadEMYokeParam)
        """
        self.yoke_params = yoke_params

        self.build_yoke()

    # -------------------------------------------
    def build_yoke(self):
        """
        Build a yoke using the attributes stored in self.yoke_params
        The yoke is stored in self.yoke.
        """

        # --- Get the parameters
        try:
            pole_length = self.yoke_params.length
            chamfer =self.yoke_params.chamfer
            profile_params = self.yoke_params.profile_params
            mesh_params = self.yoke_params.mesh_params
            axis = self.yoke_params.axis
            color = self.yoke_params.color
            mat_name = self.yoke_params.mat
            theta_3 = self.yoke_params.profile_params.theta_3
        except AttributeError:
            raise Exception('Attribute error in QuadEMYoke.build_yoke()')

        # --- Pole profile
        self.yoke_profile = QuadEMYokeProfile(profile_params, mesh_params) # Build the profiles and subdivision lists
        self.yoke_profile.change_profile_to_axis(axis) # Permutation of the coordinates depending on the long. axis
        pole_profile = self.yoke_profile.pole_profile
        pole_sub = self.yoke_profile.pole_sub
        backleg_profile = self.yoke_profile.backleg_profile
        backleg_sub = self.yoke_profile.backleg_sub

        # --- Build the pole and yoke
        str_options = 'ki->Numb,TriAngMin->'+str(mesh_params.tri_ang_min)+',TriAreaMax->' + str(mesh_params.area_max)
        if chamfer is not None:
            chamfer_tip, chamfer_side = chamfer
        else:
            chamfer_tip, chamfer_side = [0, 0]
        if chamfer_side >= chamfer_tip:
            length = pole_length - 2 * chamfer_side
        else:
            length = pole_length - 2 * chamfer_tip
        # --- Main part
        # Extrusion
        self.pole = rad.ObjMltExtTri(length / 4, length / 2, pole_profile, pole_sub, axis, [0, 0, 0], str_options)
        self.backleg = rad.ObjMltExtTri(length / 4, length / 2, backleg_profile, backleg_sub, axis, [0, 0, 0],
                                        str_options)
        # Subdivision
        long_sub_pole = mesh_params.long_sub
        rad.ObjDivMag(self.pole, [long_sub_pole[0], 1, 1])
        rad.ObjDivMag(self.backleg, [long_sub_pole[0], 1, 1])
        # --- Extremities
        if chamfer_side >= chamfer_tip and chamfer_side > 0:
            ext_pos, ext_len = [length / 2 + chamfer_side / 2, chamfer_side]
        elif chamfer_tip > chamfer_side and chamfer_tip > 0:
            ext_pos, ext_len = [length / 2 + chamfer_tip / 2, chamfer_tip]
        else:
            ext_len = None
        if ext_len is not None:
            # Extrusion
            self.pole_ext = rad.ObjMltExtTri(ext_pos, ext_len, pole_profile, pole_sub, axis, [0, 0, 0], str_options)
            self.backleg_ext = rad.ObjMltExtTri(ext_pos, ext_len, backleg_profile,
                                                backleg_sub, axis, [0, 0, 0], str_options)
            # Subdivision
            long_sub_ext = mesh_params.long_sub_ext
            rad.ObjDivMag(self.pole_ext, [long_sub_ext[0], 1, 1])
            rad.ObjDivMag(self.backleg_ext, [long_sub_ext[0], 1, 1])
        else:
            self.pole_ext = None
            self.backleg_ext = None
        # --- Cut the chamfers
        # Pole tip
        if chamfer_tip > 0:
            x, z = pole_profile[0]
            norm = [2 ** 0.5, -1, -1]
            pos = [pole_length / 2 - chamfer_tip, x, z]
            if axis == 'y':
                norm = [norm[1], norm[0], norm[2]]
                pos = [pos[2], pos[0], pos[1]]
            if axis == 'z':
                norm = [norm[1], norm[2], norm[0]]
                pos = [pos[1], pos[2], pos[0]]
            self.pole_ext = rad.ObjCutMag(self.pole_ext, pos, norm, 'Frame->Lab')[0]
        # Pole side
        if chamfer_side > 0:
            x, z = pole_profile[-3]
            norm =  [1, sin(theta_3* pi / 180), -cos(theta_3* pi / 180)]
            pos = [pole_length / 2 - chamfer_side, x, z]
            if axis == 'y':
                norm = [norm[1], norm[0], norm[2]]
                pos = [pos[2], pos[0], pos[1]]
            if axis == 'z':
                norm = [norm[1], norm[2], norm[0]]
                pos = [pos[1], pos[2], pos[0]]
            self.pole_ext = rad.ObjCutMag(self.pole_ext, pos, norm, 'Frame->Lab')[0]

        # --- Set the material
        mat = rad_mat.set_soft_mat(mat_name)
        rad.MatApl(self.pole, mat)
        rad.MatApl(self.backleg, mat)
        if self.pole_ext is not None:
            rad.MatApl(self.pole_ext, mat)
        if self.backleg_ext is not None:
            rad.MatApl(self.backleg_ext, mat)

        # --- Color
        rad.ObjDrwAtr(self.pole, color)
        rad.ObjDrwAtr(self.backleg, color)
        if self.pole_ext is not None:
            rad.ObjDrwAtr(self.pole_ext, color)
        if self.backleg_ext is not None:
            rad.ObjDrwAtr(self.backleg_ext, color)

        # --- Yoke assembly
        if self.pole_ext is not None:
            rad.ObjAddToCnt(self.pole, [self.pole_ext])
        if self.backleg_ext is not None:
            rad.ObjAddToCnt(self.backleg, [self.backleg_ext])
        self.obj = rad.ObjCnt([self.pole, self.backleg])


# -------------------------------------------
# Class for electromagnet sextupole yoke
# -------------------------------------------
class SextuEMYoke(MM):

    def __init__(self, yoke_params):
        """
        Initialize an electromagnet sextupole yoke
        :param yoke_params: yoke parameters (see SextuEMYokeParam)
        """
        self.yoke_params = yoke_params

        self.build_yoke()

    def build_yoke(self):
        """
        Build a yoke using the attributes stored in self.yoke_params
        The yoke is stored in self.yoke.
        """

        # --- Get the parameters
        try:
            pole_length = self.yoke_params.length
            chamfer =self.yoke_params.chamfer
            profile_params = self.yoke_params.profile_params
            mesh_params = self.yoke_params.mesh_params
            axis = self.yoke_params.axis
            color = self.yoke_params.color
            mat_name = self.yoke_params.mat
            theta_3 = self.yoke_params.profile_params.theta_3
        except AttributeError:
            raise Exception('Attribute error in SextuEMYoke.build_yoke()')

        # --- Pole profile
        self.yoke_profile = SextuEMYokeProfile(profile_params, mesh_params) # Build the profiles and subdivision lists
        self.yoke_profile.change_profile_to_axis(axis) # Permutation of the coordinates depending on the long. axis
        pole_profile = self.yoke_profile.pole_profile
        pole_sub = self.yoke_profile.pole_sub
        backleg_profile = self.yoke_profile.backleg_profile
        backleg_sub = self.yoke_profile.backleg_sub

        # --- Build the pole and yoke
        str_options = 'ki->Numb,TriAngMin->'+str(mesh_params.tri_ang_min)+',TriAreaMax->' + str(mesh_params.area_max)
        if chamfer is not None:
            chamfer_tip, chamfer_side = chamfer
        else:
            chamfer_tip, chamfer_side = [0, 0]
        if chamfer_side >= chamfer_tip:
            length = pole_length - 2 * chamfer_side
        else:
            length = pole_length - 2 * chamfer_tip
        # --- Main part
        # Extrusion
        self.pole = rad.ObjMltExtTri(length / 4, length / 2, pole_profile, pole_sub, axis, [0, 0, 0], str_options)
        self.backleg = rad.ObjMltExtTri(length / 4, length / 2, backleg_profile, backleg_sub, axis, [0, 0, 0],
                                        str_options)
        # Subdivision
        long_sub_pole = mesh_params.long_sub
        rad.ObjDivMag(self.pole, [long_sub_pole[0], 1, 1])
        rad.ObjDivMag(self.backleg, [long_sub_pole[0], 1, 1])
        # --- Extremities
        if chamfer_side >= chamfer_tip and chamfer_side > 0:
            ext_pos, ext_len = [length / 2 + chamfer_side / 2, chamfer_side]
        elif chamfer_tip > chamfer_side and chamfer_tip > 0:
            ext_pos, ext_len = [length / 2 + chamfer_tip / 2, chamfer_tip]
        else:
            ext_len = None
        if ext_len is not None:
            # Extrusion
            self.pole_ext = rad.ObjMltExtTri(ext_pos, ext_len, pole_profile, pole_sub, axis, [0, 0, 0], str_options)
            self.backleg_ext = rad.ObjMltExtTri(ext_pos, ext_len, backleg_profile,
                                                backleg_sub, axis, [0, 0, 0], str_options)
            # Subdivision
            long_sub_ext = mesh_params.long_sub_ext
            rad.ObjDivMag(self.pole_ext, [long_sub_ext[0], 1, 1])
            rad.ObjDivMag(self.backleg_ext, [long_sub_ext[0], 1, 1])
        else:
            self.pole_ext = None
            self.backleg_ext = None
        # --- Cut the chamfers
        # Pole tip
        if chamfer_tip > 0:
            x, z = pole_profile[0]
            norm = [2 ** 0.5, -1, -1]
            pos = [pole_length / 2 - chamfer_tip, x, z]
            if axis == 'y':
                norm = [norm[1], norm[0], norm[2]]
                pos = [pos[2], pos[0], pos[1]]
            if axis == 'z':
                norm = [norm[1], norm[2], norm[0]]
                pos = [pos[1], pos[2], pos[0]]
            self.pole_ext = rad.ObjCutMag(self.pole_ext, pos, norm, 'Frame->Lab')[0]
        # Pole side
        if chamfer_side > 0:
            x, z = pole_profile[-3]
            norm =  [1, sin(theta_3* pi / 180), -cos(theta_3* pi / 180)]
            pos = [pole_length / 2 - chamfer_side, x, z]
            if axis == 'y':
                norm = [norm[1], norm[0], norm[2]]
                pos = [pos[2], pos[0], pos[1]]
            if axis == 'z':
                norm = [norm[1], norm[2], norm[0]]
                pos = [pos[1], pos[2], pos[0]]
            self.pole_ext = rad.ObjCutMag(self.pole_ext, pos, norm, 'Frame->Lab')[0]

        # --- Set the material
        mat = rad_mat.set_soft_mat(mat_name)
        rad.MatApl(self.pole, mat)
        rad.MatApl(self.backleg, mat)
        if self.pole_ext is not None:
            rad.MatApl(self.pole_ext, mat)
        if self.backleg_ext is not None:
            rad.MatApl(self.backleg_ext, mat)

        # --- Color
        rad.ObjDrwAtr(self.pole, color)
        rad.ObjDrwAtr(self.backleg, color)
        if self.pole_ext is not None:
            rad.ObjDrwAtr(self.pole_ext, color)
        if self.backleg_ext is not None:
            rad.ObjDrwAtr(self.backleg_ext, color)

        # --- Yoke assembly
        if self.pole_ext is not None:
            rad.ObjAddToCnt(self.pole, [self.pole_ext])
        if self.backleg_ext is not None:
            rad.ObjAddToCnt(self.backleg, [self.backleg_ext])
        self.obj = rad.ObjCnt([self.pole, self.backleg])


# -------------------------------------------
# Class for electromagnet octupole yoke
# -------------------------------------------
class OctuEMYoke(MM):

    def __init__(self, yoke_params):
        """
        Initialize an electromagnet octupole yoke
        :param yoke_params: yoke parameters (see OctuEMYokeParam)
        """
        self.yoke_params = yoke_params

        self.build_yoke()

    def build_yoke(self):
        """
        Build a yoke using the attributes stored in self.yoke_params
        The yoke is stored in self.yoke.
        """

        # --- Get the parameters
        try:
            pole_length = self.yoke_params.pole_length
            yoke_length = self.yoke_params.yoke_length
            chamfer =self.yoke_params.chamfer
            profile_params = self.yoke_params.profile_params
            mesh_params = self.yoke_params.mesh_params
            axis = self.yoke_params.axis
            color = self.yoke_params.color
            mat_name = self.yoke_params.mat
        except AttributeError:
            raise Exception('Attribute error in OctuEMYoke.build_yoke()')

        # --- Pole profile
        self.yoke_profile = OctuEMYokeProfile(profile_params, mesh_params) # Build the profiles and subdivision lists
        self.yoke_profile.change_profile_to_axis(axis) # Permutation of the coordinates depending on the long. axis
        pole_profile = self.yoke_profile.pole_profile
        pole_sub = self.yoke_profile.pole_sub
        backleg_profile = self.yoke_profile.backleg_profile
        backleg_sub = self.yoke_profile.backleg_sub

        # --- Build the pole and yoke
        str_options = 'ki->Numb,TriAngMin->'+str(mesh_params.tri_ang_min)+',TriAreaMax->' + str(mesh_params.area_max)
        if chamfer is not None:
            chamfer_tip, chamfer_yoke = chamfer
        else:
            chamfer_tip, chamfer_side = [0, 10]

        # --- Pole and yoke
        # Extrusion
        self.pole = rad.ObjMltExtTri(pole_length / 4, pole_length / 2, pole_profile, pole_sub, axis, [0, 0, 0],
                                     str_options)
        self.backleg = rad.ObjMltExtTri(pole_length / 4, pole_length / 2, backleg_profile, backleg_sub, axis, [0, 0, 0],
                                        str_options)
        # Subdivision
        long_sub_pole = mesh_params.long_sub
        rad.ObjDivMag(self.pole, [long_sub_pole[0], 1, 1])
        rad.ObjDivMag(self.backleg, [long_sub_pole[0], 1, 1])

        # Cut the yoke to its final dimensions
        # Cut 1
        if axis == 'x':
            pos = [yoke_length / 2, 0, 0]
            norm = [1, 0, 0]
        elif axis == 'y':
            pos = [0, yoke_length / 2, 0]
            norm = [0, 1, 0]
        else:
            pos = [0, 0, yoke_length / 2]
            norm = [0, 0, 1]
        self.backleg = rad.ObjCutMag(self.backleg, pos, norm, 'Frame->Lab')[0]
        # Cut 2 : upper chamfer
        x, z = backleg_profile[3]
        self.backleg = rad.ObjCutMag(self.backleg, [x, yoke_length / 2 - chamfer_yoke, z],
                                     [1, 1, 1], 'Frame->Lab')[0]
        # Cut 3 : lower chamfer
        x, z = backleg_profile[1]
        self.backleg = rad.ObjCutMag(self.backleg, [x, yoke_length / 2 - chamfer_yoke, z],
                                     [-1, 1, -1], 'Frame->Lab')[0]

        # --- Set the material
        mat = rad_mat.set_soft_mat(mat_name)
        rad.MatApl(self.pole, mat)
        rad.MatApl(self.backleg, mat)

        # --- Color
        rad.ObjDrwAtr(self.pole, color)
        rad.ObjDrwAtr(self.backleg, color)

        self.obj = rad.ObjCnt([self.pole, self.backleg])


# -------------------------------------------
# Class for dipole-quad yoke
# -------------------------------------------
class DipoleQuadEMYoke(Yoke):
    def __init__(self, yoke_params):
        """
        Initialize an dipole-quadrupole yoke
        :param yoke_params: yoke parameters (see DipoleQuadEMYokeParam)
        """
        self.yoke_params = yoke_params
        self.build_yoke()

    # -------------------------------------------
    def build_yoke(self):
        """
        Build a dipole-quadrupole yoke
        """
        # --- Get the parameters
        try:
            length = self.yoke_params.length
            profile_params = self.yoke_params.profile_params
            mesh_params = self.yoke_params.mesh_params
            axis = self.yoke_params.axis
            color = self.yoke_params.color
            mat_name = self.yoke_params.mat
            end = self.yoke_params.end
            n_segments = int(mesh_params.long_sub[0] / 2)
            if n_segments < 2:
                n_segments = 2
            r_bend = self.yoke_params.r_bend
            if r_bend is not None:
                angle_bend = asin(length / (2 * r_bend))
            else:
                angle_bend = 0
                r_bend = 1e6;
        except AttributeError:
            raise Exception('Attribute error in DipoleQuadEMYoke.build_yoke()')

        # --- Pole profile and subdivision list
        self.yoke_profile = DipoleQuadEMYokeProfile(profile_params, mesh_params)
        self.yoke_profile.change_profile_to_axis('y')  # Permutation of the coordinates depending on the long. axis
        pole_profile = self.yoke_profile.pole_profile
        pole_sub = self.yoke_profile.pole_sub
        x_min = min([pole_profile[k][1] for k in range(len(pole_profile))]) # Minimum transverse position

        # --- Build the yoke
        # The generalized extrusion is not yet available with Python (it does not triangulate the base). To bypass
        # this issue, the yoke is defined as an assembly of rotated and cut parts
        str_options = 'ki->Numb,TriAngMin->'+str(mesh_params.tri_ang_min)+',TriAreaMax->' + str(mesh_params.area_max)
        self.obj = rad.ObjCnt([]) # Initialize an empty container
        dtheta = angle_bend / (n_segments - 1) # Various geometrical parameters...
        if end == 'rect':
            r_max = r_bend - x_min
        else:
            r_max = r_bend
        l_max = r_max * sin(dtheta) * 2 # The factor 2 is added to avoid cutting in an edge of the block
        for n in range(n_segments):
            theta =  n * dtheta
            # Extrusion
            yoke_n = rad.ObjMltExtTri(0, l_max, pole_profile, pole_sub, 'y', [0, 0, 0], str_options)
            # Cut the blocks
            # yoke_n = rad.ObjCutMag(yoke_n, [r_bend, 0, 0], [sin(- dtheta / 2), cos(- dtheta / 2), 0], 'Frame->Lab')[1]
            # yoke_n = rad.ObjCutMag(yoke_n, [r_bend, 0, 0], [sin(dtheta / 2), cos(dtheta / 2), 0], 'Frame->Lab')[0]
            yoke_n = rad.ObjCutMag(yoke_n, [r_bend * (1 - cos(dtheta / 2)), - r_bend * sin(dtheta / 2), 0],
                                   [sin(- dtheta / 2), cos(- dtheta / 2), 0], 'Frame->Lab')[1]
            yoke_n = rad.ObjCutMag(yoke_n, [r_bend * (1 - cos(dtheta / 2)), r_bend * sin(dtheta / 2), 0],
                                   [sin(dtheta / 2), cos(dtheta / 2), 0], 'Frame->Lab')[0]
            # Rotation
            rot = rad.TrfRot([r_bend, 0, 0], [0, 0, 1], -theta)
            rad.TrfOrnt(yoke_n, rot)
            # Add to the container
            rad.ObjAddToCnt(self.obj, [yoke_n])
        # Cut the extremities
        self.obj = rad.ObjCutMag(self.obj, [0, 0, 0], [0, 1, 0], 'Frame->Lab')[1]
        if end == 'rect':
            self.obj = rad.ObjCutMag(self.obj, [0, length / 2, 0], [0, 1, 0], 'Frame->Lab')[0]

        # --- Set the material
        mat = rad_mat.set_soft_mat(mat_name)
        rad.MatApl(self.obj, mat)

        # --- Color
        rad.ObjDrwAtr(self.obj, color)

        # --- Rotate if specified
        if axis == 'x':
            rot_z = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 2)
            rot_x = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rot_z)
            rad.TrfOrnt(self.obj, rot_x)
        if axis == 'z':
            rot = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rot)


# -------------------------------------------
# Class for dipole-quad yoke
# -------------------------------------------
class DipoleModulePMYoke(Yoke):
    def __init__(self, yoke_params):
        """
        Initialize an PM dipole module yoke and pole
        :param yoke_params: yoke parameters (see DipoleModulePMYokeParam)
        """
        self.yoke_params = yoke_params
        self.build_yoke()

    # -------------------------------------------
    def build_yoke(self):
        """
        Build a PM dipole module yoke
        """
        # --- Get the parameters
        try:
            yoke_length = self.yoke_params.yoke_length
            pole_length = self.yoke_params.pole_length
            profile_params = self.yoke_params.profile_params
            yoke_mesh_params = self.yoke_params.yoke_mesh_params
            pole_mesh_params = self.yoke_params.pole_mesh_params
            axis = self.yoke_params.axis
            color = self.yoke_params.color
            yoke_mat_name = self.yoke_params.yoke_mat
            pole_mat_name = self.yoke_params.pole_mat
            long_sub_pole = pole_mesh_params.long_sub
            long_sub_yoke = yoke_mesh_params.long_sub
        except AttributeError:
            raise Exception('Attribute error in DipoleModulePM.build_yoke()')

        # --- Pole profile and subdivision list
        self.yoke_profile = DipoleModulePMYokeProfile(profile_params)
        if self.yoke_profile.pole_profile is None:
            raise Exception('Attribute error in DipoleModulePM.build_yoke()')

        self.yoke_profile.change_profile_to_axis('y')  # Permutation of the coordinates depending on the long. axis
        pole_profile = self.yoke_profile.pole_profile
        pole_sub = self.yoke_profile.pole_sub
        backleg_profile = self.yoke_profile.backleg_profile
        backleg_sub = self.yoke_profile.backleg_sub

        # --- Build the backleg yoke
        str_options = 'ki->Numb,TriAngMin->'+str(yoke_mesh_params.tri_ang_min)+',TriAreaMax->' \
                      + str(yoke_mesh_params.area_max)
        self.backleg = rad.ObjMltExtTri(yoke_length/4, yoke_length/2, backleg_profile, backleg_sub, 'y', [0, 0, 0],
                                        str_options)

        # --- Build the pole
        str_options = 'ki->Numb,TriAngMin->' + str(pole_mesh_params.tri_ang_min) + ',TriAreaMax->' \
                      + str(pole_mesh_params.area_max)
        self.pole = rad.ObjMltExtTri(pole_length/4, pole_length/2, pole_profile, pole_sub, 'y', [0, 0, 0], str_options)

        # Subdivision
        rad.ObjDivMag(self.pole, [long_sub_pole[0], 1, 1])
        rad.ObjDivMag(self.backleg, [long_sub_yoke[0], 1, 1])

        # --- Set the material
        pole_mat = rad_mat.set_soft_mat(pole_mat_name)
        yoke_mat = rad_mat.set_soft_mat(yoke_mat_name)
        rad.MatApl(self.pole, pole_mat)
        rad.MatApl(self.backleg, yoke_mat)

        # --- Color
        rad.ObjDrwAtr(self.pole, color)
        rad.ObjDrwAtr(self.backleg, color)

        # --- Put the pole and yoke in a container
        self.obj = rad.ObjCnt([self.pole, self.backleg])

        # --- Rotate if specified
        if axis == 'x':
            rot_z = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 2)
            rot_x = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rot_z)
            rad.TrfOrnt(self.obj, rot_x)
        if axis == 'z':
            rot = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rot)


# -------------------------------------------
# Base class for coils
# -------------------------------------------
class Coil(MM):
    """Base class for coils"""


# -------------------------------------------
# Quadrupole coils
# -------------------------------------------
class QuadEMCoil(Coil):
    def __init__(self, coil_params, yoke_params=None):
        # --- Hold the coil parameters
        self.coil_params = coil_params
        self.yoke_params = yoke_params
        # --- Build the coil
        self.build_race_track()

    # -------------------------------------------
    def build_race_track(self):
        """
        Build a racetrack coil using the coil parameters defined in self.coil (see MMCoilParam)
        If yoke_params is defined, it is used for setting the length, inner radius (ie pole width) and position
        """

        self.sym = 'full' # This function builds the whole coil
        # --- Try to get the parameters
        try:
            # Coil parameters
            coil_segments = self.coil_params.coil_segments
            height = self.coil_params.height
            width = self.coil_params.width
            dr = self.coil_params.dr
            current_density = self.coil_params.current * self.coil_params.turns / (height * width)
            color = self.coil_params.color
            # Pole profile
            if self.yoke_params is not None:
                yoke_profile = QuadEMYokeProfile(self.yoke_params.profile_params, self.yoke_params.mesh_params)
                pole_profile = yoke_profile.pole_profile
                x0, z0 = pole_profile[-1]
                x1, z1 = pole_profile[-2]
                r0 = (x0*x0 + z0*z0) ** 0.5
                pole_width = 2 *((x0 - x1)*(x0 -x1) + (z0 - z1)*(z0 - z1)) ** 0.5
                self.coil_params.pole_width = pole_width
                # length
                length = self.yoke_params.length
                self.coil_params.pole_length = length
                # Axis
                axis = self.yoke_params.axis
                self.coil_params.axis = axis
                # Chamfer
                if self.yoke_params.chamfer is not None:
                    chamfer = self.yoke_params.chamfer[1]
                else:
                    chamfer = 0
            else:
                if self.coil_params.pole_width is not None and self.coil_params.pole_length is not None:
                    pole_width = self.coil_params.pole_width
                    length = self.coil_params.pole_length
                    chamfer = 0
                    r0 = 0
                    axis = self.coil_params.axis
                else:
                    raise Exception('Missing parameters in QuadEMCoil.build_race_track()')

        except AttributeError:
            raise Exception('Attribute error in QuadEMCoil.build_race_track()')

        # --- Build a racetrack coil
        pos = [r0 - dr - height / 2 , 0, 0]
        self.obj = rad.ObjRaceTrk(pos,
                              [dr + chamfer, dr + chamfer + width],
                              [length + 2 * (dr - chamfer), pole_width + 2 * (dr - chamfer) ],
                              height, coil_segments, current_density, 'm', 'x')
        # --- Rotate the coil to its final position
        r_x_90 = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
        r_y_45 = rad.TrfRot([0, 0, 0], [0, 1, 0], -pi / 4)
        r_z_90 = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 2)
        r_z_45 = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 4)
        if axis == 'x':
            rad.TrfOrnt(self.obj, r_y_45)
            rad.TrfOrnt(self.obj, r_z_90)
        elif axis == 'y':
            rad.TrfOrnt(self.obj, r_y_45)
        elif axis == 'z':
            rad.TrfOrnt(self.obj, r_x_90)
            rad.TrfOrnt(self.obj, r_z_45)
        # --- Color
        rad.ObjDrwAtr(self.obj, color)


# -------------------------------------------
# Sextupole coils
# -------------------------------------------
class SextuEMCoil(Coil):
    def __init__(self, coil_params, yoke_params=None):
        # --- Hold the coil parameters
        self.coil_params = coil_params
        self.yoke_params = yoke_params
        # --- Build the coil
        self.build_race_track()

    # -------------------------------------------
    def build_race_track(self):
        """
        Build a racetrack coil using the coil parameters defined in self.coil (see MMCoilParam)
        If yoke_params is defined, it is used for setting the length, inner radius (ie pole width) and position
        """

        self.sym = 'full' # This function builds the whole coil
        # --- Try to get the parameters
        try:
            # Coil parameters
            coil_segments = self.coil_params.coil_segments
            height = self.coil_params.height
            width = self.coil_params.width
            dr = self.coil_params.dr
            current_density = self.coil_params.current * self.coil_params.turns / (height * width)
            color = self.coil_params.color
            # Pole profile
            if self.yoke_params is not None:
                yoke_profile = SextuEMYokeProfile(self.yoke_params.profile_params, self.yoke_params.mesh_params)
                pole_profile = yoke_profile.pole_profile
                x0, z0 = pole_profile[-1]
                x1, z1 = pole_profile[-2]
                r0 = (x0*x0 + z0*z0) ** 0.5
                pole_width = 2 *((x0 - x1)*(x0 -x1) + (z0 - z1)*(z0 - z1)) ** 0.5
                self.coil_params.pole_width = pole_width
                # length
                length = self.yoke_params.length
                self.coil_params.pole_length = length
                # Axis
                axis = self.yoke_params.axis
                self.coil_params.axis = axis
                # Chamfer
                if self.yoke_params.chamfer is not None:
                    chamfer = self.yoke_params.chamfer[1]
                else:
                    chamfer = 0
            else:
                if self.coil_params.pole_width is not None and self.coil_params.pole_length is not None:
                    pole_width = self.coil_params.pole_width
                    length = self.coil_params.pole_length
                    chamfer = 0
                    r0 = 0
                    axis = self.coil_params.axis
                else:
                    raise Exception('Missing parameters in SextuEMCoil.build_race_track()')

        except AttributeError:
            raise Exception('Attribute error in SextuEMCoil.build_race_track()')

        # --- Build a racetrack coil
        pos = [r0 - dr - height / 2 , 0, 0]
        self.obj = rad.ObjRaceTrk(pos,
                              [dr + chamfer, dr + chamfer + width],
                              [length + 2 * (dr - chamfer), pole_width + 2 * (dr - chamfer) ],
                              height, coil_segments, current_density, 'm', 'x')
        # --- Rotate the coil to its final position
        r_x_90 = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
        r_y_30 = rad.TrfRot([0, 0, 0], [0, 1, 0], -pi / 6)
        r_z_90 = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 2)
        r_z_30 = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 6)
        if axis == 'x':
            rad.TrfOrnt(self.obj, r_y_30)
            rad.TrfOrnt(self.obj, r_z_90)
        elif axis == 'y':
            rad.TrfOrnt(self.obj, r_y_30)
        elif axis == 'z':
            rad.TrfOrnt(self.obj, r_x_90)
            rad.TrfOrnt(self.obj, r_z_30)
        # --- Color
        rad.ObjDrwAtr(self.obj, color)


# -------------------------------------------
# Octupole coils
# -------------------------------------------
class OctuEMCoil(Coil):
    def __init__(self, coil_params, yoke_params=None):
        """Initialize an octupole coil
        :param coil_params: coil parameters
        :param yoke_params=None: yoke parameters (needed to determine coil width, position, etc.)
        """
        # --- Hold the coil parameters
        self.coil_params = coil_params
        self.yoke_params = yoke_params
        # --- Build the coil
        self.build_race_track()

    # -------------------------------------------
    def build_race_track(self):
        """
        Build a racetrack coil using the coil parameters defined in self.coil (see MMCoilParam)
        If yoke_params is defined, it is used for setting the length, inner radius (ie pole width) and position
        """

        self.sym = 'full' # This function builds the whole coil
        # --- Try to get the parameters
        try:
            # Coil parameters
            coil_segments = self.coil_params.coil_segments
            height = self.coil_params.height
            width = self.coil_params.width
            dr = self.coil_params.dr
            current_density = self.coil_params.current * self.coil_params.turns / (height * width)
            color = self.coil_params.color
            # Pole profile
            if self.yoke_params is not None:
                yoke_profile = OctuEMYokeProfile(self.yoke_params.profile_params, self.yoke_params.mesh_params)
                profile = yoke_profile.backleg_profile
                x0, z0 = profile[1]
                x1, z1 = profile[2]
                r0 = ((x0 / 2 + x1 / 2) ** 2 + (z0 / 2 + z1 / 2) ** 2) ** 0.5
                pole_width = self.yoke_params.yoke_length
                pole_length = ((x1 - x0) ** 2 + (z1 - z0) ** 2 ) ** 0.5
                self.coil_params.pole_width = pole_width
                # Axis
                axis = self.yoke_params.axis
                self.coil_params.axis = axis
                # Chamfer
                if self.yoke_params.chamfer is not None:
                    chamfer = self.yoke_params.chamfer[1]
                else:
                    chamfer = 0
            else:
                raise Exception('Missing parameters in QuadEMCoil.build_race_track()')

        except AttributeError:
            raise Exception('Attribute error in QuadEMCoil.build_race_track()')
            
        # --- Build a racetrack coil
        pos = [r0, 0, 0]
        self.obj = rad.ObjRaceTrk(pos,
                              [dr + chamfer, dr + chamfer + width],
                              [pole_length + 2 * (dr - chamfer), pole_width + 2 * (dr - chamfer)],
                              height, coil_segments, current_density, 'm', 'z')
        # --- Rotate the coil to its final position
        r_x_90 = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
        r_y_45 = rad.TrfRot([0, 0, 0], [0, 1, 0], -pi / 4)
        r_z_90 = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 2)
        r_z_45 = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 4)
        if axis == 'x':
            rad.TrfOrnt(self.obj, r_y_45)
            rad.TrfOrnt(self.obj, r_z_90)
        elif axis == 'y':
            rad.TrfOrnt(self.obj, r_y_45)
        elif axis == 'z':
            rad.TrfOrnt(self.obj, r_x_90)
            rad.TrfOrnt(self.obj, r_z_45)
        # --- Color
        rad.ObjDrwAtr(self.obj, color)


# -------------------------------------------
# Dipole-quadrupole main coils
# -------------------------------------------
class DipoleQuadEMMainCoil(Coil):
    def __init__(self, coil_params, yoke_params=None):
        """
        Build a DQ main coil
        :param coil_params: coil parameters
        :param yoke_params=None: yoke parameters (needed to position the coil)
        """
        # Hold the parameters
        self.coil_params = coil_params
        self.yoke_params = yoke_params
        # Build the coil
        self.build_race_track()

    # -------------------------------------------
    def build_race_track(self):
        self.sym = 'full'  # This function builds the whole coil
        # --- Try to get the parameters
        try:
            # Coil parameters
            coil_segments = self.coil_params.coil_segments
            height = self.coil_params.height
            width = self.coil_params.width
            dr = self.coil_params.dr
            current_density = self.coil_params.current * self.coil_params.turns / (height * width)
            color = self.coil_params.color
            yoke_points = self.coil_params.yoke_points
            # Pole profile
            if self.yoke_params is not None and yoke_points is not None:
                # Sagitta
                yoke_length = self.yoke_params.length
                r_bend = self.yoke_params.r_bend
                if r_bend is not None:
                    angle_bend = asin(yoke_length / (2 * r_bend))
                    sag = r_bend * (1 - cos(angle_bend / 2))
                else:
                    sag = 0
                yoke_profile = DipoleQuadEMYokeProfile(self.yoke_params.profile_params, self.yoke_params.mesh_params)
                # Get points on the pole profile (needed to position the coil)
                x00, z00 = yoke_profile.pole_profile[yoke_points[0]]
                x10, z10 = yoke_profile.pole_profile[yoke_points[1]]
                x0, z0 = (x00 + x10) / 2, (z00 + z10) / 2
                r0 = z0 * 2 ** 0.5
                dx0 = x0 - z0
                pole_width = ((x00 - x10) * (x00 - x10) + (z00 - z10) * (z00 - z10)) ** 0.5 + sag
                self.coil_params.pole_width = pole_width
                # length
                length = self.yoke_params.length
                self.coil_params.pole_length = length
                # Axis
                axis = self.yoke_params.axis
                self.coil_params.axis = axis
            else:
                if self.coil_params.pole_width is not None and self.coil_params.pole_length is not None:
                    pole_width = self.coil_params.pole_width
                    length = self.coil_params.pole_length
                    r0 = 0
                    axis = self.coil_params.axis
                else:
                    raise Exception('Missing parameter in DipoleQuadEMMainCoil.build_race_track()')
                    
        except AttributeError:
            raise Exception('Attribute error in DipoleQuadEMMainCoil.build_race_track()')
           
        # --- Build a racetrack coil
        pos = [r0 + dx0 - dr - height / 2, 0, 0]
        self.obj = rad.ObjRaceTrk(pos, [dr, dr + width],  [length + 2 * dr, pole_width + 2 * dr], height, coil_segments,
                                  current_density, 'm', 'x')
        # --- Rotate the coil to its final position
        if axis == 'x':
            r_z_90 = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 2)
            r_x_90 = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            r_x_45 = rad.TrfRot([0, 0, dx0], [1, 0, 0], pi / 4)
            rad.TrfOrnt(self.obj, r_z_90)
            rad.TrfOrnt(self.obj, r_x_90)
            rad.TrfOrnt(self.obj, r_x_45)
        elif axis == 'y':
            r_y_45 = rad.TrfRot([dx0, 0, 0], [0, 1, 0], -pi / 4)
            rad.TrfOrnt(self.obj, r_y_45)
        elif axis == 'z':
            r_x_90 = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            r_z_45 = rad.TrfRot([dx0, 0, 0], [0, 0, 1], -pi / 4)
            rad.TrfOrnt(self.obj, r_x_90)
            rad.TrfOrnt(self.obj, r_z_45)
        # --- Color
        rad.ObjDrwAtr(self.obj, color)


# -------------------------------------------
# Dipole-quadrupole auxiliary coils
# -------------------------------------------
class DipoleQuadEMAuxCoil(Coil):
    def __init__(self, coil_params, yoke_params=None, corr_coil_params=None):
        """
                Build a DQ auxiliary coil
                :param coil_params: coil parameters
                :param yoke_params=None: yoke parameters (needed to position the coil)
                :param corr_coil_params=None: correction coil parameters (for vertical position of the coil)
                """
        # Hold the parameters
        self.coil_params = coil_params
        self.yoke_params = yoke_params
        self.corr_coil_params = corr_coil_params
        # Build the coil
        self.build_race_track()

    # -------------------------------------------
    def build_race_track(self):
        self.sym = 'full'  # This function builds the whole coil
        # --- Try to get the parameters
        try:
            # Coil parameters
            coil_segments = self.coil_params.coil_segments
            height = self.coil_params.height
            width = self.coil_params.width
            dr = self.coil_params.dr
            current_sign = self.coil_params.current_sign
            current_density = self.coil_params.current * self.coil_params.turns / (height * width)  * current_sign
            color = self.coil_params.color
            yoke_points = self.coil_params.yoke_points
            # Pole profile
            if self.yoke_params is not None and yoke_points is not None:
                # Sagitta
                yoke_length = self.yoke_params.length
                r_bend = self.yoke_params.r_bend
                if r_bend is not None:
                    angle_bend = asin(yoke_length / (2 * r_bend))
                    sag = r_bend * (1 - cos(angle_bend / 2))
                else:
                    sag = 0
                yoke_profile = DipoleQuadEMYokeProfile(self.yoke_params.profile_params,
                                                       self.yoke_params.mesh_params)
                # Get points on the pole profile (needed to position the coil)
                x00, z00 = yoke_profile.pole_profile[yoke_points[0]]
                x10, z10 = yoke_profile.pole_profile[yoke_points[1]]
                x0, z0 = (x00 + x10) / 2, z10
                pole_width = abs(x10 - x00) + sag
                self.coil_params.pole_width = pole_width
                # length
                length = self.yoke_params.length
                self.coil_params.pole_length = length
                # Axis
                axis = self.yoke_params.axis
                self.coil_params.axis = axis
            else:
                if self.coil_params.pole_width is not None and self.coil_params.pole_length is not None:
                    pole_width = self.coil_params.pole_width
                    length = self.coil_params.pole_length
                    axis = self.coil_params.axis
                    x0, z0 = 0, 0
                else:
                    raise Exception('Missing parameter in DipoleQuadEMAuxCoil.build_race_track()')
               
            # Correction coil
            if self.corr_coil_params is not None:
                h_corr = self.corr_coil_params.height + self.corr_coil_params.dr
            else:
                h_corr = 0
        except AttributeError:
            raise Exception('Attribute error in DipoleQuadEMAuxCoil.build_race_track()')
            
        # --- Build a racetrack coil
        pos = [x0, 0, z0 - dr - height / 2 - h_corr]
        self.obj = rad.ObjRaceTrk(pos, [dr, dr + width], [pole_width + 2 * dr, length + 2 * dr], height,
                                  coil_segments, - current_density, 'm', 'z')
        # --- Rotate the coil to its final position
        if axis == 'x':
            r_x_90 = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            r_y_90 = rad.TrfRot([0, 0, 0], [0, 1, 0], -pi / 2)
            rad.TrfOrnt(self.obj, r_x_90)
            rad.TrfOrnt(self.obj, r_y_90)
        elif axis == 'z':
            pass
            r_x_90 = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, r_x_90)
        # --- Color
        rad.ObjDrwAtr(self.obj, color)


# -------------------------------------------
# PM blocks for dipole modules
# -------------------------------------------
class DipoleModulePMBlocks(MM):
    def __init__(self, pm_params=None, yoke_params=None):
        """
        :param pm_params=None: PM parameters
        :param yoke_params=None: yoke and pole parameters
        """
        # Hold the parameters
        self.pm_params = pm_params
        self.yoke_params = yoke_params

    def build_top_pm(self):
        """
        Build top magnets for DL modules
        """
        # Create an empty container for the blocks
        self.obj = rad.ObjCnt([])
        # Extract the parameters
        try:
            # Geometric parameters
            w = self.pm_params.top_width
            h = self.pm_params.top_height
            pm_len = self.pm_params.top_full_length
            yoke_len = self.yoke_params.yoke_length
            n = self.pm_params.top_n_blocks
            # Material
            mat_name = self.pm_params.mat
            temp = self.pm_params.temp
            temp_coef = self.pm_params.temp_coef
            br = self.pm_params.br * (1 + temp_coef * (temp - 20))
            # Other parameters
            sub = self.pm_params.sub.sub
            color = self.pm_params.color
            if self.yoke_params.profile_params is not None:
                profile_params = self.yoke_params.profile_params
            else:
                profile_params = DipoleModulePMYokeProfileParam()
            if self.yoke_params is not None:
                self.yoke_profile = DipoleModulePMYokeProfile(profile_params)
                self.yoke_profile.change_profile_to_axis('y')  # Permutation of the coord. depending on the long. axis
                pole_profile = self.yoke_profile.pole_profile
                z0 = max(pole_profile)[0]
            else:
                z0 = 0
            if self.yoke_params is not None:
                axis = self.yoke_params.axis
            else:
                axis = 'y'
        except AttributeError:
            raise Exception('Attribute error in DipoleModulePMBlocks.build_top_pm()')

        # Create the blocks
        len = pm_len / (2 * n)
        d_pos = (yoke_len - pm_len) / (2 * n)
        for k in range(n):
            # Longitudinal position
            pos = (k + 0.5) * (d_pos + len)
            # Block
            pm = rad.ObjRecMag([0, pos, z0 + h / 2], [w, len, h], [0, 0, -1])
            rad.ObjDivMag(pm, sub)
            # Add to container
            rad.ObjAddToCnt(self.obj, [pm])

        # --- Set the material
        mat = rad_mat.set_pm_mat(mat_name, br=br)
        rad.MatApl(self.obj, mat)

        # --- Color
        rad.ObjDrwAtr(self.obj, color)

        # --- Rotate if specified
        if axis == 'x':
            rot_z = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 2)
            rot_x = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rot_z)
            rad.TrfOrnt(self.obj, rot_x)
        if axis == 'z':
            rot = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rot)

    def build_side_pm(self, io='in'):
        """
        Build inner side magnets for DL modules
        :param io='in': build inner ('in') or outer ('out') block
        """
        # Create an empty container for the blocks
        self.obj = rad.ObjCnt([])
        # Extract the parameters
        try:
            # Geometric parameters
            w = self.pm_params.side_width
            h = self.pm_params.side_height
            pm_len = self.pm_params.side_full_length
            yoke_len = self.yoke_params.yoke_length
            n = self.pm_params.side_n_blocks
            yoke_point = self.pm_params.yoke_points
            if n is None or w is None or h is None or pm_len is None or pm_len <= 0:
                return
            # Material
            mat = self.pm_params.mat
            temp = self.pm_params.temp
            temp_coef = self.pm_params.temp_coef
            br = self.pm_params.br * (1 + temp_coef * (temp - 20))
            # Other parameters
            sub = self.pm_params.sub.sub
            color = self.pm_params.color
            if self.yoke_params.profile_params is not None:
                profile_params = self.yoke_params.profile_params
            else:
                profile_params = DipoleModulePMYokeProfileParam()
            x0, z0 = 0, 0
            if self.yoke_params is not None:
                self.yoke_profile = DipoleModulePMYokeProfile(profile_params)
                self.yoke_profile.change_profile_to_axis('y')  # Permutation of the coord. depending on the long. axis
                pole_profile = self.yoke_profile.pole_profile
                if io == 'in':
                    z0, x0 = pole_profile[yoke_point[0]]
                elif io == 'out':
                    z0, x0 = pole_profile[yoke_point[1]]
            if self.yoke_params is not None:
                axis = self.yoke_params.axis
            else:
                axis = 'y'
        except AttributeError:
            raise Exception('Attribute error in DipoleModulePMBlocks.build_side_pm()')

        # Create the blocks
        len = pm_len / (2 * n)
        x_pos = 0
        y_pos = (yoke_len - pm_len) / (2 * n)
        z_pos = z0 - h / 2
        sgn = 1
        if io == 'in':
            x_pos = x0 - w / 2
        elif io == 'out':
            x_pos = x0 + w / 2
            sgn = -1
        for k in range(n):
            # Longitudinal position
            pos = (k + 0.5) * (y_pos + len)
            # Block
            pm = rad.ObjRecMag([x_pos, pos, z_pos], [w, len, h], [sgn, 0, 0])
            rad.ObjDivMag(pm, sub)
            # Add to container
            rad.ObjAddToCnt(self.obj, [pm])

        # --- Set the material
        mat = rad_mat.set_pm_mat(mat, br=br)
        rad.MatApl(self.obj, mat)

        # --- Color
        rad.ObjDrwAtr(self.obj, color)

        # --- Rotate if specified
        if axis == 'x':
            rot_z = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 2)
            rot_x = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rot_z)
            rad.TrfOrnt(self.obj, rot_x)
        if axis == 'z':
            rot = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rot)


# -------------------------------------------
# PM blocks for dipole modules
# -------------------------------------------
class DipoleModulePMShims(MM):
    def __init__(self, shim_params=None, pm_params=None, yoke_params=None):
        """
        :param pm_params=None: PM parameters
        :param yoke_params=None: yoke and pole parameters
        """
        # Hold the parameters
        self.shim_params = shim_params
        self.pm_params = pm_params
        self.yoke_params = yoke_params

    def build_shim(self, io='in'):
        """
        Build inner side magnets for DL modules
        :param io='in': build inner ('in') or outer ('out') shims
        """
        # Create an empty container for the blocks
        self.obj = rad.ObjCnt([])
        # Extract the parameters
        if self.shim_params is None or self.pm_params is None or self.yoke_params is None:
            return
        try:
            # Geometric parameters
            pm_w = self.pm_params.side_width
            pm_h = self.pm_params.side_height
            yoke_len = self.yoke_params.yoke_length
            yoke_point = self.pm_params.yoke_points
            th = self.shim_params.thickness
            if pm_w is None or yoke_len <= 0:
                return
            if pm_h is None:
                pm_h = 0
            # Material
            mat_params = self.shim_params.mat
            temp = self.shim_params.temp
            temp_coef = self.shim_params.temp_coef
            # Other parameters
            sub = self.shim_params.sub.sub
            color = self.shim_params.color
            if self.yoke_params.profile_params is not None:
                profile_params = self.yoke_params.profile_params
            else:
                profile_params = DipoleModulePMYokeProfileParam()
            x0, z0 = 0, 0
            if self.yoke_params is not None:
                self.yoke_profile = DipoleModulePMYokeProfile(profile_params)
                self.yoke_profile.change_profile_to_axis('y')  # Permutation of the coord. depending on the long. axis
                pole_profile = self.yoke_profile.pole_profile
                if io == 'in':
                    z0, x0 = pole_profile[yoke_point[0]]
                elif io == 'out':
                    z0, x0 = pole_profile[yoke_point[1]]
            if self.yoke_params is not None:
                axis = self.yoke_params.axis
            else:
                axis = 'y'
        except AttributeError:
            raise Exception('Attribute error in DipoleModulePMShims.build_shim()')

        # Create the shims
        x_pos = 0
        y_pos = yoke_len / 4
        z_pos = z0 - pm_h - th / 2
        if io == 'in':
            x_pos = x0 - pm_w / 2
        elif io == 'out':
            x_pos = x0 + pm_w / 2
        self.obj = rad.ObjRecMag([x_pos, y_pos, z_pos], [pm_w, yoke_len / 2, th], [0, 0, 0])
        rad.ObjDivMag(self.obj, sub)

        # --- Set the material
        ksi = mat_params[1]
        ksi_br = [0, 0, 0]
        for k in range(3):
            ksi_br[k] = [ksi[k], mat_params[0][k] * (1 + temp_coef * (temp - 20))]
        mat = rad.MatSatIsoFrm(ksi_br[0], ksi_br[1], ksi_br[2])
        rad.MatApl(self.obj, mat)

        # --- Color
        rad.ObjDrwAtr(self.obj, color)

        # --- Rotate if specified
        if axis == 'x':
            rot_z = rad.TrfRot([0, 0, 0], [0, 0, 1], pi / 2)
            rot_x = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rot_z)
            rad.TrfOrnt(self.obj, rot_x)
        if axis == 'z':
            rot = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rot)


# -------------------------------------------
# Electromagnet quadrupole
# -------------------------------------------
class QuadEM(MM):
    def __init__(self, params, current=None, solve_switch=True, print_switch=True):
        """
        Initialize a quadrupole object
        :param params: quadrupole parameters, see QuadEMParam
        :param current=None: current (A), set to default value if None
        :param solve_switch=True: solve the magnetization problem if True
        :param print_switch=True: quiet mode if false
        """

        # --- Parameters
        self.params = params
        yoke_params = self.params.yoke_params
        if current is not None:
            self.params.coil_params.current = current

        # --- Build the magnet
        self.build()

        # --- Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def build(self):
        """
        Build the quadrupole
        :return: None
        """

        # --- build the yoke
        self.yoke = QuadEMYoke(self.params.yoke_params)

        # --- Build the coil
        self.coil = QuadEMCoil(self.params.coil_params, self.params.yoke_params)

        # --- Symmetries
        # Yoke
        axis = self.params.yoke_params.axis
        if axis == 'x':
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [0, -1, 1])  # Pole symmetry plane
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [1, 0, 0])  # longitudinal axis
        elif axis == 'y':
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [-1, 0, 1])  # Pole symmetry plane
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [0, 1, 0])  # longitudinal axis
        elif axis == 'z':
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [-1, 1, 0])  # Pole symmetry plane
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [0, 0, 1])  # longitudinal axis
        # Coil
        if self.coil.sym != 'full':
            if axis == 'x':
                rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [0, -1, 1])  # Pole symmetry plane
                rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [1, 0, 0])  # longitudinal axis
            elif axis == 'y':
                rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [-1, 0, 1])  # Pole symmetry plane
                rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [0, 1, 0])  # longitudinal axis
            elif axis == 'z':
                rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [-1, 1, 0])  # Pole symmetry plane
                rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [0, 0, 1])  # longitudinal axis

        # --- Assembly
        self.obj = rad.ObjCnt([self.yoke.obj, self.coil.obj])
        if axis == 'x':
            rad.TrfZerPara(self.obj, [0, 0, 0], [0, 1, 0])
            rad.TrfZerPara(self.obj, [0, 0, 0], [0, 0, 1])
        elif axis == 'y':
            rad.TrfZerPara(self.obj, [0, 0, 0], [1, 0, 0])
            rad.TrfZerPara(self.obj, [0, 0, 0], [0, 0, 1])
        elif axis == 'z':
            rad.TrfZerPara(self.obj, [0, 0, 0], [1, 0, 0])
            rad.TrfZerPara(self.obj, [0, 0, 0], [0, 1, 0])

    def delete(self, delete_params=True):
        """
        Clear the MM object, erase the Radia objects
        """
        self.result = None
        self.time_end = None
        self.time_start = None
        if delete_params:
            self.params = None
        try:
            rad.UtiDel(self.yoke.obj)
            self.yoke = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.coil.obj)
            self.coil = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.obj)
            self.obj = None
        except AttributeError:
            pass

    def set_current(self, current, solve_switch=True, print_switch=True):
        """
        Change the current and solve
        :param current: new current
        :param solve_switch: solve if True
        :param print_switch: monitor if True
        """
        try:
            # Read previous current
            current_old = self.params.coil_params.current
            # Hold the new current
            self.params.coil_params.current = current
            # Current ratio
            current_ratio = current / current_old
            # Scale the current
            rad.ObjScaleCur(self.obj, current_ratio)
            # Solve
            if solve_switch:
                self.solve(print_switch)
        except AttributeError:
            raise Exception('error in QuadEM.set_current()')

# -------------------------------------------
# Electromagnet sextupole
# Correction coils to be implemented...
# -------------------------------------------
class SextuEM(MM):
    def __init__(self, params, current=None, solve_switch=True, print_switch=True):
        """
        Initialize a quadrupole object
        :param params: quadrupole parameters, see QuadEMParam
        :param current=None: current (A), set to default value if None
        :param solve_switch=True: solve the magnetization problem if True
        :param print_switch=True: quiet mode if false
        """

        # --- Parameters
        self.params = params
        yoke_params = self.params.yoke_params
        if current is not None:
            self.params.coil_params.current = current
        coil_params = self.params.coil_params

       # --- Build
        self.build()

        # --- Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def build(self):
        """
        Build the sextupole
        :return: None
        """
        # --- build the yoke
        self.yoke = SextuEMYoke(self.params.yoke_params)

        # --- Build the coil
        self.coil = SextuEMCoil(self.params.coil_params, self.params.yoke_params)

        # --- Symmetries
        # Yoke
        axis = self.params.yoke_params.axis
        if axis == 'x':
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [0, -1, 3 ** 0.5])  # Pole symmetry plane
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [1, 0, 0])  # longitudinal axis
        elif axis == 'y':
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [-1, 0, 3 ** 0.5])  # Pole symmetry plane
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [0, 1, 0])  # longitudinal axis
        elif axis == 'z':
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [-1, 3 ** 0.5, 0])  # Pole symmetry plane
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [0, 0, 1])  # longitudinal axis
        # Coil
        # if self.coil.sym != 'full':
        #     if axis == 'x':
        #         rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [0, -1, 1]) # Pole symmetry plane
        #         rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [1, 0, 0]) # longitudinal axis
        #     elif axis == 'y':
        #         rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [-1, 0, 1]) # Pole symmetry plane
        #         rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [0, 1, 0])  # longitudinal axis
        #     elif axis == 'z':
        #         rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [-1, 1, 0]) # Pole symmetry plane
        #         rad.TrfZerPerp(self.coil.obj, [0, 0, 0], [0, 0, 1])  # longitudinal axis

        # --- Assembly
        self.obj = rad.ObjCnt([self.yoke.obj, self.coil.obj])
        if axis == 'x':
            v = [1, 0, 0]
        elif axis == 'y':
            v = [0, 1, 0]
        else:
            v = [0, 0, 1]
        rot_60 = rad.TrfRot([0, 0, 0], v, pi / 3)
        inv = rad.TrfInv()
        trf = rad.TrfCmbL(rot_60, inv)
        rad.TrfMlt(self.obj, trf, 6)

    def delete(self, delete_params=True):
        """
        Clear the MM object, erase the Radia objects
        """
        self.result = None
        self.time_end = None
        self.time_start = None
        if delete_params:
            self.params = None
        try:
            rad.UtiDel(self.yoke.obj)
            self.yoke = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.coil.obj)
            self.coil = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.obj)
            self.obj = None
        except AttributeError:
            pass

    def set_current(self, current, solve_switch=True, print_switch=True):
        """
        Change the current and solve
        :param current: new current
        :param solve_switch: solve if True
        :param print_switch: monitor if True
        """
        try:
            # Read previous current
            current_old = self.params.coil_params.current
            # Hold the new current
            self.params.coil_params.current = current
            # Current ratio
            current_ratio = current / current_old
            # Scale the current
            rad.ObjScaleCur(self.obj, current_ratio)
            # Solve
            if solve_switch:
                self.solve(print_switch)
        except AttributeError:
            raise Exception('error in SextuEM.set_current()')

# -------------------------------------------
# Electromagnet octupole
# -------------------------------------------
class OctuEM(MM):
    def __init__(self, params, current=None, solve_switch=True, print_switch=True):
        """
        Initialize a octupole object
        :param params: octupole parameters, see OctuEMParam
        :param current=None: current (A), set to default value if None
        :param solve_switch=True: solve the magnetization problem if True
        :param print_switch=True: quiet mode if false
        """

        # --- Parameters
        self.params = params
        yoke_params = self.params.yoke_params
        if current is not None:
            self.params.coil_params.current = current
        coil_params = self.params.coil_params

        # --- Build
        self.build()

        # --- Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def build(self):
        """
        Build the magnet
        :return: None
        """
        # --- build the yoke
        self.yoke = OctuEMYoke(self.params.yoke_params)

        # --- Build the coil
        self.coil = OctuEMCoil(self.params.coil_params, self.params.yoke_params)

        # --- Symmetries
        # Yoke
        axis = self.params.yoke_params.axis
        if axis == 'x':
           rad.TrfZerPara(self.yoke.obj, [0, 0, 0], [0, -1, 1])  # Pole symmetry plane
           rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [1, 0, 0])  # longitudinal axis
        elif axis == 'y':
           rad.TrfZerPara(self.yoke.obj, [0, 0, 0], [-1, 0, 1])  # Pole symmetry plane
           rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [0, 1, 0])  # longitudinal axis
        elif axis == 'z':
           rad.TrfZerPara(self.yoke.obj, [0, 0, 0], [-1, 1, 0])  # Pole symmetry plane
           rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [0, 0, 1])  # longitudinal axis

        # --- Assembly
        self.obj = rad.ObjCnt([self.yoke.obj, self.coil.obj])
        if axis == 'x':
           rad.TrfZerPara(self.obj, [0, 0, 0], [0, 1, 0])
           rad.TrfZerPara(self.obj, [0, 0, 0], [0, 0, 1])
        elif axis == 'y':
           rad.TrfZerPara(self.obj, [0, 0, 0], [1, 0, 0])
           rad.TrfZerPara(self.obj, [0, 0, 0], [0, 0, 1])
        elif axis == 'z':
           rad.TrfZerPara(self.obj, [0, 0, 0], [1, 0, 0])
           rad.TrfZerPara(self.obj, [0, 0, 0], [0, 1, 0])

    def delete(self, delete_params=True):
        """
        Clear the MM object, erase the Radia objects
        """
        self.result = None
        self.time_end = None
        self.time_start = None
        if delete_params:
            self.params = None
        try:
            rad.UtiDel(self.yoke.obj)
            self.yoke = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.coil.obj)
            self.coil = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.obj)
            self.obj = None
        except AttributeError:
            pass

    def set_current(self, current, solve_switch=True, print_switch=True):
        """
        Change the current and solve
        :param current: new current
        :param solve_switch: solve if True
        :param print_switch: monitor if True
        """
        try:
            # Read previous current
            current_old = self.params.coil_params.current
            # Hold the new current
            self.params.coil_params.current = current
            # Current ratio
            current_ratio = current / current_old
            # Scale the current
            rad.ObjScaleCur(self.obj, current_ratio)
            # Solve
            if solve_switch:
                self.solve(print_switch)
        except AttributeError:
            raise Exception('error in QuadEM.set_current()')

# -------------------------------------------
# Electromagnet quadrupole-quadrupole
# -------------------------------------------
class DipoleQuadEM(MM):
    def __init__(self, params, current=None, current_corr=None, solve_switch=True, print_switch=True):
        """
        Initialize an electromagnet dipole-quadrupole (DQ)
        :param params: DQ parameters
        :param current=None: magnet current (A) (default if None)
        :param current_corr=None: magnet corrector current (a) (default if None)
        :param solve_switch=True: solve if True
        :param print_swith=True: Verbose on if True
        """

        # --- Set the currents if specified
        if current is not None:
            params.main_coil_params.current = current
            params.aux_coil_params.current = current
        if current_corr is not None:
            params.corr_coil_params.current = current_corr

        # --- Parameters
        self.params = params

        # --- Build
        self.build()

        # --- Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def build(self):
        """
        Build the magnet
        :return: None
        """

        # --- build the yoke
        self.yoke = DipoleQuadEMYoke(self.params.yoke_params)

        # --- Build the main coil
        self.main_coil = DipoleQuadEMMainCoil(self.params.main_coil_params, self.params.yoke_params)

        # --- Build the auxiliary coil
        self.aux_coil = DipoleQuadEMAuxCoil(self.params.aux_coil_params, self.params.yoke_params,
                                            self.params.corr_coil_params)

        # --- Build the correction coil
        self.corr_coil = DipoleQuadEMAuxCoil(self.params.corr_coil_params, self.params.yoke_params)

        # --- Symmetries
        # Yoke
        axis = self.params.yoke_params.axis
        if axis == 'x':
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [1, 0, 0])  # longitudinal axis
        elif axis == 'y':
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [0, 1, 0])  # longitudinal axis
        elif axis == 'z':
            rad.TrfZerPerp(self.yoke.obj, [0, 0, 0], [0, 0, 1])  # longitudinal axis

        # --- Assembly
        self.obj = rad.ObjCnt([self.yoke.obj, self.main_coil.obj, self.aux_coil.obj, self.corr_coil.obj])
        if axis == 'x':
            rad.TrfZerPara(self.obj, [0, 0, 0], [0, 1, 0])  # Vertical axis
        elif axis == 'y':
            rad.TrfZerPara(self.obj, [0, 0, 0], [0, 0, 1])  # Vertical axis
        elif axis == 'z':
            rad.TrfZerPara(self.obj, [0, 0, 0], [0, 1, 0])  # vertical axis

    def delete(self, delete_params=True):
        """
        Clear the MM object, erase the Radia objects
        """
        self.result = None
        self.time_end = None
        self.time_start = None
        if delete_params:
            self.params = None
        try:
            rad.UtiDel(self.yoke.obj)
            self.yoke = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.main_coil.obj)
            self.main_coil = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.aux_coil.obj)
            self.aux_coil = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.corr_coil.obj)
            self.corr_coil = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.obj)
            self.obj = None
        except AttributeError:
            pass

    def set_current(self, current, solve_switch=True, print_switch=True):
        """
        Change the main current
        :param current: current in the main and aux coils (A)
        :param solve_switch=True: solve is True
        :param print_switch=True: monitor if true
        """
        # Scaling coefficient
        k = current / self.main_coil.coil_params.current
        # Change the coil parameters
        self.main_coil.coil_params.current = current
        self.aux_coil.coil_params.current = current
        # Change the current
        rad.ObjScaleCur(self.main_coil.obj, k)
        rad.ObjScaleCur(self.aux_coil.obj, k)
        # --- Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def set_current_corr(self, current_corr, solve_switch=True, print_switch=True):
        """
        Change the main current
        :param i_corr: current in the correction coils (A)
        :param solve_switch=True: solve is True
        :param print_switch=True: monitor if true
        """
        # Scaling coefficient
        k = current_corr / self.corr_coil.coil_params.current
        # Change the coil parameters
        self.corr_coil.coil_params.current = current_corr
        # Change the current
        rad.ObjScaleCur(self.corr_coil.obj, k)
        # --- Solve
        if solve_switch:
            self.solve(print_switch=print_switch)


# -------------------------------------------
# PM dipole module
# -------------------------------------------
class DipoleModulePM(MM):
    def __init__(self, params, solve_switch=True, print_switch=True):
        # --- Parameters
        self.params = params

        # --- Build
        self.build()

        # --- Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def build(self):
        """
        Build the magnet
        :return: None
        """

        yoke_params = self.params.yoke_params
        pm_params = self.params.pm_params
        shim_params = self.params.shim_params
        # --- build the yoke
        self.yoke = DipoleModulePMYoke(yoke_params)

        # --- Build the upper magnet assembly
        self.pm_top = DipoleModulePMBlocks(pm_params, yoke_params)
        self.pm_top.build_top_pm()

        # --- Build the inner magnet assembly
        self.pm_in = DipoleModulePMBlocks(pm_params, yoke_params)
        self.pm_in.build_side_pm('in')

        # --- Build the outer magnet assembly
        self.pm_out = DipoleModulePMBlocks(pm_params, yoke_params)
        self.pm_out.build_side_pm('out')

        # --- Build the inner shims
        self.shim_in = DipoleModulePMShims(shim_params, pm_params, yoke_params)
        self.shim_in.build_shim('in')

        # --- Build the outer shims
        self.shim_out = DipoleModulePMShims(shim_params, pm_params, yoke_params)
        self.shim_out.build_shim('out')

        # --- Symmetries
        # Yoke
        axis = yoke_params.axis

        # --- Assembly
        self.obj = rad.ObjCnt([self.yoke.obj, self.pm_top.obj, self.pm_in.obj, self.pm_out.obj,
                               self.shim_in.obj, self.shim_out.obj])
        if axis == 'x':
            rad.TrfZerPerp(self.obj, [0, 0, 0], [1, 0, 0])  # longitudinal axis
            rad.TrfZerPara(self.obj, [0, 0, 0], [0, 1, 0])  # Vertical axis
        elif axis == 'y':
            rad.TrfZerPerp(self.obj, [0, 0, 0], [0, 1, 0])  # longitudinal axis
            rad.TrfZerPara(self.obj, [0, 0, 0], [0, 0, 1])  # Vertical axis
        elif axis == 'z':
            rad.TrfZerPerp(self.obj, [0, 0, 0], [0, 0, 1])  # longitudinal axis
            rad.TrfZerPara(self.obj, [0, 0, 0], [0, 1, 0])  # vertical axis

    def delete(self, delete_params=True):
        """
        Clear the MM object, erase the Radia objects
        """
        self.result = None
        self.time_end = None
        self.time_start = None
        if delete_params:
            self.params = None
        try:
            rad.UtiDel(self.yoke.obj)
            self.yoke = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.pm_top.obj)
            self.pm_top = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.pm_in.obj)
            self.pm_in = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.pm_out.obj)
            self.pm_out = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.shim_in.obj)
            self.shim_in = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.shim_out.obj)
            self.shim_out = None
        except AttributeError:
            pass

        try:
            rad.UtiDel(self.obj)
            self.obj = None
        except AttributeError:
            pass


# -------------------------------------------
# Class for staggered quad yoke
# -------------------------------------------
class QuadStaggeredYoke(Yoke):

    def __init__(self, yoke_params):
        """Initialize a staggered quadrupole yoke
        :param yoke_params: yoke parameters (instance of )
        """
        # Hold the parameters
        self.yoke_params = yoke_params

        # --- Pole profile
        self.yoke_profile = QuadStaggeredYokeProfile(self.yoke_params.profile_params,
                                                     self.yoke_params.mesh_params)  # Build the profiles and subdivision lists
        self.yoke_profile.change_profile_to_axis(self.yoke_params.axis)  # Permutation of the coordinates depending on the long. axis
        self.build()

    # -------------------------------------------
    def build_pole(self, pole_id, period_id):
        """
        Build a pole
        :param pole_id: pole type (0: up right, 1: down right)
        :param period_id: period identifier
        :return pole
        """
        # --- Get the parameters
        try:
            length = self.yoke_params.length
            period = self.yoke_params.period
            mesh_params = self.yoke_params.mesh_params
            axis = self.yoke_params.axis
            color = self.yoke_params.color
            mat_name = self.yoke_params.mat
            pole_profile = deepcopy(self.yoke_profile.pole_profile)
            pole_sub = self.yoke_profile.pole_sub
        except AttributeError:
            raise Exception('Attribute error in DipoleModulePM.build_pole()')

        pos = period * period_id # Position

        # --- Build the pole
        str_options = 'ki->Numb,TriAngMin->' + str(mesh_params.tri_ang_min) + ',TriAreaMax->' + str(
            mesh_params.area_max)
        # Upper or lower pole ?
        if pole_id == 1:
            pos += period / 2
            for k in range(len(pole_profile)):
                pole_profile[k][0] = - pole_profile[k][0]

        # Extrusion
        pole = rad.ObjMltExtTri(pos, length, pole_profile, pole_sub, axis, [0, 0, 0], str_options)

        # Subdivision
        long_sub_pole = mesh_params.long_sub
        rad.ObjDivMag(pole, [long_sub_pole[0], 1, 1])

        # --- Set the material
        mat = rad_mat.set_soft_mat(mat_name)
        rad.MatApl(pole, mat)

        # --- Symetries
        if pole_id == 0:
            rad.TrfZerPerp(pole, [0, 0, 0], [1, 0, -1])
            rad.TrfZerPerp(pole, [0, 0, 0], [1, 0, 1])
        else:
            rad.TrfZerPerp(pole, [0, 0, 0], [1, 0, 1])
            rad.TrfZerPerp(pole, [0, 0, 0], [1, 0, -1])

        # --- Color
        rad.ObjDrwAtr(pole, color)

        # --- Return the pole
        return pole

    # -------------------------------------------
    def build(self):
        """
        Build an assembly of staggered poles
        """

        # --- Build an empty container
        self.obj = rad.ObjCnt([])

        # --- Build all the periods
        n_per_half_und = int(self.yoke_params.n_periods / 2)
        for k in range(n_per_half_und):
            # build poles 0 to 1
            pole_0 = self.build_pole(0, k)
            pole_1 = self.build_pole(1, k)
            # Add to the container
            rad.ObjAddToCnt(self.obj, [pole_0, pole_1])

        # --- Cut the half of the first magnet
        self.obj = rad.ObjCutMag(self.obj, [0, 0, 0], [0, 1, 0], 'Frame->Lab')[1]

        # --- Symmetries
        rad.TrfZerPara(self.obj, [0, 0, 0], [0, 1, 0])

    # -------------------------------------------
    def print_profile(self):
        """
        Print the yoke profile
        """
        try:
            self.yoke_profile.print(pole_only=True)
        except AttributeError:
            raise Exception('Attribute error in QuadStaggeredYoke.print_profile()')

    # -------------------------------------------
    def plot_profile(self):
        """
        Plot the yoke profile
        (Note that x and z may permuted with respect to the 3D object if the long. axis is 'y')
        """
        try:
            self.yoke_profile.plot(pole_only=True)
        except AttributeError:
            raise Exception('Attribute error in QuadStaggeredYoke.plot_profile()')


# -------------------------------------------
# Class for staggered quad PM blocks
# -------------------------------------------
class QuadStaggeredPM(MM):
    def __init__(self, pm_params, yoke_params):
        """
        Initialize a staggered PM block assembly
        :param pm_params: PM block parameters
        :param yoke_params: yoke parameters (contains the length, etc.)
        """

        # Hold the parameters
        self.pm_params = pm_params
        self.yoke_params = yoke_params

        # Build the PM blocks
        self.build()

    # -------------------------------------------
    def build_pm(self, pm_id, period_id):
        """
        Build a PM block for a staggered quadrupole assembly
        :param pm_id: pole identifier (0 to 3)
        :param period_id: period identifier
        :return the pm block
        """

        # --- Get the parameters
        try:
            # PM parameters
            r0 = self.pm_params.r0
            width = self.pm_params.width
            height = self.pm_params.height
            chamfer = self.pm_params.chamfer
            mesh_params = self.pm_params.mesh_params
            mat_name = self.pm_params.mat
            color = self.pm_params.color
            angle = self.pm_params.angle
            br = self.pm_params.br
            # yoke parameters
            yoke_length = self.yoke_params.length
            period = self.yoke_params.period
            axis = self.yoke_params.axis
        except AttributeError:
            raise Exception('Attribute error in QuadStaggeredPM.build_pm()')

        # --- Contour of the PM block
        x0 = r0 / 2 ** 0.5
        x1, y1 = x0 - (width / 2 - chamfer)  / 2 ** 0.5, x0 + (width / 2 - chamfer) / 2 ** 0.5
        x2, y2 = x1, y1 + chamfer * 2 ** 0.5
        x4 = x0 + height / 2 ** 0.5
        x3, y3 = x4 - width / 2 / 2 ** 0.5, x4 + width / 2 / 2 ** 0.5
        if pm_id > 1:
            pm_profile = [[x0, x0], [x1, y1], [x2, y2], [x3, y3], [x4, x4]]
        else:
            pm_profile = [[-x0, x0], [-x1, y1], [-x2, y2], [-x3, y3], [-x4, x4]]
        pm_sub = [[1, 1], [1, 1], [1, 1], [1, 1], [1, 1]]

        # --- Block length
        length = (period - yoke_length) / 2

        # --- Position and magnetization direction
        if pm_id == 0:
            pos = period * period_id - length / 2
            dir = [1, -1, -1]
        elif pm_id == 1:
            pos = period * period_id + length / 2
            dir = [-1, -1, 1]
        elif pm_id == 2:
            pos = yoke_length / 2 + period * period_id + length / 2
            dir = [-1, -1, -1]
        else:
            pos = yoke_length / 2 + period * period_id + 3 * length / 2
            dir = [1, -1, 1]
        dir_x = dir[0] * cos(angle) / 2 ** 0.5
        dir_y = dir[1] * sin(angle)
        dir_z = dir[2] * cos(angle) / 2 ** 0.5
        dir = [dir_x, dir_y, dir_z]

        # --- Build the block
        str_options = 'ki->Numb,TriAngMin->' + str(mesh_params.tri_ang_min) + ',TriAreaMax->' + str(
            mesh_params.area_max)
        # Extrusion
        pm = rad.ObjMltExtTri(pos, length, pm_profile, pm_sub, axis, dir, str_options)
        # Subdivision
        long_sub_pole = mesh_params.long_sub
        rad.ObjDivMag(pm, [long_sub_pole[0], 1, 1])

        # --- Set the material
        mat = rad_mat.set_pm_mat(mat_name, br=br)
        rad.MatApl(pm, mat)

        # --- Symetries
        if pm_id > 1:
            rad.TrfZerPerp(pm, [0, 0, 0], [1, 0, -1])
            rad.TrfZerPerp(pm, [0, 0, 0], [1, 0, 1])
        else:
            pass
            rad.TrfZerPerp(pm, [0, 0, 0], [1, 0, 1])
            rad.TrfZerPerp(pm, [0, 0, 0], [1, 0, -1])

        # --- Color
        rad.ObjDrwAtr(pm, color)

        # --- Return the pole
        return pm

    # -------------------------------------------
    def build(self):

        # --- Build an empty container
        self.obj = rad.ObjCnt([])

        # --- Build all the periods
        n_per_half_und = int(self.yoke_params.n_periods / 2)
        for k in range(n_per_half_und):
            # build pm blocks 0 to 3
            pm_0 = self.build_pm(0, k)
            pm_1 = self.build_pm(1, k)
            pm_2 = self.build_pm(2, k)
            pm_3 = self.build_pm(3, k)
            # Add to the container
            rad.ObjAddToCnt(self.obj, [pm_0, pm_1, pm_2, pm_3])

        # --- Cut the half of the first magnet
        self.obj = rad.ObjCutMag(self.obj, [0, 0, 0], [0, 1, 0], 'Frame->Lab')[1]

        # --- Symmetries
        rad.TrfZerPara(self.obj, [0, 0, 0], [0, 1, 0])


# -------------------------------------------
# Class for staggered quadrupole
# -------------------------------------------
class QuadStaggered(MM):
    def __init__(self, params=None, prec_params=None, solve_switch=True, print_switch=True):
        """
        Initialize a staggered quadrupole object
        :param params=None: quadrupole parameters, see QuadStaggeredParam
        :param prec_params=None: precision parameters, see MMPrecParam
        :param solve_switch=True: solve the magnetization problem if True
        :param print_switch=True: quiet mode if false
        """
        # --- Parameters
        if params is None:
            self.params = QuadStaggeredParam()
        else:
            self.params = params
        yoke_params = self.params.yoke_params
        surround_field_params = self.params.surround_field_params
        pm_params = self.params.pm_params
        if prec_params is None:
            self.prec_params = PrecParam()
        else:
            self.prec_params = prec_params

        # --- Build
        self.build()

        # --- Solve
        if solve_switch:
            self.solve(print_switch=print_switch)

    def build(self):
        # --- Build the yoke
        self.yoke = QuadStaggeredYoke(self.params.yoke_params)

        # --- Build the background field
        self.surround = Surround(self.params.surround_field_params)

        # --- Assembly
        self.obj = rad.ObjCnt([self.yoke.obj, self.surround.obj])

        # --- Build PM blocks if specified
        if self.params.pm_params is not None:
            self.pm = QuadStaggeredPM(self.params.pm_params, self.params.yoke_params)
            rad.ObjAddToCnt(self.obj, [self.pm.obj])
            # self.obj = self.pm.obj # For debug


# -------------------------------------------
# Generalized Halbach magnets
# -------------------------------------------
class GeneralizedHalbachPM(MM):
    def __init__(self, params=None, prec_params=None, solve_switch=True, print_switch=False):
        """
        Initialized a generalized Halbach quadrupole
        """
        if params is None:
            params = GeneralizedHalbachPMParams()
        if prec_params is None:
            prec_params = PrecParam()

        self.params = params
        self.prec_params = prec_params

        self.build_block_profiles()

        self.build()

    def build(self):
        """
        Build the generalized Halbach magnet
        """
        
        # Build the blocks
        self.obj = rad.ObjCnt([])
        for k in range(len(self.profile)):
            rad.ObjAddToCnt(self.obj, [self.build_block(k)])

        # Symmetries
        rad.TrfZerPara(self.obj, [0, 0, 0], [0, 0, 1])  # Pole symmetry plane
        rad.TrfZerPerp(self.obj, [0, 0, 0], [0, 1, 0])  # longitudinal axis
        
        # Rotate according to the axis specifications
        if self.params.axis == 'x':
            rz = rad.TrfRot([0, 0, 0], [0, 0, 1], - pi / 2)
            rad.TrfOrnt(self.obj, rz)
        elif self.params.axis == 'y':
            pass
        elif self.params.axis == 'z':
            rx = rad.TrfRot([0, 0, 0], [1, 0, 0], pi / 2)
            rad.TrfOrnt(self.obj, rx)

    def build_block(self, k=0):
        """
        Build the kth block of the magnet
        """
        if k < 0 or k >= len(self.profile):
            raise Exception('k is out of range')
        
        try:
            length = self.params.length
            mesh_params = self.params.mesh_params
            color = self.params.color
            mat_name = self.params.mat
        except AttributeError:
            raise Exception('Attribute error in GeneralizedHalbachPM.build_block()')
        
        mx, mz = cos(self.angles[k]), sin(self.angles[k])

        block_sub = self.sub[k]

        # --- Build the block
        str_options = 'ki->Numb,TriAngMin->'+str(mesh_params.tri_ang_min)+',TriAreaMax->' + str(mesh_params.area_max)
        # Extrusion
        block = rad.ObjMltExtTri(length / 4, length / 2, self.profile[k], block_sub, 'y', [-mx, 0, mz], str_options)

         # Subdivision
        long_sub_pole = mesh_params.long_sub
        rad.ObjDivMag(block, [long_sub_pole[0], 1, 1])

        # --- Set the material
        mat = rad_mat.set_pm_mat(mat_name, br=self.params.br)
        rad.MatApl(block, mat)

        # --- Color
        rad.ObjDrwAtr(block, color)

        return block
        
    def build_block_profiles(self):
        
        if self.params.v_aperture is None:
            v_aperture = 0

        else:
            v_aperture = self.params.v_aperture

        # Define the contour of the upper blocks
        dtheta = pi / (self.params.n_blocks // 2)

        self.profile = []
        self.sub = []
        self.angles = []
        
        for k in range(self.params.n_blocks // 2):
            if self.params.dr is not None:
                dr = self.params.dr[k]
            else:
                dr = 0

            # z0 = 1j * v_aperture / 2 + (self.params.r0 + dr) * exp(1j * k * dtheta)
            # z1 = 1j * v_aperture / 2 + (self.params.r0 + dr) * exp(1j * (k + 1) * dtheta)
            # z2 = 1j * v_aperture / 2 + self.params.r1 * exp(1j * (k + 1) * dtheta)
            # z3 = 1j * v_aperture / 2 + self.params.r1 * exp(1j * k * dtheta)

            z = [(self.params.r0 + dr) * exp(1j * k * dtheta), 
                 (self.params.r0 + dr) * exp(1j * (k + 1) * dtheta),
                 self.params.r1 * exp(1j * (k + 1) * dtheta),
                 self.params.r1 * exp(1j * k * dtheta)]
            
            theta_k = polar((z[1] + z[2]) / 2)[1]
            
            for k in range(4):
                if z[k].imag < v_aperture / 2:
                   z[k] = z[k].real + 1j * v_aperture / 2
            
            self.profile.append([[z[0].imag, -z[0].real], 
                                 [z[1].imag, -z[1].real],
                                 [z[2].imag, -z[2].real],
                                 [z[3].imag, -z[3].real]])
            
            self.sub.append([[1,1],[1,1],[1,1],[1,1]])

            if self.params.angles is not None:
                self.angles.append(self.params.angles[k])

            else:
                self.angles.append(2 * self.params.order * theta_k)


# -------------------------------------------
# Base class for surrounding fields (solenoid, backgroud, etc.)
# -------------------------------------------
class Surround(MM):
    """Base class for surrounding field (solenoid, background, etc.)"""
    def __init__(self, params=None):
        # --- Parameters
        if params is None:
            params = SurroundParam()
        self.surround_field_params = params
        # --- Build the surrounding field according to the type
        if params.type == 'background':
            self.obj = Background(self.surround_field_params).obj
        elif params.type == 'solenoid':
            self.obj = Solenoid(self.surround_field_params).obj

    def field(self,xyz=None,b='by'):
        """
        return the field at xyz
        :param xyz=None: point
        :param b='by': component
        :return the field
        """
        # --- Default position
        if xyz is None:
            xyz = [0, 0, 0]
        # --- Field
        b_xyz = rad.Fld(self.obj,b, xyz)
        # --- Return
        return b_xyz


# -------------------------------------------
# Background field
# -------------------------------------------
class Background(Surround):
    def __init__(self, background_params=None):
        # --- Hold the value of the surrounding field
        if background_params is None:
            self.background_params = BackgroundParam()
        else:
            self.background_params = background_params
        # --- Define the background field
        self.obj = rad.ObjBckg(self.background_params.surround_field_params.back_field_params)


# -------------------------------------------
# Solenoid
# -------------------------------------------
class Solenoid(Surround):
    def __init__(self, solenoid_params=None):
        # --- Hold the value of the solenoid parameters
        if solenoid_params is None:
            self.solenoid_params = SolenoidParam()
        else:
            self.solenoid_params = solenoid_params
        # --- Get the parameters
        length = solenoid_params.length
        r_in = solenoid_params.r_in
        r_out = solenoid_params.r_out
        j = solenoid_params.j
        k = solenoid_params.k
        n_seg = solenoid_params.n_seg
        a = solenoid_params.axis
        # --- Build the solenoid
        self.obj = rad.ObjRaceTrk([0,0,0], [r_in, r_out], [0, 0], length, n_seg, j * k, 'm', a)


# -------------------------------------------
# Shield
# -------------------------------------------
class Shield(MM):
    def __init__(self, shield_params=None):
        if shield_params is None:
            self.shield_params = ShieldParam()
        else:
            self.shield_params = shield_params

    def build_flange(self, flange_type='full', pos=0):

        # Get the parameters
        try:
            length = self.shield_params.length
            profile_params = self.shield_params.profile_params
            axis = self.shield_params.axis
        except AttributeError:
            raise Exception('Attribute error in Shield.build_flange()')

        # Build the profile
        self.yoke_profile = ShieldProfile(profile_params)
        if flange_type == 'full':
            self.yoke_profile.build_full_flange()
        elif flange_type == 'opened':
            self.yoke_profile.build_opened_flange()
        self.yoke_profile.change_profile_to_axis(axis)  # Permutation of the coordinates depending on the long. axis
