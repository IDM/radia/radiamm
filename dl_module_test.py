import radia as rad
from matplotlib.pyplot import plot, show
import numpy as np
from radia_mm import MeshExtTriParam, PrecParam, DipoleModulePMYokeParam, DipoleModulePMYoke, \
    DipoleModulePMYokeProfileParam, DipoleModulePMYokeProfile, DipoleModulePMYokeParam, DipoleModulePMYoke, \
    DipoleModulePMBlocksParam, DipoleModulePMBlocks, DipoleModulePMParam, DipoleModulePM

# --- Profile
# p_dl_prof = DipoleModulePMYokeProfileParam()
# dl_prof = DipoleModulePMYokeProfile(profile_params=p_dl_prof)
# dl_prof.build_ebs_dl_high_field()
# dl_prof.plot()

# --- Yoke Tests
# p_yoke = DipoleModulePMYokeParam(axis='y')
# yoke = DipoleModulePMYoke(p_yoke)
# yoke.plot_geo()
# input()

# --- PM block Tests
# p_pm = DipoleModulePMBlocksParam()
# p_yoke = DipoleModulePMYokeParam(axis='y')
# pm_top = DipoleModulePMBlocks(pm_params=p_pm, yoke_params=p_yoke)
# pm_top.build_top_pm()
# pm_top.plot_geo()
# pm_side = DipoleModulePMBlocks(pm_params=p_pm, yoke_params=p_yoke)
# pm_side.build_side_pm()
# pm_side.plot_geo()
# input()

# --- DL magnet
p_dl = DipoleModulePMParam(dipole_module_type='dl_1')
dl = DipoleModulePM(p_dl, solve_switch=True)
multi, ib, ib_r, xz = dl.multipoles()
dl.plot_geo()
input()